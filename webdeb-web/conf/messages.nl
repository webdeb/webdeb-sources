#
# WebDeb - Copyright (C) <2014-2019> <Université catholique de Louvain (UCL), Belgique ; Université de Namur (UNamur), Belgique>
#
# List of the contributors to the development of WebDeb: see AUTHORS file.
# Description and complete License: see LICENSE file.
#
# This program (WebDeb) is free software:
# you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program (see COPYING file).
# If not, see <http://www.gnu.org/licenses/>.
#
#

title.dev=WebDeb DEV
title.qa=WebDeb QA Platform (test)
title.prod=WebDeb Platform
title.navbar.dev=WebDeb DEV
title.navbar.qa=WebDeb QA (test)
title.navbar.prod=WebDeb
navbar.style.dev=default
navbar.style.qa=inverse
navbar.style.prod=default

#
# General purpose
#
# Welcome screen
welcome.cookie=We gebruiken cookies. We volgen u niet persoonlijk, maar we gebruiken ze om u de beste \
  surfervaring te bieden. Lees ons <a href="{0}" class="primary" target="_blank">cookies beleid</a> om meer te weten.
welcome.btn.sign.up=Meld je nu aan

welcome.example.actor.pic=780.jpg
welcome.example.actor.name=Hillary Clinton
welcome.example.text.content=Ik heb een droom dat deze natie op een dag zal opstaan en de ware betekenis zal uitdragen \
van zijn credo: "Wij houden deze waarheden als vanzelfsprekend, dat alle mensen gelijk geschapen zijn." Ik heb een droom dat een \
dag op de rode heuvels van Georgia de zonen van voormalige slaven en de zonen van voormalige slaveneigenaars samen zullen kunnen zitten \
aan de tafel van broederschap. (...)
welcome.example.tag.content=Onderwijs
welcome.example.debate.content=Moeten we migranten verwelkomen of niet?
welcome.example.text.author=Martin Luther King
welcome.example.arg.content=Het doel (van het onderwijs) moet de training zijn van onafhankelijk handelende en denkende individuen.
welcome.example.arg.author=Albert Einstein
welcome.example.browse.content=<span class="fa fa-tag smaller-font"></span> Ecologie <span class="fa fa-tag smaller-font"></span> \
  politieke speech <span class="fa fa-tag smaller-font"></span> Europese Unie
welcome.example.validate.origin=Ik zal Amerika weer groot maken!
welcome.example.validate.destination=Donald Trump belooft de Verenigde Staten weer groot te maken.

welcome.example.question.actor=Wilt u meer informatie over <a href="{1}"> {0} </a>?
welcome.example.question=Bekijk de meest populaire actoren <a href="{0}"> hier </a>.
welcome.example.question.topic=Wilt u meer informatie over <a href="{0}"> {1} </a> of <a href="{2}"> {3} </ a >?
welcome.example.question.args=Bekijk de meest populaire onderwerpen <a href="{0}"> hier </a>.
welcome.example.mapsign=Zoeken naar gestructureerde informatie
welcome.example.exchange=Leren argumenteren

welcome.slogan.1=collectief geheugen
welcome.slogan.2=van het debat

welcome.slogan.3= Het samenwerkingsplatform om te (laten) weten wie wie is en wie wat zegt.
welcome.slogan.4=Alle info is vrij toegankelijk en kan door u worden verrijkt.
welcome.index.title=WebDeb —
welcome.index.desc=The WebDeb index page

welcome.index.1.title=Verkennen
welcome.index.2.title=$Contribuer à
welcome.index.2.title.bis=$Contribuer à
welcome.index.3.title=$Enseigner avec

welcome.index.1.subtitle=te weten wie is wie en wie wat zegt <br> begrijpen wat het debat doet leven
welcome.index.2.subtitle=spraakmakende meningen <br> te delen banden tussen actoren te tonen
welcome.index.3.subtitle=correct met informatie te  leren <br> omgaan de posities in het debat te leren begrijpen


welcome.index.whatis.1=Wat is
welcome.index.whatis.2=$Une mémoire partagée    
welcome.index.whatis.3=$Pour savoir qui est qui et qui dit quoi et y voir clair dans ce qui fait débat
welcome.index.whatis.4=$Une mine d’informations 
welcome.index.whatis.5=$Sur les acteurs, les débats, les positions et les arguments
welcome.index.whatis.6=$Un site gratuit et ouvert   
welcome.index.whatis.7=$Vous pouvez le consulter sans vous enregistrer   
welcome.index.whatis.8=$Un outil collaboratif  
welcome.index.whatis.9=$Vous pouvez ajouter ou modifier une information
welcome.index.whatis.10=$Un site indépendant
welcome.index.whatis.11=$Aucune opinion n’'est privilégiée
welcome.index.whatis.12=$Une plateforme tous terrains    
welcome.index.whatis.13=$Tous les débats peuvent être abordés, dans toutes les langues   

welcome.index.whatshows.1=Wat we kunnen zien op
welcome.index.whatshows.2=De loopbaan van een persoon 
welcome.index.whatshows.3=De samenstelling van een organisatie
welcome.index.whatshows.4=Het organisatieschema van een organisatie
welcome.index.whatshows.5=Wat wordt er gezegd van een acteur
welcome.index.whatshows.6=De argumenten van een debat
welcome.index.whatshows.7=Actoren voor of tegen een mening
welcome.index.whatshows.8=De argumenten van een tekst
welcome.index.whatshows.9=Coalities rond een kwestie

welcome.index.join.1=Om de informatie op WebDeb te raadplegen
welcome.index.join.2=de weg is vrij
welcome.index.join.3=Om de database te verrijken
welcome.index.join.4=word lid van WebDeb

explore.title.news=In het nieuws
explore.title.slides=Korte presentatie
explore.cte.order.title=Order

explore.advice.title.fr=Titel in het Frans
explore.advice.title.en=Titel in het Engels
explore.advice.title.nl=Titel in het Nederlands

# flash and message keys
success=Succes
info=Informatie
danger=Fout
warning=Waarschuwing

# other gen purpose
label.yes=Ja
label.no=Nee
label.unknown=Onbekend
label.unknown.f=Onbekend
label.and=en
label.of=van
label.nodata=geen gegevens om weer te geven...
label.adlreadyInGroup=Want al in groep

general.btn.zoomout=Uitzoomen
general.btn.zoomin=Inzoomen
general.btn.next=Volgende
general.btn.prev=Vorige
general.btn.cancel=Annuleren
general.btn.saveleave=$Quitter et enregistrer
general.btn.save=Opslaan
general.btn.savein=Opslaan in ...
general.btn.delete=Verwijderen
general.btn.close=Sluiten
general.btn.goback=Ga terug
general.btn.goback.title=Ga terug naar de vorige pagina
general.btn.goback.main=Ga terug naar de startpagina
general.btn.confirm=Bevestigen
general.btn.confirmin=Bevestig in ...
general.btn.edit=$Editer
general.btn.submit=Submit
general.btn.ok=Ok
general.btn.see=$Voir
general.btn.display=$Afficher
general.btn.display.by=$Afficher par :
general.btn.in=$Dans :
general.btn.select=Select
general.btn.undo=Undo
general.btn.copy=$copier
general.btn.nothanks=Nee, bedankt!

general.btn.merge=Samenvoegen
general.btn.contribution.see.tooltip=bijdrage bekijken
general.btn.contribution.edit.tooltip=voor deze bijdrage wijzigen, toevoegen, verwijderen
general.btn.contribution.add.tooltip=Elementen toevoegen aan deze bijdrage
general.required.fields=Velden gemarkeerd met een * zijn verplicht.
general.print.text=Afdrukken
general.delete.success=Succesvolle verwijdering !
general.delete.error=Verwijderen mislukt ...
general.sort.label=Items sorteren
general.thisway=Hier
general.overthere=Daar
general.underconstruct=Deze visualisatie is nog niet beschikbaar ...
general.share=Delen
general.more=Kom meer te weten
general.group=Group
general.required=$Ce champs est requis
general.contributors=$avec {0} contributeurs
general.contributions=$contient {0} contributions
general.contributions.complete=$et
general.contributions.no.result=$Aucun résultat
general.contributions.search.instructions=$Commencez à taper pour rechercher, appuyez ensuite sur enter ou sur la loupe.
general.citations.search.instructions=Commencez à taper pour rechercher, appuyez ensuite sur enter ou sur la loupe pour voir les résultats. \
  Sélectionnez ensuite une ou plusieurs citations et cliquez ensuite sur le bouton enregistrer
general.continue.title=$Continuer
general.classify.title=$Classer
general.results.see.more=$Plus de résultats

general.add.btn.debate=$Ajouter un débat
general.add.btn.argument=$Ajouter un argument
general.add.btn.argument.sub=$Ajouter un sous argument
general.add.btn.argument.first=$Ajouter une affirmation faîtière
general.add.btn.citation=$Ajouter une citation
general.add.btn.position=$Ajouter une position
general.add.btn.category=$Ajouter une catégorie
general.add.btn.subdebate=$Ajouter une thèse
general.add.btn.tag=$Ajouter une réponse
general.add.btn.tag.parent=$Ajouter un dossier plus large
general.add.btn.tag.child=$Ajouter un sous dossier
general.add.btn=$Ajouter

general.debate.none=$Il n''y a aucun débat
general.citation.none=$Il n''y a aucune citation
general.actor.none=$Il n''y a aucun acteur
general.text.none=$Il n''y a aucun texte
general.tag.none=$Il n''y a aucun dossier

general.message.danger=Operatie mislukt...
general.message.warning=Informatie moet worden gecorrigeerd of toegevoegd
general.message.success=Succesvolle operatie !

general.contribution.action.viz.title=$Visualisation :
general.contribution.action.edit.title=$Édition :
general.contribution.action.add.title=$Ajout :

general.order.title=$Trier par
general.order.popular.title=$Les plus populaires
general.order.relevance.title=$Les plus pertinents
general.order.version.desc.title=$Les plus récents
general.order.version.asc.title=$Date d'ajout (la plus ancienne)

general.metatitle= — WebDeb

general.metatitle.explore.title=Explore
general.metatitle.explore.desc=See what is new on WebDeb

general.metatitle.teach.title=$Enseigner

general.dialog.confirm.title=Weet u het zeker?
general.dialog.confirm.delete=Weet u zeker dat u het geselecteerde element wilt verwijderen?
general.dialog.wait=Even geduld aub ...
general.btn.load.more=Laad meer gegevens
general.more.details=Bekijk details
general.more.details.tooltip=Bekijk alle details voor deze bijdrage
general.search.pointable.instructions=Selecteer een element om verder te gaan
general.screen.toosmall=Uw scherm is te smal om deze pagina correct weer te geven. \ Indien u een smartphone gebruikt, bekijk dan uw scherm horizontaal.

general.goto.source=ga naar de bron
general.edit.properties=Eigenschappen bewerken
general.implemented.soon=Binnenkort beschikbaar ...
general.got.it=Begrepen!

general.chart.export=Exporteer
general.chart.export.title=Exporteer deze visualisatie
general.chart.export.desc=Kies uw exportformaat

general.label.pic=Afbeelding (geen auteursrechten)
general.label.logo=Logo
general.label.btn.browse=Bladeren ...
general.label.btn.clear=Verwijderen
general.label.btn.clearlink=Deze link verwijderen
general.label.btn.dowload=Downloaden
general.label.pic.preview=Voorbeeld
general.label.pic.noimage=Geen afbeelding ...
general.label.pic.error=Deze foto is te groot (max. 5MB).
general.upload.pic.error=Er is een fout opgetreden bij het uploaden van de afbeelding.
general.upload.pic.unsafe=Deze foto lijkt ongepast te zijn door naaktheid of geweld te tonen.

# Pronoum and determiner
general.pronoun.personal.I=Ik
general.pronoun.personal.you=Jij
general.pronoun.personal.he=Hij
general.pronoun.personal.she=Zij
general.pronoun.personal.it=Het
general.pronoun.personal.we=Wij
general.pronoun.personal.you2=Jullie
general.pronoun.personal.they=Zij

#
# filters
#
general.filter.show=Open filters
general.filter.title=Filteren op ...
general.filter.delete.title=$Supprimer les filtres
general.filter.label.ctype=Bijdrage type
general.filter.ctype.-1=Contribution
general.filter.ctype.0=Actor
general.filter.ctype.1=Debat
general.filter.ctype.4=Argument
general.filter.ctype.3=Quote
general.filter.ctype.2=Tekst
general.filter.ctype.6=tag
general.filter.title.ctype=Filteren op bijdragetype

general.filter.label.name=Actor of auteursnaam
general.filter.title.name=Filteren op actor of betrokken auteursnaam

general.filter.label.affiliation=Verbanden tussen actoren of auteurs
general.filter.title.affiliation=Filter op affiliatie van actoren of betrokken auteurs

general.filter.label.function=Actor of auteurfuncties
general.filter.title.function=Filteren op functie van actoren of betrokken auteurs

general.filter.label.graduating=$Afficher les diplômes
# actors
general.filter.label.atype=Type actor
general.filter.title.atype=Filteren op type actor

general.filter.label.arole=$Role de l''acteur
general.filter.title.arole=$Filtrer par role de l''acteur

general.filter.label.afftype=Type aansluiting
general.filter.title.afftype=Filteren op type aansluiting

general.filter.label.gender=Geslacht van de actor
general.filter.title.gender=Filteren op geslacht van de actor

general.filter.label.birthdate=Geboortedatum of aanmaakdatum
general.filter.title.birthdate=Filteren op geboortedatum (personen) of aanmaakdatum (organisatie)

general.filter.label.country=Land van de actor
general.filter.title.country=Filtreren op land van de actor of auteur

general.filter.label.legal=Juridische status
general.filter.title.legal=Filter organisatie op juridische status

general.filter.label.sector=Zakelijke sector
general.filter.title.sector=Filter organisatie per bedrijfssector

# textual contributions
general.filter.label.publidate=Publicatiedatum
general.filter.title.publidate=Filteren op publicatiedatum

general.filter.label.publidate.from=$Depuis (aaaa, mm/aaaa ou dd/mm/aaaa)
general.filter.label.publidate.to=$Jusqu'à (aaaa, mm/aaaa ou dd/mm/aaaa)

general.filter.label.language=Taal
general.filter.title.language=Filter op taal

general.filter.label.topic=Onderwerp
general.filter.title.topic=Filteren op onderwerp

general.filter.label.origin=Brontype
general.filter.title.origin=Filteren op brontype (tekst, media, ...)

general.filter.label.source=Bronnaam
general.filter.title.source=Filteren op bronnaam, waar de bijdrage vandaan kwam

general.filter.label.ttype=Teksttype
general.filter.title.ttype=Filteren op teksttype (journalistiek, wetenschappelijk, ...)

general.filter.label.tag=Dossier
general.filter.title.tag=Filteren op dossier

general.filter.label.ftype=Soort dossier
general.filter.title.ftype=Filteren op soort dossier

general.filter.label.author=$Nom de l''auteur

general.filter.label.argtype=Meningtype
general.filter.title.argtype=Filter op meningen

general.filter.button.msg=Filter de resultaten

general.filter.label.place=Gebied
general.filter.title.place=Filteren op gebied gelinkt aan de mening of de quote

general.filter.label.fulltext.text=$Titre du texte
general.filter.label.fulltext.debate=$Titre du débat
general.filter.label.fulltext.tag=$Titre du dossier
general.filter.label.fulltext.citation=$Extrait de la citation
general.filter.label.fulltext.actor=$Nom


general.browser.ie.warning.title=Waarschuwing, het lijkt alsof u Internet Explorer gebruikt
general.browser.ie.warning.msg=Microsoft ondersteunt deze webbrowser niet meer. Deze zou incompatibel kunnen zijn met het gebruik van WebDeb.\
Verder heeft deze browser mogelijke beveiligingstekortkomingen. Indien u Windows 10 heeft, gebruik dan liever Edge.  
general.browser.warned=Ik begrijp het, en ik wil doorgaan
general.browser.advice=We raden aan WebDeb met de volgende browsers te gebruiken:/ 
general.browser.firefox=Mozilla Firefox
general.browser.vivaldi=Vivaldi
general.browser.chrome=Google Chrome

general.contribution.private.title=Deze bijdrage is privé of binnen een privé groep. U kan deze niet bekijken of wijzigen.
general.contribution.private.msg=U kan deze bijdrage niet bekijken of wijzigen.

contributions.dropdown.actor.see=$Voir l''acteur
contributions.dropdown.text.see=$Voir le texte

contribution.claim.title=$Signaler
contribution.claim.type.title=$Description du problème
contribution.claim.comment.title=$Commentaire
contribution.claim.from.title=$Signalé depuis
contribution.claim.type.0=$Inexacte
contribution.claim.type.1=$Offensant
contribution.claim.type.2=$Publicité / Spam

#
#menus
#
menu.dash=Situatieoverzicht
menu.browse=Verkennen
menu.newentry=Bijdragen
menu.viz=Visualiseer
menu.validate=Valideren
menu.about=Over
menu.aboutus=Het WebDeb-team
menu.partners=Media Partners
menu.nomail=Inschrijvingsmail niet ontvangen?
menu.help=Help
menu.terms=Algemene voorwaarden
menu.contact=Contact
menu.signin=Aanmelden
menu.lang=Taal
menu.lang.en=English
menu.lang.fr=Français
menu.lang.nl=Nederlands

# breadcrumb
bread.home=home
bread.error=foutpagina
bread.contribute=bijdragen
bread.contribute.actor=bewerk actor
bread.contribute.text=tekst bewerken
bread.contribute.argument=tekst annoteren
bread.contribute.argument.edit=wijzig mening
bread.contribute.link=nieuwe link
bread.search=zoeken
bread.viz=bekijken
bread.viz.argument=argument
bread.viz.text=text
bread.viz.actor=acteur
bread.about=over het project
bread.aboutus=over het team
bread.dashboard=dashboard
bread.settings=profiel
bread.settings.update=profiel bijwerken
bread.recover=wachtwoord herstellen
bread.partner=partners
bread.validate.link=link valideren
bread.validate.argument=mening valideren
bread.help=help
bread.terms=algemene voorwaarden
bread.group.mark=bijdragen markeren
bread.group.seeall=groepsbijdragen
bread.group.merge=bijdragen samenvoegen
bread.editFreeSources.text=bewerk gratis bronnen

# footer
footer.project.support=ondersteund door :

#
# Exceptions
#
database.freesource.remove.error=Er is een fout opgetreden bij het verwijderen van het gratis copyright, deze is niet verwijderd.

# general error messages
error.hack=Lijkt erop dat u probeert toegang te krijgen tot iets waar u geen toegang toe heeft ...
error.crash=Er is een onverwachte fout opgetreden, neem contact op met de beheerder als dit opnieuw gebeurt.
error.oops=De pagina waarnaar u op zoek bent bestaat niet
error.form=Het ingediende formulier bevat fouten, corrigeer ze voordat je ze indient.

# permission exception message keys (see PermissionException enum, underscores in enum items are replaced by dots)
permission.affiliation.delete.notpermitted=Je mag deze affiliatie niet verwijderen.
permission.not.group.member=Je bent geen lid van deze groep.
permission.not.group.owner=Je bent geen eigenaar van deze groep.
permission.not.public.group=Deze groep is niet openbaar.
permission.text.not.visible=Je mag de inhoud van deze tekst niet bekijken.
permission.text.not.private=De tekst is mogelijk niet privé ingesteld omdat deze niet privé is gemaakt.
permission.error.scope=U mag wijzigingen in deze bijdrage niet opslaan in de huidige vorm (groep).

# persistence errors (used in PersistenceException)
persistence.error.delete.contribution=Er is een databasefout opgetreden, uw bijdrage is niet verwijderd.
persistence.error.save.actor=Er is een databasefout opgetreden, je actor is niet opgeslagen.
persistence.error.save.affiliation=Er is een databasefout opgetreden, de verzonden voorkeuren zijn niet opgeslagen.
persistence.error.remove.affiliation=Er is een databasefout opgetreden, een affiliatie kon niet worden verwijderd.
persistence.error.save.text=Er is een databasefout opgetreden, de ingediende tekst is niet opgeslagen.
persistence.error.save.argument=Er is een databasefout opgetreden, het ingediende mening is niet opgeslagen.
persistence.error.save.link=Er is een databasefout opgetreden, de meninglink is niet opgeslagen.
persistence.error.save.topic=Er is een fout opgetreden bij het opslaan van de onderwerpen die aan uw bijdrage zijn gekoppeld, uw bijdrage is niet opgeslagen.
persistence.error.save.source=Er is een fout opgetreden bij het opslaan van de bron die aan uw tekst is gekoppeld, uw bijdrage is niet opgeslagen.
persistence.error.bind.actor=Er is een databasefout opgetreden bij het verbinden van actor en bijdrage, uw bijdrage is niet opgeslagen.
persistence.error.unbind.actor=Er is een databasefout opgetreden bij het verwijderen van een actor uit de bijdrage, de actor is er nog steeds aan gekoppeld.
persistence.error.save.text.links=Er is een databasefout opgetreden, de justificatielinken zijn niet opgeslagen.

persistence.error.save.tag=Er is een fout opgetreden, het dossier kon niet opgeslagen worden.
persistence.error.save.tag.link=Er is een fout opgetreden, de link tussen dossiers kon niet opgeslagen worden. 

persistence.error.save.contributor=Er is een databasefout opgetreden, we konden uw registratieverzoek niet opslaan.
persistence.error.update.contributor=Er is een databasefout opgetreden, we konden niet doorgaan met uw verzoek.
persistence.error.bind.contributor=Er is een databasefout opgetreden tijdens het binden van u als bijdrager, uw bijdrage is niet opgeslagen.

persistence.error.add.member=Er is een databasefout opgetreden, de bijdrager is niet toegevoegd aan de geselecteerde groep.
persistence.error.remove.member=Er is een databasefout opgetreden, de bijdrager is niet uit de geselecteerde groep verwijderd.
persistence.error.default.group=Er is een databasefout opgetreden, we konden uw nieuwe standaardgroep niet instellen.
persistence.error.save.group=Er is een databasefout opgetreden, we konden de groep niet opslaan.
persistence.error.invite.group=Er is een databasefout opgetreden, we konden leden niet uitnodigen om te groeperen.

persistence.error.mark.group=Uw validatie en markeringen zijn niet opgeslagen, neem contact op met de beheerder.
persistence.error.merge=Er is een fout opgetreden bij het samenvoegen van bijdragen, het samenvoegen is mislukt.
persistence.error.merge.mismatch=Er is een fout opgetreden bij het samenvoegen van bijdragen, het lijkt erop dat u probeerde verschillende soorten bijdragen samen te voegen.
persistence.error.merge.professions=Er is een fout opgetreden bij het samenvoegen van beroepen.

persistence.error.freesource.save=Er is een fout opgetreden bij het opslaan van de gratis auteursrechtbron, deze is niet opgeslagen.
persistence.error.freesource.delete=Fout bij het verwijderen van de gratis bron

# outdated version exception
persistence.error.outdated.version=Een andere bijdrager heeft in de tussentijd dezelfde bijdrage bijgewerkt. Codeer uw wijzigingen opnieuw.

# not found object exception
persistence.error.not.found=Het object waarnaar je op zoek bent bestaat niet.
persistence.error.not.found.ingroup=Het object waarnaar u op zoek bent, is niet zichtbaar in deze groep.

# follow group error
persistence.error.follow.group.error=Er is een fout opgetreden tijdens het opslaan van de groepsvoorkeuren...
persistence.error.unfollow.default.group=Standaard kan u uw groep niet ontvolgen.

#
# nlp services error
#
nlp.text.error=Er kon geen informatie uit de gegeven URL worden gehaald.
nlp.text.invalidurl=Deze url is ongeldig.
nlp.language.error=Geen enkele taal kon worden gevonden uit de analyse van de gegeven tekst.
nlp.actor.error=Geen details kunnen worden opgehaald voor de actor die je hebt ingevuld.

#
# icons
#

general.filter.icon.ctype=fa fa-fw fa-book
general.filter.icon.name=far fa-fw fa-address-book
general.filter.icon.affiliation=far fa-fw fa-address-card
general.filter.icon.function=fa fa-fw fa-user-secret
general.filter.icon.atype=fa fa-fw fa-street-view
general.filter.icon.afftype=fas fa-link
general.filter.icon.gender=fa fa-fw fa-venus-mars
general.filter.icon.birthdate=fa fa-fw fa-birthday-cake
general.filter.icon.country=fa fa-fw fa-globe
general.filter.icon.sector=fa fa-fw fa-industry
general.filter.icon.publidate=fa fa-fw fa-calendar
general.filter.icon.language=fa fa-fw fa-language
general.filter.icon.topic=fa fa-fw fa-tags
general.filter.icon.origin=fa fa-fw fa-clipboard
general.filter.icon.source=fas fa-fw fa-external-link-alt
general.filter.icon.ttype=fas fa-fw fa-file-alt
general.filter.icon.tag=fa fa-fw fa-tag
general.filter.icon.ftype=fa fa-fw fa-tag
general.filter.icon.argtype=far fa-fw fa-comments
general.filter.icon.place=fa fa-fw fa-globe
general.filter.icon.legal=fa fa-fw fa-university

#format errors
contributor.error=Er zijn fouten opgetreden in de gegevens die u hebt ingevuld.
actor.error=Er is een fout opgetreden in de ingediende actor.
affiliation.error=Er is een fout opgetreden in de ingediende affiliatie.
argument.error=Er is een fout opgetreden in het ingediende mening.
link.error=Er is een fout opgetreden in de link voor het ingediende mening.
text.error=Er is een fout opgetreden in de ingediende tekst.
tag.error=Er is een fout opgetreden in de ingediende tag.
tag.link.error=Er is een fout opgetreden in de ingediende taglink.
role.error=Er is een fout opgetreden in de rol van de ingediende actor.
unknown.gender=Ingegeven geslacht-id bestaat niet.
unknown.legalstatus=Ingegeven juridische status id bestaat niet.
unknown.sector=Ingegeven bedrijfstak-ID bestaat niet.
unknown.profession=Ingegeven beroep id bestaat niet.
unknown.argument.type=Ingegeven meningtype bestaat niet.
unknown.argument.link=Ingegeven mening link bestaat niet.
unknown.tag=Dit dossier bestaat niet.
unknown.tag.link=De link naar dit dossier bestaat niet.
unknown.topic=Ingegeven onderwerp-id bestaat niet.
unknown.language=Ingegeven taal-id bestaat niet.
unknown.text.visibility=Ingegeven tekst zichtbaarheid bestaat niet.

# toolbox
toolbox.menu.settings=Mijn profiel
toolbox.menu.contributions=Mijn bijdragen
toolbox.menu.settings.other=Profiel van
toolbox.menu.contributions.other=Bijdragen van
toolbox.menu.group=Mijn inschrijvingen
toolbox.menu.group.admin=Mijn groepen
toolbox.menu.project.admin=Projecten
toolbox.menu.feeder=Gegevens importeren
toolbox.menu.admin=De site beheren
toolbox.menu.profession=Beheer functies
toolbox.menu.freeSources=Beheer gratis bronnen
toolbox.menu.arguments.dictionary=Woordenboek beheren
toolbox.menu.arguments.mail=E-mail sturen
toolbox.menu.help=Help
toolbox.menu.logout=Afmelden

#account / registration
#
login.first=U moet eerst inloggen voordat u kan bijdragen aan WebDeb.
login.register=Nog geen onderdeel van de WebDeb-gemeenschap? Meld je nu aan.
login.success=U bent net ingelogd, welkom bij het WebDeb-platform.
login.failure=ongeldige inloggegevens (e-mail of wachtwoord).
login.not.validated=U hebt uw e-mail niet bevestigd (of gevraagd om een wachtwoordherstel), controleer eerst uw e-mails.
login.error.cookies=Uw browser ondersteunt geen cookies, schakel deze aub in om verbinding te kunnen maken.
login.banned=Je bent verbannen van het platform, neem contact op met de beheerder.
login.logout=Je bent zojuist afgemeld, tot snel op WebDeb.

# sign-in
signin.signin=Aanmelden
signin.email=Uw e-mailadres
signin.password=Uw wachtwoord
signin.remember=Onthoud mij
signin.register=Wilt u registreren?
signin.forgot=Wachtwoord vergeten?
signin.error.mailorpass=Zowel uw e-mail als uw wachtwoord zijn verplicht.
signin.error.nomatch=Ongeldige email of wachtwoord
signin.not.validated=Validatie of wachtwoordherstel aangevraagd

# contact
contact.subject.label=*/Onderwerp van het bericht * 
contact.subject.place=Onderwerp toevoegen
contact.subject.required=Een berichtonderwerp is vereist.

contact.content.label=Inhoud van het bericht *
contact.content.place=Voer hier uw bericht in
contact.content.required=Een bericht is vereist.

contactus.success=Bericht succesvol verzonden! Wij zullen zo snel mogelijk antwoorden.
contactus.fail=Het bericht kan niet verzonden worden, gelieve later opnieuw te proberen.

# contributor-related fields (signup and profile)
contributor.signup.successfull=Je hebt je succesvol aangemeld, check je e-mails!
contributor.signup.validated=Je hebt je account succesvol gevalideerd, maak nu verbinding!
contributor.signup.error=Je inschrijvingsformulier bevat fouten, los ze aub op
contributeor.signup.error.email=U heeft zich succesvol ingeschreven, maar er is een fout opgetreden bij het verzenden van een e-mail naar uw adres. Neem contact op met de beheerder.
contributor.signup.error.save=Er is een fout opgetreden bij het verzenden van uw formulier naar de database. Probeer het opnieuw of neem contact op met de systeembeheerder.
contributor.signup.msg.success=Uw account is aangemaakt. Controleer uw e-mails om uw abonnement te voltooien.

contributor.validate.success=Uw account is gevalideerd. Welkom bij het WebDeb-platform, {0}.
contributor.validate.failure=Er bestaat geen validatie-aanvraag. Zorg ervoor dat u de validatielink heeft gebruikt die we in onze e-mail hebben verstrekt.
contributor.validate.unknown=Er is een fout opgetreden tijdens het valideren van uw account, neem contact op met de beheerder.
contributor.validate.already=Uw account is al gevalideerd, u kunt verbinding maken met WebDeb.
contributor.validate.expired=Uw abonnement is verlopen, meld u opnieuw aan.

contributor.edit.nosuch=Er kan geen bijdrager worden opgehaald.
contributor.edit.error=Er zijn fouten in het ingediende formulier.
contributor.edit.success=Je hebt je profiel succesvol bijgewerkt.
contributor.edit.other.success=Je hebt het profiel van {0} succesvol bijgewerkt.

contributor.new.title=Word lid van WebDeb?
contributor.edit.title=Profiel bijwerken
contributor.new.btn=Aanmelden
contributor.new.intro=Alle velden zijn verplicht (gemarkeerd met <b> * </b>). Uw persoonlijke gegevens worden niet overgedragen aan een andere partij en zijn alleen zichtbaar voor webdeb \
   geregistreerde gebruikers. Uw e-mailadressen kunnen alleen worden bekeken en gebruikt door Webdeb en groepsbeheerders \
   in het geval dat u zich aansluit bij andere groepen en alleen wordt gebruikt voor het juiste gebruik van het platform. \
   <a href="{0}" target="_blank"> Meer informatie over het privacybeleid. </a>

contributor.place.mail=Uw gebruikersnaam of e-mailadres
contributor.place.mail2=adres@voorbeld.be
contributor.label.mail=E-mailadres (wordt gebruikt als uw inlognaam) *
contributor.label.mail2=E-mailadres *
contributor.error.mail=De opgegeven e-mail is ongeldig.
contributor.error.mail.exist=De opgegeven e-mail is al geregistreerd.

contributor.place.pseudo=mijnPseudo02
contributor.label.pseudo=Uw gebruikersnaam *
contributor.error.pseudo=De gebruikersnaam is verplicht en bedraagt minimum 3 tekens.
contributor.error.pseudo.exist=Deze gebruikersnaam is al in gebruik.
contributor.place.password=********
contributor.label.password=Wachtwoord *
Contributor.help.password=Minimaal 8 tekens lang wachtwoord
contributor.error.password.match=Beide opgegeven wachtwoorden komen niet overeen.
contributor.error.password.length=Het wachtwoord moet minimaal 8 tekens lang zijn.

contributor.place.repassword=********
contributeor.label.repassword=Bevestig wachtwoord *

contributor.place.firstname=Kenneth
contributor.label.firstname=Je voornaam *
contributor.error.firstname=Je voornaam is verplicht.

contributor.place.lastname=Caluwaerts
contributor.label.lastname=Je achternaam *
contributor.error.lastname=Je achternaam is verplicht.

contributor.place.residence=Lijst met landen
contributor.label.residence=Uw land van verblijf

contributor.label.gender=Uw geslacht

contributor.label.birthyear=Geboortejaar
contributor.place.birthyear=1975

contributor.label.affiliation.affiliationsForm=Uw lidmaatschap(pen)
contributor.label.affiliation.help=Uw lidmaatschap is optioneel. De data kunnen uitgedrukt worden als dd/mm/jjjj (complete datum), mm/jjjj (maand/jaar) of jjjj (enkel jaar).

contributor.label.affiliation.function=Functie of functie
contributor.label.affiliation.name=Organisatie
contributor.label.affiliation.start=Startdatum
contributor.label.affiliation.place.start=jjjj, mm/jjjj, dd/mm/jjjj
contributor.label.affiliation.end=Einddatum
contributor.label.affiliation.place.end=jjjj, mm/jjjj, dd/mm/jjjj
contributor.label.affiliation.ongoingdate=Lopende

contributor.label.pedagogic=Ik ben een leraar, student of onderzoeker (ik behoor tot een onderwijs- of onderzoeksgemeenschap) *
contributor.error.pedagogic=Je moet zeggen als je tot een onderwijs- of onderzoeksgemeenschap behoort, of niet.

contributor.label.newsletter=Ik wil mij abonneren op de nieuwsbrief.
contributor.label.newsletter.token=$Vous devez vous désabonner des newsletters manuellement
contributor.label.newsletter.error=$Impossible de se désabonner des newsletters
contributor.label.newsletter.success=$Vous êtes maintenant désabonné aux newsletters
contributor.label.newsletter.unsubscribe=$se désabonner des newsletters

contributor.label.terms=Accepteer onze Algemene voorwaarden *
contributor.accept.terms=Ik accepteer deze algemene voorwaarden.
contributor.error.terms=Je moet de algemene voorwaarden accepteren om je inschrijving te bevestigen.

contributor.change.mail=Je e-mailadres veranderen?
contributor.change.mail.instructions=Vul uw nieuwe e-mailadres in, u ontvangt dan een e-mail van om uw adres te bevestigen.
contributor.change.mail.fail=Er is een fout opgetreden bij het wijzigen van je e-mailadres. Uw adres is niet gewijzigd.
contributor.change.mail.success=We hebben een bevestigingsmail verzonden, klik op de bijgevoegde link om uw nieuwe adres te bevestigen.

contributor.change.password=Verander je wachtwoord?
contributor.change.password.instructions=Vul uw nieuwe wachtwoord twee keer in en druk op de knop "Bevestigen".
contributor.change.password.fail=Er is een fout opgetreden tijdens het opslaan van je nieuwe wachtwoord, het is niet gewijzigd.
contributor.change.password.success=Je hebt je wachtwoord succesvol gewijzigd. Je kunt het de volgende keer dat je inlogt gebruiken.

contributor.recover.password=Wachtwoord vergeten?
contributor.recover.password.title=Herstel je wachtwoord.
contributeor.recover.password.instructions=Vul het e-mailadres voor uw inschrijving in. We sturen u een e-mail om uw wachtwoord opnieuw in te stellen.
contributor.recover.password.success=Volg de instructies in de e-mail die we naar {0} hebben gestuurd om uw wachtwoord te herstellen.
contributor.recover.password.nomailsent=Er is een fout opgetreden tijdens het verzenden van de e-mail met de instructies om uw wachtwoord opnieuw in te stellen. Neem contact op met de beheerder.

contributor.delete.account=Uw account definitief te verwijderen ?
contributor.delete.account.instructions=Deze bewerking is onomkeerbaar. Zodra uw account is verwijderd, worden al uw persoonlijke gegevens behalve \
  uw gerbuikersnaam en uw bijdragen uit onze database verwijderd.
contributor.delete.account.fail=Onjuist wachtwoord.
contributor.delete.account.success=Account verwijdert !
contributor.delete.account.btn=Mijn account verwijderen?
contributor.delete.account.pwd=Onjuist wachtwoord !
contributor.isDeleted=Bijdrager verwijdert

contributor.contributions.recent.true=Hier zijn al uw bijdragen, gerangschikt op datum van de bijdrage.
contributor.contributions.recent.false=Hier zijn al zijn / haar bijdragen, gesorteerd op de datum van de bijdrage.
contributor.contributions.search.addon.true=Zoek een van uw bijdragen
contributor.contributions.search.addon.false=Zoek een van zijn / haar bijdragen
contributor.contributions.search.place.true=Begin met typen om te zoeken tussen uw bijdragen
contributor.contributions.search.place.false=Begin met typen om te zoeken tussen zijn / haar bijdragen

contributor.resendconfmail.title=Stuur de bevestigingsmail ?
contributor.resendconfmail.instructions=Vul uw e-mailadres in, we sturen u een e-mail voor bevestigen het.

contributor.role.0=Kijker
contributor.role.1=Bijdrager
contributor.role.2=Moderator
contributor.role.3=Administrator

#
# dash
#
dash.title=Dashboard
dash.lastentries.title=Laatste actieve bijdragen in groep {0}
dash.lastentries2.title.1=Laatste actieve bijdragen
dash.lastentries2.title.2=in groep {0}
dash.stat.title=Aantal bijdragen
dash.lastentries.see.title.1=$Voir toutes les dernières contributions

#
# new contributions
#
entry.title=Bijdragen aan WebDeb
entry.new=Voeg een nieuwe bijdrage toe
entry.refine=Van een bestaande bijdrage
entry.new.actor.btn=Actor toevoegen
entry.new.text.btn=Tekst toevoegen
entry.new.debate.argument.btn=Participate in a debate
entry.new.text.argument.btn=Construct the structure of a text
entry.new.link.btn=Link valideren
entry.new.tag.btn=Voeg een dossier toe
entry.new.debate.btn=Voeg een debat toe
entry.new.citation.btn=Voeg een quote toe
entry.validate=Valideren van automatische feeds
entry.validate.arg.btn=Valideer de mening

entry.excerpt.existing.citation=Zoek een quote die al ingevoerd werd in WebDeb
entry.excerpt.existing.text=Zoek de tekst die de quote bevat in Webdeb
entry.excerpt.new.text=Encodeer een nieuwe tekst in Webdeb om er vervolgens een quote uit te halen
entry.excerpt.new.externaltext.chrome=Gebruik de Chrome extensie om rechtstreeks de quote van internet te halen.
entry.excerpt.new.externaltext.firefox=Gebruik de Firefox extensie om rechtstreeks de quote van internet te halen.

excerpt.original.excerpt=*Extrait brut
excerpt.working.excerpt=*Extrait retravaillé

entry.citation.new=$Créer une nouvelle citation
entry.citation.modify=$Modifier une citation existante
entry.citation.edit=$Editer la citation

entry.citation.original.label=$Extrait original
entry.citation.working1.label=$Supprimez du texte ci-dessous les parties en trop
entry.citation.working2.label=$Ajouter les informations supplémentaires entre crochets

entry.citation.text.source.label=$La citation provient-elle…
entry.citation.text.source.internet.label=$d’une page web
entry.citation.text.source.pdf.label=$d’un pdf
entry.citation.text.source.other.label=$d’une autre source
entry.citation.text.source.internet.url.label=$URL
entry.citation.text.source.pdf.urlOrTitle.label=$URL ou titre du texte
entry.citation.text.source.other.title.label=$Titre du texte

entry.citation.authors.title=$Qui tient les propos mentionnés dans la citation ?
entry.citation.authors.person.label=$Une ou des personnes ?
entry.citation.authors.org.label=$Une ou des organisations ?
entry.citation.authors.textauthors.label=$S’agit-il des auteurs du texte ?
entry.citation.authors.textauthors2.label=$Les auteurs du texte ?
entry.citation.authors.person.label2=$Une ou plusieurs autres personnes ?
entry.citation.authors.org.label2=$Une ou plusieurs autres organisations ?

entry.citation.tags.title=$Classez la citation dans WebDeb
entry.citation.tags.label=$A quelle(s) question(s) répond la citation ?
entry.citation.tags.tag.label=$Que dit-on sur
entry.citation.citedauthors.label=$La citation parle-t-elle d’une personne ou d’une organisation ?
entry.citation.places.label=$La citation concerne-t-elle un ou des térritoires ?
entry.citation.skip.form=$Pressé ? Vous pouvez enregistrer… mais il ne reste que 3 étapes

entry.citation.workingExcerpt.label1=$Voici l’extrait que vous voulez insérer. Souhaitez-vous supprimer des parties inutiles ?
entry.citation.workingExcerpt.label2=$Faut-il remplacer des pronoms ou ajouter des informations pour rendre l''extrait plus compréhensible hors contexte ?

entry.citation.text.label=$Vous pouvez vérifier et compléter les informations sur le texte
entry.citation.position.change.shade.label=$Changer la position
entry.citation.argument.transform.label=$Résumer en une affirmation

entry.citation.selection.title=$Ajouter une citation
entry.citation.selection.new.title=$Ajouter une nouvelle citation
entry.citation.selection.search.title=$Rechercher une citation sur WebDeb
entry.citation.selection.newone.title=$Sur WebDeb
entry.citation.selection.newfromtext.title=$Depuis un texte
entry.citation.selection.newonweb.title=$Depuis le Web

entry.citation.selection.0.title=$author
entry.citation.selection.1.title=$texte
entry.citation.selection.2.title=$débat
entry.citation.selection.3.title=$tag
entry.citation.selection.4.title=$extrait
entry.citation.selection.5.title=$mes citations

entry.citation.selection.0.place=$nom d''un acteur lié à la citation
entry.citation.selection.1.place=$titre ou url d''un texte d''où provient la citation
entry.citation.selection.2.place=$titre du débat dans lequel la citation et reprise
entry.citation.selection.3.place=$nom du tag avec lequel la citation à été taguée
entry.citation.selection.4.place=$extrait se trouvant dans la citation
entry.citation.selection.5.place=$extrait se trouvant parmis les citations que vous avez ajoutées

#citations errors
citation.error.original.blank=Het fragment van de tekst ontbreekt
citation.error.original.length=Het fragment van de tekst mag niet meer dan 600 tekens bevatten

citation.error.working.blank=$L''extrait retravaillé est manquant
citation.error.working.length=De quote uit het fragment mag niet meer dan 600 tekens bevatten
citation.error.working.notcorresponding=Het herwerkte fragment komt niet overeen met het oorspronkelijke fragment


entry.select.group.desc=U kunt de groep wijzigen waarin deze bijdrage wordt gepubliceerd.

entry.import.group=Importeren in groep
entry.import.group.desc=U kan deze bijdrage in uw groep importeren. Het volstaat er een te selecteren en uw keuze te bevestigen.
entry.import.success=Deze bijdrage is succesvol geïmporteerd in groep {0}.
entry.import.fail=Er is een fout opgetreden, deze bijdrage is niet geïmporteerd in groep {0} {1}.

entry.still.errors=Er zijn fouten in een of meer van de door jou ingediende formulieren, corrigeer ze.
entry.delete.confirm.title=Weet u zeker dat u dit item wilt verwijderen?
entry.delete.confirm.text=Bevestig dat u het geselecteerde element uit de lijst wilt verwijderen.
entry.delete.cancel=Annuleer verwijdering
entry.delete.confirm=Bevestig verwijdering

# generic error messages
entry.error.url.format=Het opgegeven adres ziet er niet geldig uit (van het formulier "http://www.somesite.com")
entry.error.actor.noname=Je moet de naam van de acteur invullen als je een functie hebt ingevuld

# affiliation errors
affiliation.error.function.format=Ingegeven functie heeft een ongeldige indeling.
affiliation.error.affname.format=Ingegeven affiliatie heeft een ongeldig formaat.
affiliation.error.missing.affiliation=U moet tenminste een functie of organisatie opgeven als u een datum hebt ingevuld.
affiliation.error.missing.person=De naam van de persoon is verplicht.
affiliation.error.date.format=De datum moet zijn zoals 2005, 01/2005 of 15/01/2005.
affiliation.error.date.order=De startdatum van de affiliatie mag niet later zijn dan de einddatum.
affiliation.error.person.start=De startdatum is mogelijk niet vóór de geboortedatum.
affiliation.error.person.end=De einddatum mag niet later zijn dan de datum van overlijden.
affiliation.error.org.start=De startdatum is mogelijk niet eerder dan de aanmaakdatum van deze organisatie.
affiliation.error.org.end=De einddatum mag niet later zijn dan de einddatum van de organisatie.
affiliation.error.datetype=U moet een datumprecisie selecteren.
affiliation.error.before.born.start=De startdatum is mogelijk niet vóór de geboortedatum.
affiliation.error.after.death.end=De einddatum mag niet later zijn dan de datum van overlijden.
affiliation.error.before.creation.start=De startdatum van de affiliatie is mogelijk niet vóór de aanmaakdatum van de organisatie;
affiliation.error.after.termination.end=De einddatum van de affiliatie mag niet later zijn dan de einddatum van de organisatie.
affiliation.error.notperson=De actor is mogelijk niet gelieerd aan een persoon.
affiliation.error.notself=De actor is mogelijk niet aan zichzelf gelieerd.
affiliation.error.oldself=U mag de naam van de vorige actor niet gebruiken als naam van de aangeslotene. Om dit te doen, moet u eerst de naam bijwerken en vervolgens de voorkeuren in twee stappen.
affiliation.error.type.unset=Het type aansluiting is verplicht.
affiliation.error.type.wrong=Het type aansluiting is ongeldig voor connecties met personen (alleen de "... owned by" zijn toegestaan)

# argument
argument.route.noarg=Geen quote is ingevuld.
argument.route.cancel=Er is geen nieuwe link ingediend (je hebt je aanvraag geannuleerd).
argument.route.arg.cancel=U heeft geen enkele wijziging in de mening ingediend.
argument.route.error=Het lijkt erop dat je hebt geprobeerd een mening in te dienen dat je al in de database hebt opgeslagen, of je hebt te lang gewacht voordat je je wijzigingen hebt verzonden.

argument.excerpt.notfound=Het gekopieerde quote hoort niet bij de tekst.
argument.not.found=Geen details over het geselecteerde mening zijn te vinden in de databank.
argument.text.notfound=De geselecteerde tekst waarmee je hebt gevraagd om mee te werken is niet gevonden in de databank.

argument.properties.oops=Er is iets fout gegaan, vul de eigenschappen van dit mening opnieuw in.
argument.properties.added=De mening is succesvol toegevoegd aan de databank

argument.new.title=Voeg een nieuwe quote toe
excerpt.edit.title=Wijzig de quote
argument.edit.title=Wijzig de mening
argument.modify.title=Opslaan
argument.new.instructions.1=Converteer uw quote in een mening.
argument.new.instructions.2=Converteer uw quote in een mening.
argument.new.instructions.3=Selecteer een klasse voor uw mening.
argument.new.instructions.4=Bevestig dat je een link tussen deze meningen wilt definiëren
argument.label.excerpt=Geselecteerd quote
argument.label.author=Auteur(s) van de quote *
argument.label.author.name=Naam
argument.label.author.place.name=Persoon of organisatie
argument.label.author.place.function=Functie of machtiging
argument.label.author.function=(optioneel)
argument.label.author.place.affname=affiliatie
argument.label.author.affname=(optioneel)
argument.label.reporter=Acteur(s) van wie men de woorden citeert
argument.label.reporter.name=Naam
argument.label.reporter.place.name=Persoon of organisatie
argument.label.reporter.function=Functie (optioneel)
argument.label.reporter.place.function=Functie of machtiging
argument.label.reporter.affname=Affiliatie (optioneel)
argument.label.reporter.place.affname=Affiliatie
argument.label.arg=Acteur(s) over wie men praat in de mening
argument.label.arg.place.name=Persoon of organisatie
argument.label.cited=Acteur(s) over wie men praat in de quote
argument.label.cited.place.name=Persoon of organisatie
argument.label.cited.function=Functie (optioneel)
argument.label.cited.place.function=Functie of machtiging
argument.label.cited.affname=Affiliatie (optioneel)
argument.label.cited.place.affname=Affiliatie
argument.help.author=Vul de naam (namen) in van de actoren die de mening zegt
argument.help.reporter=Vul de naam (namen) in van de actoren die de mening rapporteren
argument.help.cited=Vul de naam (namen) in van de actoren die in de mening worden geciteerd
argument.error.noauthor=Er moet minimaal één auteur zijn opgegeven
argument.error.authorisreporter=Een auteur mag ook geen verslaggever zijn.
argument.error.authoriscited=Een geciteerde actor mag ook geen auteur of verslaggever zijn.

debate.fulltitle.label=$Rédigez la question du débat
argument.standard.shade.label=Begin van de mening
debate.standard.shade.label=$Début de la question
debate.standard.shade.place=$Choisissez le début de débat
argument.standard.part.label=Tweede meninggedeelte
debate.standard.part.label=Tweede gedeelte van de kwestie
argument.standard.part.warning.words=Voor een beter begrip van de verklaring zonder terug te moeten naar de tekst, wordt u gevraagd om de voornaamwoorden te vervangen \
door wat ze betekenen. Corrigeer uw zin, tenzij het voornaamwoord verwijst naar een ander element dat erin is opgenomen.

argument.fake.title=$Autres citations

argument.debate.2.start=$les points fort et faibles de
argument.debate.2.end=$sont discutés

debate.type.simple=$réponse simple
debate.type.multiple=$Le débat a t-il de mutiple réponses ?
debate.edit.select.label=$Sélectionnez un type de debat
debate.edit.title.label=$Merci de rédiger la suite de votre question (max : XXX)
debate.edit.fulltitle.label=$Merci de rédiger votre question (max : XXX)

debate.edit.ambiguity.title=$Voici les débats déjà encodés dans WebDeb ayant un intitulé similaire. Un de ces débats, déjà dans WebDeb, coïncide-t-il avec le vôtre ?
debate.edit.ambiguity.yes.label=$Oui ? Sélectionnez-le :
debate.edit.more.title=$Quelques informations sur votre débat (par exemple pour définir un terme, présenter le projet en débat, donner quelques éléments du contexte)
debate.edit.more.description.label=$Texte libre (maximum 1500 signes)
debate.edit.more.description.error=$La description doit faire maximum 1500 signes
debate.edit.more.externalurls.title=$Hyperliens
debate.edit.more.externalurls.label=$Hyperliens pour en savoir plus sur ce débat
debate.edit.more.externalurls.url.label=$URL
debate.edit.more.externalurls.url.place=$Exemple : https://fr.wikipedia.org/wiki/Albert_Einstein
debate.edit.more.externalurls.alias.label=$Titre
debate.edit.more.externalurls.alias.place=$Exemple : Page wikipédia d'Albert Einstein
debate.edit.more.image.label=$Ajouter une illustration au débat

debate.edit.tags.places.label=$Le débat concerne-t-il un ou des térritoires particuliers ?
debate.edit.tags.authors.label=$Le débat porte-t-il sur un ou plusieurs acteurs (personnes ou organisations) ?
debate.tags.label=$A quelle(s) question(s) répond le débat ?
bubble.entry.tags.debate.tags.label=$Exemple : pour le débat "faut-il ou non ajouter des bulles explicatives dans les \
  formulaires de WebDeb", je pourrais indiquer "que dit-on sur les bulles d''aides explicatives" et \
  "que dit-on sur les formulaires WebDeb".

context.dragndrop.title=$Déplacer les arguments et citations
context.dragndrop.title2=$Classer les citations
context.dragndrop.subtitle=$Vous pouvez déplacer les citations d’une colonne à l’autre et créer de nouveaux dossiers
context.dragndrop.reload.title=$Recharger
context.dragndrop.close.title=$Fermer la classification

context.alliesopponents.title=$Afficher par auteurs des arguments
context.alliesopponents.close.title=$Fermer l'affichage par auteurs

debate.new.added=$Le débat a été ajouté avec succès !

argument.help.topic=U kunt toevoegen of verwijderen door ze te scheiden met een komma of met behulp van de enter-toets. U kunt onderwerpen maken die niet worden voorgesteld wanneer u aan het invullen bent
argument.error.topic=Er moet minstens één onderwerp worden gespecificeerd
argument.label.type=Type mening * (Bedankt voor het controleren van de relevantie van onze automatische suggestie).
argument.label.type.0=Constatief.
argument.label.type.1=Prescriptief.
argument.label.type.2=Opinie.
argument.label.type.3=Performatief.
argument.label.type.desc.0=<i> The auteur denkt dat het waar is, het is niet waar dat, ... <i />
argument.label.type.desc.1=<i> De auteur vindt dat er iets / iemand moet / moet zijn, ... <i />
argument.label.type.desc.2=<i> De auteur houdt van, haat, voelt, ... <i />
argument.label.type.desc.3=<i> De auteur belooft, beslist, commit zichzelf, orders, ... <i />
argument.error.type=Het meningtype is verplicht
argument.label.type.suggestion.other=U kunt onze voorgestelde categorie uiteindelijk veranderen door:
argument.label.type.error=De annotatieservice is offline ...

argument.label.timing=Mening timing *
argument.label.timing.sumup=type timing
argument.error.timing=De timing is verplicht

argument.label.subtype.2=Type gevoel
argument.label.subtype.Opinion=Type gevoel
argument.place.subtype.2=Selecteer het type gevoel
argument.help.subtype.2=Selecteer een van de bovenstaande alternatieven voor het gevoel dat overeenkomt met het geselecteerde fragment

argument.label.subtype.3=Type performatief
argument.label.subtype.Performative=Type performatief
argument.place.subtype.3=Selecteer het type performatief
argument.help.subtype.3=Selecteer een van de bovenstaande alternatieven als het type performatief dat overeenkomt met het geselecteerde fragment

argument.error.subtype=Een subtype voor het gekozen type is verplicht

argument.label.shade=Nuance
argument.place.shade=Selecteer
argument.place.pronoun=Selecteer het onderwerp van betekenis
argument.help.shade=Selecteer uit de bovenstaande alternatieven de verbale nuance die overeenkomt met het geselecteerde fragment
argument.error.shade=De nuance is verplicht

argument.label.standard=Quote uit het fragment hierboven *
argument.label.standard.edit=Een gestandaardiseerde vorm van het quote (zonder punt "te beëindigen". ")
argument.label.techexcerpt=Wijzig de tekst enkel als dit nodig is om hem \ verstaanbaar of korter te maken. Gelieve de denkbeelden van de auteur nooit te vertekenen en alle wijzigen tussen haakjes[…]te plaatsen 
argument.place.standard=De migranten ontvangen
argument.place.standard2=Houd er rekening mee dat u prioriteit geeft aan de volgorde "subject-werkwoord-objecten", om het voornaamwoord te vervangen door de betekenis ervan en om een eenvoudig begrijpbaar mening uit de oorspronkelijke tekst te halen.
argument.error.standard=Het gestandaardiseerde formulier is verplicht

argument.title.place=Betrokken plaats(en)*
argument.label.place=Place
argument.label.place.sumup=Place
argument.place.place=Een continent, een land, een gebied, een stad, etc.
argument.help.place=Vul een plaats in waar deze mening van toepassing is
argument.help.placename=Geef de naam van de plaats aan, er worden dan verschillende voorstellen gedaan, het is dan nodig om uit de voorgestelde lijst te kiezen
argument.error.placename=De plaats wordt meerdere keren aangegeven

argument.label.time=Vul een datum of periode in waar dit mening voor staat (optioneel)
argument.label.time.sumup=Datum van de periode
argument.place.time.from=Een datum, een begin van een periode of tijd, etc.
argument.place.time.to=Een andere datum, einde van periode of tijd, etc.
argument.help.time=Vul een datum of periode in waar dit mening voor staat

argument.links.transform_from_excerpt=$Voulez-vous résumer la citation que vous venez d''encoder en une opinion ?

argument.justification.exists.text=$L''argument existe déjà dans cette catégorie.
argument.links.label.excerpt.other=Fragment waaruit het te linken mening afkomstig is
argument.links.label.standard=Standaardformulier
argument.links.label.standard.other=Standaardvorm van het te linken mening
argument.label.links.link=Type relatie
argument.label.tag=Tags *
argument.label.tag.place.name.0=Tag 1
argument.label.tag.place.name.1=Tag 2
argument.label.tag.place.name.2=Tag 3

argument.links.error.toarg=De mening is verplicht
argument.links.error.linktype=Het type link is verplicht
argument.links.error.linkshade=De nuance is verplicht
argument.links.alreadyexist=Deze link bestaat al

argument.links.oops=Er is iets fout gegaan. Vul de links voor deze mening opnieuw in.
argument.links.added=De link tussen de meningen is succesvol toegevoegd.
argument.links.not.added=Er is een fout opgetreden, de link tussen de meningen is niet toegevoegd aan de databank.
argument.links.empty=Er is geen link tussen meningen gedefinieerd

argument.links.delete.confirm.title=Weet u zeker dat u de link wilt verwijderen?
argument.links.delete.confirm.text=Bevestig het verwijderen van de link naar mening "{0}"

# argument link types and shades
argument.links.type.-1=geen link
argument.links.type.0=gelijkenis
argument.links.type.1=rechtvaardiging
argument.links.shade.0=analoog
argument.links.shade.1=kwalificeert
argument.links.shade.2=is tegen
argument.links.shade.3=ondersteunt
argument.links.shade.4=nuanceert
argument.links.shade.5=betwist
argument.links.shade.6=wordt ondersteund door
argument.links.shade.7=is gekwalificeerd door
argument.links.shade.8=wordt betwist door

argument.links.add.search=Zoek een bestaande mening
argument.links.add.create=Creëer een nieuwe mening
argument.links.add.validate=Valideer een mening
argument.links.validate.noresults=Geen voorstel beschikbaar

argument.excerpt.links.add.search=Kies een van de quotes hieronder
argument.excerpt.links.add.create=Haal een nieuwe quote uit de tekst
argument.excerpt.links.add.validate=Valideer een quote

argument.links.citation.instructions=Markeer het gekozen fragment en klik op de knop « toevoegen » die tevoorschijn komt tijdens het markeren.
argument.links.justification.instructions=Selecteer een van de hierboven vermelde voorstellen en klik op de knop "creëer de link".\
Indien geen enkel voorstel geschikt is, klik dan op "creëer een nieuwe mening".
argument.links.shade.justification.label=Type link : 
argument.links.shade.justification.form=Kies het antwoord
argument.links.shade.2.form=Voeg een argument toe dat de mening verdedigt
argument.links.shade.3.form=Voeg een argument toe dat de mening betwist
argument.links.shade.illustration=Type link : 
argument.links.shade.illustration.form=Kies het antwoord
argument.links.shade.4.form=wordt geïllustreerd door
argument.links.shade.5.form=wordt genuanceerd door
argument.links.shade.6.form=wordt betwist door

argument.delete.confirm.title=Weet je zeker dat je de mening wilt verwijderen?
argument.delete.confirm.text=Bevestig de verwijdering van de mening "{0}"

argument.delete.success=De mening is verwijderd
argument.delete.error=Er is een fout opgetreden bij het verbinden met de database, de mening is niet verwijderd.

argument.sumup.actorname=betrokken actor (en)
argument.sumup.author=auteur
argument.sumup.role=rollen van actoren
argument.sumup.firstArgument=Is de overkoepelende mening van het debat :
argument.sumup.contextIsText=bevindt zich in de argumentatieve structuur van de tekst :
argument.sumup.contextIsDebate=bevindt zich in de argumentatieve structuur van het debat :
argument.sumup.type=Type
argument.sumup.excerpt=quote
# actors
entry.actor.auto=U heeft net de naam van een voor ons onbekende actor ingevoerd. Wilt u zijn fiche vervolledigen?
entry.actor.from.signup=U geregistreerd bij affiliatie (s) we nog niet weet, zou u in enkele details over hen in te vullen?
entry.actor.title.person=Een persoon
entry.actor.title.org=Een organisatie
entry.actor.new=Voeg een nieuwe actor toe
entry.actor.create=Actor toevoegen
entry.actor.new.unknown=Vul de details in met betrekking tot
entry.actor.edit.btn.tooltip=Bewerk de eigenschappen van deze acteur
entry.actor.instructions=Klik op het type acteur dat je wilt invullen en voeg wat details toe.
entry.actor.modify=Bewerk details van
entry.actor.modify.btn=Update actor
entry.actor.modifyin.btn=Actor bijwerken in ...
entry.actor.notnow=Nu niet
entry.actor.new.description=Vul de persoonlijke gegevens over een actor in de kennisbank van WebDeb in
entry.actor.save=Je hebt succesvol een actor ingediend of bijgewerkt:
entry.actor.canceled=Je hebt geen enkele wijziging voor actor {0} opgeslagen
entry.actor.existing.title=Update een bestaande actor?
entry.actor.existing.text=U selecteerde een bestaande actor uit de database, wilt u zijn gegevens bijwerken of een nieuwe aanmaken (naamgenoot)?
entry.actor.namesake.title=Acteur al bekend?
entry.actor.namesake.desc=Je hebt de naam ingevoerd van een actor die we al kennen. Bekijk hieronder de actor(en) \
   of je hem / haar niet terugvindt en deze actor in plaats daarvan wilt gebruiken. Als geen enkel voorstel overeenkomt, bevestigt u eenvoudigweg dat u \
   een nieuwe (naamgevende) actor wil aanmaken.
entry.actor.existing.ok=Wijzig bestaande actor
entry.actor.existing.use=Gebruik bestaande actor
entry.actor.existing.nok=Maak een nieuwe actor aan

actor.label.actortype=Selecteer het soort acteur *
actor.label.actortype.-1=Onbekend
actor.label.actortype.0=Individuele persoon
actor.label.actortype.desc.0=Een persoon
actor.label.actortype.1=Organisatie (of project, merk, evenement, ...)
actor.label.actortype.desc.1=Een organisatie zoals een bedrijf, openbare entiteit, project, merk, evenement, etc.
actor.label.unknowntype=Deze acteur is van onbekend type.
actor.error.actortype=Selecteer een type voor je acteur

actor.label.fullname=Naam van de actor in {0} *
actor.place.lastname=Smith
actor.label.lastname=Achternaam
actor.place.name=John
actor.label.name=Voornaam
actor.place.pseudo=De koning of JFK
actor.label.pseudo=Pseudoniem of acroniem
actor.error.nofirst=Als u een achternaam hebt ingevuld, moet u een voornaam invullen
actor.error.nolast=Als u een voornaam hebt ingevuld, moet u een achternaam invullen
actor.error.noname=Je moet een naam invullen
actor.help.name=De eerste en laatste namen, het pseudoniem of alle drie de velden moeten worden ingesteld

actor.label.officialnumber=Officieel nummer

bubble.entry.actor.officialnumber=nummer in 9 cijfers dat door openbare inmeningen aan organisaties wordt gegeven. Geen interpunctie tussen cijfers.
actor.place.crossref=https: //en.wikipedia.org/wiki/John_Smith_ (Ohio_Senator)
actor.label.crossref=Wikipedia-pagina, of indien geen, de website van actor
actor.help.crossref=De Wikipedia-webpagina van de actor, of indien niet, zijn / haar persoonlijke webpagina

actor.label.affiliation.start=Startdatum
actor.label.affiliation.end=Einddatum
actor.label.affiliation.place.starttype=Nauwkeurigheid van de datum
actor.label.affiliation.place.endtype=Nauwkeurigheid van de datum

actor.label.affiliation.person.affiliations=Functies en voorkeuren van de actoren
actor.label.affiliation.person.affiliationsForm=Functies en voorkeuren van de actoren
actor.label.affiliation.person.name=Naam van de organisatie
actor.label.affiliation.person.function=Functie of jobtitel

actor.label.affiliation.qualification.qualifications=Opleidingen gevolgd door de persoon
actor.label.affiliation.qualification.qualificationsForm= Opleidingen gevolgd door de persoon
actor.label.affiliation.qualification.name=Naam van de opleidingsorganisatie
actor.label.affiliation.qualification.function=Naam van de opleiding

actor.label.affiliation.filiation.parents=$A comme parents
actor.label.affiliation.filiation.children=$A comme enfants
actor.label.affiliation.filiation.parentsForm=Filiatie van de persoon
actor.label.affiliation.filiation.name=Voornaam en achternaam van vader of moeder

actor.label.affiliation.org.affiliations=Verbonden aan organisaties of individuele aandeelhouders
actor.label.affiliation.org.affiliationsForm=Verbonden aan organisaties of individuele aandeelhouders
actor.label.affiliation.org.orgaffiliations=Verbonden aan organisaties of individuele aandeelhouders
actor.label.affiliation.org.orgaffiliationsForm=Verbonden aan organisaties of individuele aandeelhouders
actor.label.affiliation.org.name=Naam van de organisatie, het project, het product of de gebeurtenis (ofwel van de persoon die een aandeelhouder is of een bedrijf heeft)
actor.label.affiliation.org.afftype=Affiliatietype
actor.place.affiliation.org.afftype=Kies een type
actor.label.affiliation.org.start=Startdatum

actor.label.affiliated.person.affiliations=Geaffilieerde personen en hun functies
actor.label.affiliated.person.name=Naam van de organisatie, het project, het product of de gebeurtenis
actor.label.affiliated.person2.name=Naam van persoon
actor.label.affiliated.person.function=Function of jobtitel
actor.label.affiliated.person2.function=Function of jobtitel
actor.label.affiliated.person.afftype=Affiliatietype
actor.place.affiliated.person.afftype=Kies een type

actor.label.affiliated.org.orgaffiliations=Geaffilieerde organisaties, merken, projecten, evenementen
actor.label.affiliated.org.name=Naam van de organisatie, het project, het product of de gebeurtenis (ofwel van de persoon die een aandeelhouder is of een bedrijf heeft)
actor.label.affiliated.org.afftype=Affiliatietype
actor.place.affiliated.org.afftype=Kies een type

actor.aff.ownedby.label=Eigendom van of gefinancierd door
actor.aff.owns.label=Bezit of financiert

actor.aff.no.label=Geen resultaat...

actor.label.birthdate=Geboortedatum
actor.label.deathdate=Datum van overlijden
actor.place.date=jjjj, mm/jjjj, dd/mm/jjjj
actor.error.date.format=De opgegeven datum heeft een ongeldig formaat
actor.error.date.after.person=De datum van overlijden moet later zijn dan de geboortedatum.
actor.error.date.after.org=De datum van ontbinding moet later zijn dan de datum van creatie.

actor.label.gender=geslacht

actor.place.residence=Lijst met landen
actor.label.residence=Verblijfsland

actor.place.orgname=Verenigde Naties
actor.label.orgname=Naam van de organisatie (indien mogelijk in {0}) *
actor.label.orgname.simple=Naam van de organisatie
actor.error.orgname=De naam van de organisatie is verplicht

actor.place.acronym=VN
actor.label.acronym=acroniem
actor.help.acronym=De verkorte naam of het acroniem van de organisatie (geen punt, spaties of andere scheidingstekens)
actor.error.acronym=Een acroniem mag geen interpunctie bevatten.

actor.place.orgcrossref=https: //en.wikipedia.org/wiki/United_Nations
actor.label.orgcrossref=Wikipedia-pagina URL of, indien geen, de website van de organisatie
actor.help.orgcrossref=De persoonlijke webpagina van de organisatie, of op Wikipedia
actor.error.orgcrossref.format=Het opgegeven adres ziet er niet goed uit ((van het formulier "http://www.woldwide-organization.com")

actor.label.legal=Type organisatie
actor.label.sector=Sector(en) waar de organisatie actief is
actor.label.allsectors=Controleer alle sectoren

actor.label.creation=Aanmaakdatum
actor.label.termination=Oplossingsdatum

viz.actor.tags.label=Sociale sector
actor.label.headoffice=Locatie van het hoofdkantoor (land, regio of stad)

actor.label.namelang=Taal
actor.place.namelang=Kies een taal
actor.error.namelang=U moet een taal selecteren voor de spelling van de naam van de actor.
actor.error.lang.twice=Je mag niet twee keer dezelfde taal selecteren.
actor.error.gender.twice=Je mag niet twee keer hetzelfde geslacht selecteren voor dezelfde taal.
actor.label.showspellings.person=Vertaal de naam van deze actor in andere talen.
actor.label.showspellings.org=Vertaal de naam van deze organisatie naar een andere taal.
actor.label.showoldnames.org=Bewerk eerdere namen van deze organisatie.
actor.label.spellings=Andere vertalingen.
actor.help.spellings=U mag meer vertalingen toevoegen voor de naam van deze actor.
actor.label.oldnames=Eerdere namen
actor.help.oldnames=U kunt de vorige naam van deze organisatie toevoegen, in het geval dat deze hernoemd werd.

actor.place.gender=Kies een geslacht

actor.sumup.affiliation.person=functie (s)
actor.sumup.affiliation.org=affiliatie (s)
actor.sumup.country=nationaliteit (en)
actor.sumup.status=juridische status
actor.sumup.sector=bedrijfstak (en)
actor.sumup.crossref=webpagina

actor.role.author=auteur
actor.role.authors=auteurs
actor.role.reporter=reporter
actor.role.reporters=reporters
actor.role.source=bronauteur
actor.role.citeds=geciteerde acteurs
actor.role.none=geciteerde acteur
actor.role.reported=gerapporteerde opmerkingen
actor.role.cosigned=mede-ondertekende opmerkingen
actor.role.signed=ondertekende reacties
actor.role.as=as

actor.authors.others.label=$et {0} autre(s)

# custom messages for deletion of dynamic form elements (last element of name constructed from manageAddRemoveButton)
actor.delete.confirm.affiliationsForm.text=Bevestig de verwijdering van de affiliatie "{0}".
actor.delete.confirm.affiliationsForm.title=Weet u zeker dat u deze aansluiting wilt verwijderen?
actor.delete.confirm.orgaffiliationsForm.title=Weet u zeker dat u deze aansluiting wilt verwijderen?
actor.delete.confirm.orgaffiliationsForm.text=Bevestig de verwijdering van de affiliatie "{0}".
actor.delete.confirm.authors.text=Bevestig de verwijdering van de auteur "{0}".
actor.delete.confirm.authors.title=Weet je zeker dat je deze auteur wilt verwijderen?
actor.delete.confirm.sourceAuthors.text=Bevestig de verwijdering van de bronauteur "{0}".
actor.delete.confirm.sourceAuthors.title=Weet u zeker dat u deze bron auteur verwijderen?
actor.delete.confirm.reporters.text=Bevestig de verwijdering van de reporter "{0}".
actor.delete.confirm.reporters.title=Weet u zeker dat u deze reporter wilt verwijderen?
actor.delete.confirm.citedactors.text=Bevestig de verwijdering van de genoemde actor "{0}".
actor.delete.confirm.citedactors.title=Weet je zeker dat je deze geciteerde actor moet verwijderen?
actor.delete.confirm.spresents.text=Bevestig de verwijdering van de vertaling "{0}".
actor.delete.confirm.spellings.title=Weet u zeker dat u de vertaling van deze naam moet verwijderen?
actor.delete.confirm.orgspellings.text=Bevestig de verwijdering van de vertaling "{0}".
actor.delete.confirm.orgspellings.title=Weet u zeker dat u de vertaling van deze naam moet verwijderen?
actor.delete.confirm.oldnames.text=Bevestig de verwijdering van de vorige naam "{0}".
actor.delete.confirm.oldnames.title=Weet u zeker dat dit de vorige naam te wissen?
actor.businessSectors.delete.confirm.text=Bevestig de verwijdering van het bedrijfsleven "{0}"
actor.businessSectors.delete.confirm.title=Weet u zeker dat u deze sector wilt verwijderen?

# texts
entry.text.new=Importeer een nieuwe tekst
entry.text.modify=Wijzig een bestaande tekst
entry.text.new.btn=Nieuwe tekst
entry.text.new.description=Voeg een nieuwe tekst of uittreksel toe aan de WebDeb
entry.text.submit=Voeg nieuwe tekst toe in ...
entry.text.btn.modify=Wijzig tekst
entry.text.success=Uw tekst is opgeslagen in de kennisbank
entry.text.edit.btn.tooltip=Eigenschappen van deze tekst bewerken
entry.text.existing.title=Update een bestaande tekst?
entry.text.existing.text=Je hebt een bestaande tekst uit de database geselecteerd, wil je zijn gegevens bijwerken of een nieuwe maken (met dezelfde titel)?
entry.text.existing.ok=Wijzig bestaande tekst
entry.text.existing.nok=Maak een nieuwe tekst

entry.text.selection.new.title=$Ajouter un nouveau texte
entry.text.selection.search.title=$Rechercher un texte sur WebDeb

entry.text.argument.structure.toggle=$Montrer / cacher la structure
entry.text.argument.structure.first=$Ajouter une affirmation faîtière

text.visibility.0=$public
text.visibility.1=$usage pédagogique uniquement
text.visibility.2=$usage privé

text.label.import.content=$Importer le contenu du texte
text.label.delete.content=$Supprimer le contenu du texte
text.label.content=Inhoud van tekst
text.label.nocontent=Aan deze tekst is geen inhoud toegevoegd
text.label.nopreview=Er kan geen voorbeeld worden weergegeven voor deze tekst ...
text.label.upload=PDF-bestand uploaden
text.label.upload.update=$Modifier le fichier PDF
text.label.pdf.current=$Fichier PDF actuel :
text.label.upload.error=Bestand mag niet groter zijn dan 50MB
text.label.visibility=Inhoud is zichtbaar voor het volgende gebruik *
text.error.visibility=U moet een zichtbaarheid voor de inhoud selecteren
text.error.copyright=$Nous ne pouvons pas importer le contenu de l''url que vous avez encodée \
car il ne s''agit pas d''un source que nous avons authentifiée comme libre de droits.
text.label.content.notviewable=Inhoud kan niet worden bekeken vanwege zichtbaarheidbeperkingen.
text.title.notviewable=Content zichtbaarheid beperking
text.desc.notviewable=De inhoud van deze tekst kan niet worden bekeken, omdat de zichtbaarheid ervan "pedagogisch" is en u \
   niet geregistreerd bent als onderdeel van de academische wereld, of de inhoud is toegevoegd als "privé door een andere bijdrager. <br> \
   U kunt desgewenst de zichtbaarheid van deze inhoud wijzigen en / of inhoud aan deze tekst toevoegen. Als de zichtbaarheid \
   is ingesteld op "privé" u zult de enige zijn die toegang heeft tot de volledige inhoud (maar anderen kunnen er nog steeds een overzicht van zien).

text.place.area=Voeg hier uw tekst toe.
text.error.area=Vul wat tekstinhoud in of upload een bestand
text.error.area.oldtext=De inhoud die je hebt ingediend bevat niet meer de oude inhoud die is geannoteerd. <br> & nbsp; Zorg ervoor dat je geannoteerde inhoud niet verwijdert.
text.error.notfound=Er is een onverwachte fout opgetreden bij het wijzigen van de inhoud van deze tekst. Annuleer en probeer het opnieuw.

text.place.title=Een titel
text.label.title=Volledige teksttitel
text.help.title=De volledige teksttitel van de geïmporteerde tekst
text.error.title=De titel is verplicht
text.error.length.title=$Le titre ne peut dépasser 255 charactères
text.error.title.name=Gelieve een titel te coderen
text.error.title.lang=Selecteer een taal
text.delete.confirm.text=Bevestig de verwijdering van de vertaling "{0}".
text.delete.confirm.title=Weet u zeker dat u deze vertaling wilt verwijderen?
text.extract.msg=Deze tekst is automatisch geëxtraheerd. Controleer voordat u het opslaat of het overeenkomt met het originele document. Anders corrigeert u dit of laadt u het originele PDF-bestand.
text.extract.title=PDF-extractie geslaagd
text.extract.error.msg=De inhoud kan niet automatisch worden geëxtraheerd. Upload het PDF-bestand handmatig met de knop "PDF toevoegen".
text.extract.error.title=Er is een fout opgetreden bij het extraheren van de PDF-inhoud

text.label.author=Auteur(s)
text.label.author.name=Naam
text.label.author.org.name=Naam
text.label.author.pers.name=$Prénom et nom
text.label.author.org.place.name=$Nom de l''organisation
text.label.author.pers.place.name=$Prénom et nom de la personne
text.label.author.place.name=Persoon of organisatie
text.label.author.function=Functie of machtiging
text.label.author.place.function=Functie (optioneel)
text.label.author.affname=affiliatie
text.label.author.place.affname=affiliatie (optioneel)
text.error.author=Er moet minimaal één auteur zijn opgegeven

text.label.tag=Tags
text.label.places=Plaatsen
text.label.tag.place.name=Permacultuur

text.place.language=Kies een van de beschikbare talen
text.label.language=Teksttaal *
text.help.language=Taal van geïmporteerde tekst
text.error.language=De taal is verplicht

text.label.sourceTitle=website of uitgever.
text.place.sourceTitle=theguardian.com of Penguin Books.
text.error.sourceTitle=Aangezien de tekst niet op internet staat, vul dan de bron in

text.place.topic=bijv. politiek, sociologie, literatuur, sociale klasse, hiphop, kunstmatige intelligentie, etc.
text.label.topic=Onderwerpen (en) *
text.help.topic=Voeg de onderwerpen van deze tekst toe door ze te scheiden met komma's of met de enter-toets.
text.error.topic=Ten minste één onderwerp is verplicht
text.error.topic.dupl=Je hebt minimaal twee keer hetzelfde onderwerp ingevoerd, geen duplicaat is toegestaan
text.error.tag.name=Er is minstens een tag vereist

text.place.publicationDate=bijv. 02/12/2005 of 12/2005 of 2005
text.label.publicationDate=Publicatiedatum
text.help.publicationDate=Datums kunnen dag / maand / jaar worden uitgedrukt (dag en maand zijn optioneel, met een "-" voor jaar voor BC).
text.error.publicationDate=De publicatiedatum is verplicht of u moet "Onbekende datum" aanvinken
text.error.publicationDate.format=De publicatiedatum heeft een ongeldige indeling
text.label.unknownpubdate=Onbekende datum

text.place.textType=Kies een van de beschikbare tekstsoorten
text.label.textType=Teksttype
text.error.textType=Het teksttype is verplicht

text.label.isOnInternet=De tekst is beschikbaar op het web *
text.error.isOnInternet=Gelieve aan te geven of de tekst op internet beschikbaar is of niet

text.external.fromTwitter.url=twitter.com
text.external.fromTwitter=Tweet de {0} du {1}

text.place.url=http: //some.website.com/text-article
text.label.url=Webadres *
text.help.url=Het volledige adres (URL) waar de tekst is gepubliceerd
text.error.url=De url is verplicht als u het vak "Beschikbaar op internet" selecteert
text.error.url.exiting=$Un texte avec cette url est déjà utilisée pour un autre texte dans notre base de données. 
text.error.url.exiting2=$Un texte avec cette url est déjà utilisée pour un autre texte dans notre base de données. Pour y accéder <a href="">cliquez ici</a>.

text.place.textOrigin=Selecteer de oorsprong uit de voorgestelde waarden
text.label.textOrigin=Type van de bron *
text.help.textOrigin=Uit wat voor soort bron komt de geïmporteerde tekst
text.error.textOrigin=U moet een tekstbron uit de lijst selecteren of een specifieke bron invullen in het geval dat "Andere" is geselecteerd

text.place.wordgender=Kies het woord gender
text.label.wordgender=Woord geslacht

text.candidate.title=We hebben teksten gevonden met dezelfde titel of bron
text.candidate.desc=U hebt de naam ingevoerd van een tekst die we al lijken te kennen. Bekijk hieronder de teksten als u \
   ze niet vindt en in plaats daarvan de invoer wilt bewerken. Als geen enkel voorstel overeenkomt, bevestig dan gewoon dat u \
   deze nieuwe tekst wil invoegen.

text.pdf.nourl=Er is geen url doorgegeven om de inhoud uit te extraheren, of het verwijst niet naar een pdf.
text.pdf.error=Er is een fout opgetreden bij het extraheren van de inhoud van de gegeven URL.
text.pdf.success=De inhoud van een gegeven PDF is succesvol geëxtraheerd, bekijk deze alsjeblieft.

text.sumup.sourceTitle=source
text.sumup.author=auteur (s)
text.sumup.topic=onderwerp (en)
text.sumup.url=van webpagina

# work with text (arguments)
text.args.title=Bewerk een mening
text.args.label.search=Zoeken naar een tekst
text.args.label.searchtext=Zoeken in de tekst
text.args.place.search=begin met het typen van een titel of een actornaam om naar teksten te zoeken
text.args.place.searchtext=begin met het typen van een woord of zin om door de tekst te bladeren
text.args.description=U kunt tekst vinden door naar de titel te zoeken en een van de alternatieven te selecteren. \
Klik vervolgens op <span class="col-xs fa fa-edit"> </ span> naast de zoekbalk om de volledige inhoud te laden \
alsook de bijbehorende meningen.
text.args.label.text=Tekstinhoud
text.args.label.random=Zou u liever <a id="load-random" href="{0}"> een willekeurige tekst openen </a>?
text.args.label.new=gebruik de zoekbalk als de mening dat u wilt toevoegen zich in een tekst bevindt die al in Webdeb staat. Als dit niet het geval is, voegt u <a href="{0}"> eerst de tekst toe om de mening uit te pakken </a>.
text.args.label.create=Selecteer een willekeurig deel van de tekst om een nieuw argument toe te voegen.
text.args.btn=Tekst weergeven
text.args.btn.tooltip=Toon deze tekst en alle bijbehorende meningen
text.args.label.nlphelp=Markering
text.args.label.tab.info=Teksteigenschappen
text.args.desc.tab.info=Dit zijn de teksteigenschappen. Klik op <i> Teksteigenschappen bewerken </ i> om ze te wijzigen.
text.args.label.tab.actors=Tekstactoren
text.args.label.tab.text=Alle teksteigenschappen bekijken
text.args.desc.tab.actors=Hier zijn de actoren geïdentificeerd in de tekst.
text.args.label.author=Tekst auteur (s)
text.args.label.actor=Actor(en) betrokken bij deze tekst als onderdeel van meningenactoren
text.args.add.new=$Ajouter une nouvelle citation en surlignant l’extrait dans le texte si dessous ou en encodant la citation dans le formulaire en dessous du texte (et en cliquant ensuite sur le crayon)
text.exc.label.tab.excerpts=Quotes uit de tekst halen
text.exc.label.tab.excerpt.add=Een quote toevoegen
text.exc.desc.tab.excerpts=Ziehier een lijst van de bewerkte quotes van de tekst. Om een quote te wijzigen klik op <i class="fa fa-edit"></i>

text.args.notfound=De tekst die u heeft opgevraagd kan niet worden gevonden in WebDeb.
text.args.nocontent=Deze tekst heeft geen inhoud bijgevoegd.
text.args.unautautorized=Mogelijk ziet u de inhoud van deze tekst niet.
text.args.external.content=De inhoud van deze tekst kan alleen worden bekeken als een <a href="{0}"> afzonderlijk bestand </a>.
text.onlyonsource=$Le contenu de ce texte ne peut être vu que depuis son site web d''origine : <a href="{0}" target="_blank">{1}</a>

text.args.preview=Dit is een voorbeeld. De volledige tekst wordt geladen, even geduld aub ... <span class="fa fa-spinner fa-spin"> </ span>
text.args.label.load=hier

text.args.label.title=titel
text.args.label.language=taal
text.args.label.sourceTitle=van bron
text.args.label.place=betrokken gebied (en)
text.args.label.original.language=originele taal
text.args.label.original.title=originele titel
text.args.label.topic=onderwerp (en)
text.args.label.publicationDate=publicatiedatum
text.args.label.date=datum
text.args.label.textType=type tekst
text.args.label.url=internetlink
text.args.label.textOrigin=type van de bron

text.args.popover.new=Voeg toe
text.args.popover.edit=Bewerken
text.args.popover.viz=Visualiseer
text.args.argument.excerpt=Kopieer een fragment dat een mening bevat en plak het hier.

text.args.add.argument=De tekst analyseren
text.args.add.actor=Voeg een actor toe die in deze tekst wordt geciteerd
text.args.add.actor.submit=toevoegen

text.args.tooltip.arg.add=Voeg een mening toe
text.args.tooltip.actor.add=Voeg een actor toe die bij deze tekst is betrokken, zonder er een auteur van te zijn
text.args.tooltip.arg.remove=Verwijder deze mening
text.args.tooltip.arg.settings=Bewerk de eigenschappen van deze mening
text.args.tooltip.arg.links=Bewerk de relaties met andere meningen
text.args.tooltip.text.random=Werken met een willekeurige tekst
text.args.tooltip.arg.summary=Toon een samenvatting van de eigenschappen en linken van de meningen
text.args.tooltip.arg.viz=Visualiseer mening

text.args.no.actorname=Er is geen actornaam ingevuld.
text.args.actorname.added=De actor {0} is toegevoegd aan de lijst met geciteerde actoren in deze tekst
text.args.actorname.removed=De actor {0} is verwijderd uit de lijst met geciteerde actoren in deze tekst
text.args.actorname.remove=Verwijderen uit lijst

text.tags.label=Tags*

# work with text (links)
text.args.label.tab.links=De argumentatieve structuur creëren
text.args.desc.tab.links=U kunt rechtvaardigingslinks bewerken door meningen van links naar rechts te verslepen en \
   het type rechtvaardigingslink dat u bevalt (ondersteunt, kwalificeert of betwist) te selecteren.
text.args.links.arguments=meningen
text.args.links.links=Rechtvaardigingsgronden
text.args.tooltip.links.save=Druk op deze knop om uw wijzigingen op te slaan
text.args.links.save=linken opslaan in ...
text.args.tooltip.links.cancel=Druk op deze knop om uw huidige wijzigingen te annuleren
text.args.links.cancel=Annuleren
text.links.success=U hebt de justificatie-links in deze tekst met succes bijgewerkt
text.links.tooltip.remove=Verwijder rechtvaardigingslink naar deze meningen
text.links.tooltip.expand=Toon / verberg gekoppelde meningen
text.links.shade.select=Selecteer er een
text.links.alllinks=Alle meningen zijn aan elkaar gekoppeld
text.links.noarg=Geen mening ...
text.links.nolink=Versleep hier een mening.
text.links.missing.type=U moet het type rechtvaardiging voor alle links selecteren
text.links.nothing=Er zijn geen links om op te slaan
text.links.save.success=De rechtvaardigingslinks zijn succesvol opgeslagen
text.links.save.error=De rechtvaardigingslinks konden niet bewaard worden ...

text.title.lang.label=Taal
text.title.name.label=Titel
text.title.place.name=Vertaalde titel
text.tile.type=Is de tekst een interview of een debat? *
text.titles.title=Titelvertaling (en)
text.titles.show=Titeltekst naar andere talen;

#debate
entry.debate.new=$Ajouter un nouveau débat
entry.debate.modify=Wijwig een bestaand debat
entry.debate.submit=Het debat opslaan
entry.debate.btn.modify=Wijzig het debat 

argument.error.title.blank=De titel ontbreekt
argument.error.title.size=De titel mag de 512 tekens niet overschrijden

#tag
entry.tag.new.from.parent=$Créer un dossier plus large pour {0}
entry.tag.new.from.child=$Créer un sous-dossier pour {0}
entry.tag.new=Creëer een nieuwe tag
entry.tag.modify=Wijwig een bestaande tag
entry.tag.new.category=$Nouvelle catégorie
entry.tag.new.thesis=$Nouvelle thèse
entry.tag.modify.thesis=Modifier la thèse existante
entry.tag.modify.category=$Modifier la catégorie existante
entry.tag.new.btn=Nieuze tag
entry.tag.new.description=Voeg een nieuwe tag toe in WebDeb
entry.tag.submit=Creëer een nieuwe tag
entry.tag.btn.modify=Wijzig de tag
entry.tag.btn.merge=Tags samenvoegen
entry.tag.success=Uw tag werd succesvol opgeslagen in WebDeb
entry.tag.edit.btn.tooltip=Bewerk de eigenschappen van de tag
entry.tag.existing.title=Wijzig een bestaande tag?
entry.tag.existing.text=U heeft een bestaande tag geselecteerd in Webdeb, wil u deze updaten of een nieuw dossier invoeren (homoniem)?
entry.tag.existing.merge.text=U heeft een bestaande tag geselecteerd in WebDeb, wil u deze samenvoegen of een nieuw dossier invoeren (homoniem)?
entry.tag.namesake.title=Tag al bekend?
entry.tag.namesake.desc=U heeft een tag ingevoerd die ons bekend is. Gelieve dit na te kijken in de onderstaande lijst en te selecteren wat van toepassing is. Indien geen enkele van de voorstellen juist lijkt,\ gelieve dan te bevestigen dat u een nieuwe tag wenst te creëren (homoniem) \
entry.tag.existing.use=Gebruik de bestaande tag
entry.tag.existing.ok=Wijzig de tag
entry.tag.existing.nok=Creëer een nieuwe tag
entry.tag.auto=U heeft net de naam van een tag ingevoerd die onbekend is voor ons, wenst u zijn details te vervolledigen?
entry.tag.canceled=U heeft geen wijzigingen doorgevoerd voor tag {0}
entry.tag.save=U heeft de tag correct ingediend of gewijzigd:

tag.place.name=Kernenergie en veiligheid
tag.place.rewordingname=Nucleaire beveiliging
tag.label.name.label=$Intitulé
tag.label.rewordingname=Herformulering
tag.help.name=De naam van de tag
tag.error.name=De naam van de tag is vereist
tag.error.name.name=Gelieve een titel aan te duiden
tag.error.name.name.chars=De titel van de tag mag geen bevatten / 
tag.error.name.name.size=De tag mag niet langer zijn dan 5 woorden
tag.error.name.begin=$Le nom du dossier doit commencer par le / la / les ou l''
tag.error.name.lang=Gelieve een taal te kiezen
tag.error.rewordingname.twice=De naamherformuleringen moeten verschillend zijn voor eenzelfde taal

tag.parent=$Dossiers plus large
tag.parent.place.name=Kernenergie
tag.help.parent=
tag.error.parent=

tag.child=$Sous-dossiers
tag.child.place.name=Nucleaire beveiliging op vlak van geopolitiek

tag.hierarchy.notfound=De vermelde tag bestaat niet.
tag.hierarchy.noparent=Gelieve minstens een ouder tag te vermelden.
tag.hierarchy.admin.noparent=Gelieve minstens een ouder tag te vermelden.

tag.place.tagType=Please pick one of the available tag types
tag.label.tagType=Type of tag *
tag.error.tagType=The tag type is mandatory
tag.label.tagType.0=Root
tag.label.tagType.1=Normal
tag.label.tagType.2=Question

tag.names.title=Titel van de tag (en zijn vertalingen)*
tag.names.title2=Titel van de tag (en zijn vertalingen)
tag.rewordingnames.title=Andere formuleringen van de tag
tag.names.show=Naam van de tag in andere talen
tag.rewordingnames.show=Herformuleringen van de tag

tag.notfound=De gevraagde tag kon niet gevonden worden in de kennisdatabase.
tag.link.alreadyexist=De link tussen de twee tags bestaat al.
tag.link.added=De link tussen de tags werd succesvol opgeslagen.

tag.candidate.title=We hebben tags gevonden met een gelijkaardige naam
tag.candidate.desc=U heeft net een tag ingevoerd die we al menen te hebben in WebDeb. Gelieve na te kijken \ of deze zich niet tussen de volgende tags bevindt om deze aan te passen. Indien geen enkel voorstel juist lijkt,\ bevestig dan dat u een nieuwe tag wil toevoegen. 
tag.delete.confirm.text=Bevestig het verwijderen van de tag "{0}".
tag.delete.confirm.title=Bent u zeker dat u deze tag wil verwijderen?

# browse
browse.view.title.-1=Zoeken naar bijdragen
browse.view.title.0=Actoren bekijken
browse.view.title.1=Bekijk debatten
browse.view.title.2=Bekijk teksten
browse.view.title.3=Bekijk quotes
browse.view.title.6=Bekijk maps
browse.search.title=Zoeken naar bijdragen
browse.search.ingroup=in groep {0}
browse.search.tooltip=Start het zoeken van de kennisbank
browse.search.place=Zoeken naar actoren, meningen, teksten of naar thema's
browse.search.place.alt=Zoeken in WebDeb
browse.search.error=Er is een fout opgetreden bij het uitvoeren van uw zoekopdracht.
browse.search.noresult=Geen bijdrage gevonden.
browse.search.noresult.0=Geen actor gevonden.
browse.search.noresult.1=Geen debat gevonden.
browse.search.noresult.3=Geen mening gevonden.
browse.search.noresult.4=Geen Quote gevonden.
browse.search.noresult.5=Geen tekst gevonden. 
browse.search.noresult.6=Geen tag gevonden .
browse.search.noresult.7=Geen rechtvaardiging gevonden.
browse.search.tooltip.fromtopic=Toon alle bijdragen die getagd zijn met dit onderwerp
browse.search.tooltip.fromplace=Show the map of this place
browse.search.firstarg.title=Creëer een overkoepelende mening

browse.search.arg.title=Maak de mening die als argument dient
browse.search.arg.title2=Creëer een overkoepelende mening
browse.search.arg.title3=Vat een mening in
browse.search.arg.instructions=Voer uw zoekopdracht uit met <span class="fa fa-search"> </ span>. U kunt ook \
   <a href="{0}"> een nieuw mening toevoegen </a>.
browse.search.selectarg.addon=Zoeken naar meningen
browse.search.selectarg.place=U kunt zoeken op teksttitels, actoren, quotes of meningen
browse.search.selectarg.btn=Maak een link
browse.search.selectarg.tooltip=Maak een link naar het geselecteerde mening

browse.search.exc.title=Zoek de quote te linken aan
browse.search.exc.instructions=Voer een zoekopdracht uit met <span class="fa fa-search"></span>.
browse.search.exc.instructions2=U kan ook
browse.search.exc.instructions3=een nieuw fragment toevoegen
browse.search.selectexc.addon=Een quote zoeken
browse.search.selectexc.place=U kan zoeken op teksttitel, actor, quote
browse.search.selectexc.btn=Creëer de link
browse.search.selectexc.tooltip=Creëer de link met het geselecteerd quote

browse.search.actor.instructions=Selecteer een van de onderstaande voorstellen en klik op de knop "Creëer de link". Indien geen enkel voorstel past, klik dan op de link "Creëer een nieuwe quote".

browse.search.topic=Populaire onderwerpen
browse.search.argument=Populaire meningen
browse.search.argument_contextualized=Populaire meningen
browse.search.actor=Populaire actoren
browse.search.text=Populaire teksten
browse.search.debate=Populaire debatten
browse.search.tag=Populaire dossiers
browse.search.citation=Populaire quotes
browse.showall.actor=Toon alle actoren
browse.showall.argument=Toon alle meningen
browse.showall.text=Toon alle teksten
browse.showall.tag=Toon alle dossiers
browse.showall.citation=Toon alle quotes

browse.search.tip.ACTOR=fa fa-street-view
browse.search.tip.DEBATE=far fa fa-comments-o
browse.search.tip.ARGUMENT=far fa-comment
browse.search.tip.CITATION=fas fa-align-left
browse.search.tip.TEXT=far fa-file-alt
browse.search.tip.tag=far fa-tag

browse.search.expand= Breid uw zoekopdracht uit naar openbare groepen
browse.search.reduce=Verklein uw zoekopdracht uit naar groep

#
# visualization
#

empty=

# general
viz.no.key=Onbekend
viz.admin.actions=bewerken
viz.admin.details.label=zie de details 
viz.admin.actions.text=tekst bewerken
viz.admin.actions.desc=$Editions options :
viz.admin.update.btn=Bijdrage bijwerken
viz.admin.update.tooltip=Werk deze bijdrage bij
viz.admin.import.btn=Importeren in groep
viz.admin.import.tooltip=Importeer deze bijdrage in een van uw groepen
viz.admin.merge.success=De samenvoegactie is met succes uitgevoerd.
viz.admin.merge.fail=De samenvoegactie kan niet worden uitgevoerd.
viz.admin.merge.same=U probeerde deze bijdrage samen te voegen met zichzelf.
viz.admin.delete.success=De bijdrage is verwijderd.
viz.admin.remove.group.success=De bijdrage is verwijderd uit groep {0}.
viz.admin.delete.fail=De verwijderactie kon niet worden uitgevoerd (mogelijk is er een link naar deze bijdrage die niet kan worden verwijderd).
viz.admin.merge.search.label.0=Zoek acteurs
viz.admin.merge.search.label.1=Zoek mening
viz.admin.merge.search.label.2=Zoek teksten
viz.admin.merge.search.label.6=Zoek een dossier
viz.admin.merge.search.place.0=Zoeken op naam
viz.admin.merge.search.place.1=Zoeken op standaardformulier, auteurs of teksttitel
viz.admin.merge.search.place.2=Zoeken op titel en auteurs
viz.admin.merge.search.place.6=Zoeken op naam
viz.admin.merge.search.instructions=Begin met typen om suggesties weer te geven en start met zoeken met <span class="fa \
   fa-search "> </ span>.
viz.admin.merge.search.title=Zoeken naar bijdrage om samen te voegen
viz.admin.merge.title=Voeg deze bijdrage samen met een andere
viz.admin.merge.label=Bijdrage samenvoegen
viz.admin.merge.submit.title=Deze bijdrage samenvoegen en vervangen door degene die je zojuist hebt geselecteerd
viz.admin.merge.submit.btn=Samenvoegen

viz.admin.delete.title=Verwijder deze bijdrage (gebonden bijdragen worden ook verwijderd)
viz.admin.delete.label=Bijdrage verwijderen
viz.admin.modal.delete.0.title=Deze actor verwijderen?
viz.admin.modal.delete.0.text=Weet je zeker dat je deze actor wilt verwijderen? Als deze acteur aanwezig is in quote of teksten, \
verwijdering wordt niet toegestaan.
viz.admin.modal.delete.1.title=Verwijder dit debat?
viz.admin.modal.delete.1.text=Weet je zeker dat je dit debat wil verwijderen?/ Al zijn meningen zullen verwijderd worden.
viz.admin.modal.delete.3.title=Deze mening verwijderen?
viz.admin.modal.delete.3.text=Weet u zeker dat u deze mening wilt verwijderen? Alle links naar dit mening worden ook verwijderd.
viz.admin.modal.delete.4.title=Verwijderen de quote ?
viz.admin.modal.delete.4.text=Weet u zeker dat u de quote wilt verwijderen? Alle links zijn dan verwijdert.
viz.admin.modal.delete.5.title=Deze tekst verwijderen?
viz.admin.modal.delete.5.text=Weet u zeker dat u deze tekst wilt verwijderen? Alle meningen uit deze tekst \
   wordt ook verwijderd.
viz.admin.modal.delete.6.title=Verwijder deze tag?
viz.admin.modal.delete.6.text=Weet u zeker dat u deze tag wilt verwijderen? Het zal invloed hebben op de hiërarchie van de tags.
viz.admin.modal.delete.8.title=$Supprimer ce lien de justification ?
viz.admin.modal.delete.8.text=$Êtes-vous sûr de vouloir supprimer ce lien de justification ? Cela aura des répercusions sur la structure argumentative (débat / texte).
viz.admin.modal.delete.9.title=$$Supprimer ce lien de similarité ?
viz.admin.modal.delete.9.text=$Êtes-vous sûr de vouloir supprimer ce lien de similarité ?
viz.admin.modal.delete.10.title=$Supprimer ce lien entre l''opinion et la citation ?
viz.admin.modal.delete.10.text=$Êtes-vous sûr de vouloir supprimer ce lien entre l''opinion et la citation ?
viz.admin.modal.delete.11.title=$Supprimer ce lien de similarité ?
viz.admin.modal.delete.11.text=$Êtes-vous sûr de vouloir supprimer ce lien de similarité ?
viz.admin.modal.delete.12.title=$Supprimer ce lien  ?
viz.admin.modal.delete.12.text=$Êtes-vous sûr de vouloir supprimer ce lien ?
viz.admin.modal.delete.13.title=$Supprimer ce lien entre dossiers ?
viz.admin.modal.delete.13.text=$Êtes-vous sûr de vouloir supprimer ce lien ?
viz.admin.modal.delete.16.title=$Supprimer cette catégorie de débat ?
viz.admin.modal.delete.16.text=$Êtes-vous sûr de vouloir supprimer ce lien ?
viz.admin.modal.delete.17.title=$Supprimer cette position ?
viz.admin.modal.delete.17.text=$Êtes-vous sûr de vouloir supprimer cette position ?
viz.admin.modal.delete.19.title=$Supprimer ce texte du débat ?
viz.admin.modal.delete.19.text=$Êtes-vous sûr de vouloir supprimer ce lien ?

viz.admin.remove.title=Verwijder deze bijdrage uit de huidige groep {0}
viz.admin.remove.label=Verwijderen van {0}
viz.admin.modal.remove.0.title=Deze actor verwijderen uit de huidige groep?
viz.admin.modal.remove.0.text=Weet je zeker dat je deze actor uit groep <b> {0} </b> wilt verwijderen?
viz.admin.modal.remove.1.title=Verwijderen de debat uit de huidige groep
viz.admin.modal.remove.1.text=Weet u zeker dat u de debat uit groep <b>{0}</b> wilt verwijderen?
viz.admin.modal.remove.3.title=Deze mening uit de huidige groep verwijderen?
viz.admin.modal.remove.3.text=Weet u zeker dat u deze mening uit groep <b> {0} </b> wilt verwijderen?
viz.admin.modal.remove.4.title=Verwijderen de quote uit de huidige groep viz.admin.modal.remove.4.text=Weet u zeker dat u deze quote uit groep <b> {0} </b> wilt verwijderen?
viz.admin.modal.remove.5.title=Deze tekst uit de huidige groep verwijderen?
viz.admin.modal.remove.5.text=Weet u zeker dat u deze tekst uit groep <b> {0} </b> wilt verwijderen?

viz.admin.history.title=Geschiedenis van deze bijdrage
viz.admin.history.label=Bekijk de geschiedenis
viz.admin.history.version=datum
viz.admin.history.author=auteur
viz.admin.history.modification=Status
viz.admin.history.trace=opsporen

viz.admin.history.add=$adding
viz.admin.history.remove=$removal
viz.admin.history.modify=$modification
viz.admin.history.element.name=$Name
viz.admin.history.element.tags=$Tag
viz.admin.history.element.social_objects=$Objet(s) social
viz.admin.history.element.places=$Place
viz.admin.history.element.actortype=$Type d''acteur
viz.admin.history.element.named=$Name
viz.admin.history.element.translations=$Traduction
viz.admin.history.element.reworded=$Autre intitulé
viz.admin.history.element.gender=$Genre
viz.admin.history.element.date_of_birth=$Date de naissance
viz.admin.history.element.date_of_death=$Date de décès
viz.admin.history.element.date_created=$$Date de création
viz.admin.history.element.date_terminated=$Date de fin
viz.admin.history.element.date_publication=$Date de publication
viz.admin.history.element.country=$Pays
viz.admin.history.element.lang=$Langue
viz.admin.history.element.affiliations=$Affiliation
viz.admin.history.element.url=$Page web
viz.admin.history.element.official_number=$Numéro officiel
viz.admin.history.element.authors=$Acteur concerné
viz.admin.history.element.text=$Texte
viz.admin.history.element.text_source=$Source
viz.admin.history.element.original_excerpt=$Extrait original
viz.admin.history.element.working_excerpt=$Citation retravaillée
viz.admin.history.element.date_now=$en cours
viz.admin.history.element.tag_parent=$Dossier parent
viz.admin.history.element.tag_children=$Dossier enfant
viz.admin.history.element.language=$Langue
viz.admin.history.element.picture=$Photo

viz.admin.history.element.actor_type=$Type d''acteur
viz.admin.history.element.affiliation_type=$Type d''affiliation
viz.admin.history.element.arg_shade_type=$Début du débat / opinion
viz.admin.history.element.debate_shade_type=$Début du débat
viz.admin.history.element.argument_linktype=$Type de lien entre opinion
viz.admin.history.element.argument_type=$Type d''opinion
viz.admin.history.element.business_sector=$Secteur d''activité
viz.admin.history.element.contribution_type=$Type de contribution
viz.admin.history.element.tag_type=$Type de dossier
viz.admin.history.element.link_shade_type=$Type de lien entre opinion
viz.admin.history.element.place_type=$Type de lieu
viz.admin.history.element.legal_status=$Status légal
viz.admin.history.element.text_type=$Type de texte
viz.admin.history.element.external_urls=Url
viz.admin.history.element.description=$Description

# contributor
viz.contributor.avatar.btn=Afbeelding toevoegen
viz.contributor.avatar.tooltip=U kunt een afbeelding toevoegen die u identificeert
viz.contributor.label.pic.noimage=Geen foto ...
viz.contributor.label.affiliations=Verbonden aan organisaties
viz.contributor.update.tooltip=Werk deze details bij
viz.contributor.update.btn=Profiel bijwerken
viz.contributor.label.last.contrib=Laatste bijdragen
viz.contributor.change.pwd=Wachtwoord wijzigen
viz.contributor.change.mail=Wijzig email

viz.contribution.actor=Alle aangemaakte of gewijzigde acteurs
viz.contribution.text=Alle geïmporteerde teksten
viz.contribution.debate=Alle aangemaakte of gewijzigde debat
viz.contribution.arguments=Meningen in deze tekst
viz.contribution.citations=Quotes uit deze tekst
viz.contribution.debate.arguments=Meningen in het debat 
viz.contributor.creator=Aangemaakt door

#viewer
viz.viewer.actions=update card

# new labels for text and argument radio
viz.radio.pov=Standpunt
viz.radio.pov.author=Auteur
viz.radio.pov.source=Bron
viz.radio.pov.date=Datum
viz.radio.pov.text=Tekst
viz.radio.stat=Statistieken
viz.radio.stat.author.similar=sluit auteurs
viz.radio.stat.author.qualifies=auteurs genuanceerd
viz.radio.stat.author.opposes=tegenstanders
viz.radio.stat.text.similar=teksten sluiten
viz.radio.stat.text.qualifies=tekst genuanceerd
viz.radio.stat.text.opposes=tegengestelde teksten
viz.radio.stat.excerpt.similar=fragmenten voor
viz.radio.stat.excerpt.qualifies=fragmenten genuanceerd
viz.radio.stat.excerpt.opposes=tegenovergestelde fragmenten

viz.radio.excerpt=Fragmenten
viz.radio.texts=Teksten
viz.radio.error=Er is een fout opgetreden bij het ophalen van de radiografie ...

viz.radio.sortby=Sorteren op
viz.radio.sortby.pos=Position
viz.radio.sortby.author=Auteur
viz.radio.sortby.date=Datum
viz.radio.sortby.amount=Hoeveelheid
viz.radio.sortby.text=Tekst

viz.showmore.excerpt=Andere quote
viz.showmore.argument=Andere meningen
viz.showmore.texts=Andere teksten
viz.showmore.affiliations=Andere affiliaties
viz.showmore.authors=Andere auteurs
viz.showmore.tags=Andere dossiers
viz.showmore.places=Andere plaatsen

# argument
viz.argument.title=Mening
viz.argument.btn=Visualiseer Mening
viz.argument.btn.details=Gedetailleerde weergave
viz.argument.tooltip.arg=Visualiseer de cartografie van deze mening (alle bijbehorende gekoppelde meningenen)
viz.argument.tooltip.details.arg=Ga terug naar de vorige gedetailleerde cartografie
viz.argument.tooltip.graph.arg=Visualiseer de linkgrafiek van deze mening
viz.argument.pill.radiography=Voor/tegen
viz.argument.desc.radiography=De actoren voor of tegen de mening.
viz.argument.pill.cartography=Argumenten
viz.argument.desc.cartography=Argumenten voor of tegen de mening
viz.argument.pill.sociography=Allianties
viz.argument.desc.sociography=Standpunt van de verschillende categorieën actoren over de mening.
viz.argument.desc.sociography2=De organisaties zijn niet opgenomen in de statistieken per leeftijd, functie en geslacht.
viz.argument.pill.details=Card
viz.argument.pill.texts=Teksten / Debatten / tags
viz.argument.desc.texts=Teksten, debatten en tags waarin de mening ligt

viz.argument.texts.pov.0=Teksten
viz.argument.texts.pov.1=Debatten
viz.argument.texts.pov.2=Tags
viz.argument.texts.nodata.texts=De mening komt uit geen tekst.
viz.argument.texts.nodata.debates=De mening komt uit geen debat.
viz.argument.texts.nodata.tags=De mening komt uit geen tag

viz.argument.label.type=Type
viz.argument.label.timing=Timing
viz.argument.label.subtype=Subtype
viz.argument.label.shade=Nuance
viz.argument.label.classes=Classe (s)
viz.argument.label.topics=Onderwerp (en)
viz.argument.label.text=Tekst
viz.argument.box.title.rejects=... geschillen
viz.argument.box.title.is_rejected=... wordt betwist door
viz.argument.box.title.shades=... kwalificeert
viz.argument.box.title.is_shaded=... wordt gekwalificeerd door
viz.argument.box.title.supports=... ondersteunt
viz.argument.box.title.is_supported=... wordt ondersteund door
viz.argument.box.title.similar=... is vergelijkbaar met
viz.argument.box.title.qualifies=... nuanceert
viz.argument.box.title.opposes=... is tegen

# for context cartography pov
viz.argument.display.pov.title=Tonen
viz.argument.display.pov.0=De belangrijkste meningen
viz.argument.display.pov.1=De meningen die verantwoorden 
viz.argument.display.pov.2=De meningen die tegengaan
viz.argument.radio.pov.title=Argumentatie niveau
viz.argument.radio.pov.0=1
viz.argument.radio.pov.1=2
viz.argument.radio.pov.2=3
viz.argument.radio.pov.3=4
viz.argument.radio.pov.4=Geheel

# for argument radiography
viz.argument.new.title=Een nieuw gerelateerd mening toevoegen?
viz.argument.new.desc=Selecteer het type link
viz.argument.new.0=Nieuw vergelijkbaar quote
viz.argument.new.1=Nieuw kwalificeert quote
viz.argument.new.2=Nieuw betwist quote
viz.argument.justification.see=Zie

viz.argument.contributions=Meningen die direct aan deze bijdrage zijn gekoppeld
viz.argument.carto.ssr=die de <b>mening</b> betoogt
viz.argument.carto.is_ssr=die <b>betoogt</b> de mening
viz.argument.carto.sort=sort by total number of links

viz.argument.carto.supports=Argumenten die verantwoorden
viz.argument.carto.shades=Argumenten die nuanceren
viz.argument.carto.rejects=Argumenten die tegengaan

viz.argument.carto.0=Quote(s)

viz.argument.carto.illustrate=Quote(s) voor
viz.argument.carto.shaded_example=Genuanceerde quote(s)
viz.argument.carto.counter_example=Quote(s) tegen

viz.argument.carto.nbexcerpts.similar=Minstens {0} quotes voor
viz.argument.carto.nbexcerpts.qualifies=Minstens {0} genuanceerde quotes
viz.argument.carto.nbexcerpts.opposes=Minstens {0} quotes tegen

viz.argument.justification.map.is_support={0}mening(en) worden verantwoord door dit argument
viz.argument.justification.map.is_shade={0}mening(en) worden genuanceerd door dit argument
viz.argument.justification.map.is_reject={0}mening(en) worden tegengegaan door dit argument
viz.argument.justification.map.support={0}argument(en) verantwoorden dit mening
viz.argument.justification.map.shade={0} argument(en) nuanceren dit mening
viz.argument.justification.map.reject={0} argument(en) gaan deze mening tegen

viz.argument.carto.add.argument.supports=Voeg een argument toe dat de mening verantwoordt
viz.argument.carto.add.argument.shades=Voeg een argument toe dat de mening nuanceert
viz.argument.carto.add.argument.rejects=Voeg een argument toe dat de mening tegengaat

viz.argument.carto.argument.add.illustrate=Voeg een quote toe voor
viz.argument.carto.argument.add.shaded_example=Voeg een genuanceerde quote toe
viz.argument.carto.argument.add.counter_example=Voeg een quote toe tegen

viz.argument.carto.edit.btn=Wijzig bepaalde verantwoordende linken
viz.argument.carto.canceledit.btn=Wijzig de verantwoordende linken niet
viz.argument.radio.edit.btn=Wijzig de verantwoordende linken niet
viz.argument.radio.save.btn=Sla de wijzigingen op
viz.argument.radio.canceledit.btn=Wijzig de verantwoordende linken niet

viz.argument.select.existing=We raden aan de volgende meningen te linken

viz.argument.tags.label=Tags*

viz.argument.radio.4=akkoord
viz.argument.radio.5=genuanceerd
viz.argument.radio.6=niet akkoord

viz.argument.radio2.4=zeggen dat het waar is
viz.argument.radio2.5=genuanceerd
viz.argument.radio2.6=zeggen dat het niet waar is

viz.argument.radio3.4=zeggen dat we moeten
viz.argument.radio3.5=genuanceerd
viz.argument.radio3.6=zeggen dat we mogen niet

viz.excerpt.arguments.4=unstige meningen 
viz.excerpt.arguments.5=Genuancerde meningen
viz.excerpt.arguments.6=Ongunstige meningen

viz.argument.cited.actors=actor(en)

viz.argument.error=Het gegeven mening is niet bekend of er is een onverwachte fout opgetreden.

viz.argument.similarity.fail=Meningenlink kon niet opgeslagen worden...
viz.argument.similarity.success=Meningenlink werd succesvol opgeslagen!
# argument sociography
viz.argument.socio.pov.author=Auteur
viz.argument.socio.pov.age=Leeftijd
viz.argument.socio.pov.country=Land
viz.argument.socio.pov.function=Functie
viz.argument.socio.pov.organization=Organisatie
viz.argument.socio.pov.gender=Geslacht
viz.argument.socio.pov.publisher=Uitgever
viz.argument.socio.excerpt=Quote(s)
viz.argument.socio.nodata=Geen enkele acteur met de gekozen groepscriterium heeft een standpunt ingenomen ten opzichte van deze mening...

viz.argument.socio.unshaded=$citation(s)
viz.argument.socio.justify=$citation(s) pour
viz.argument.socio.qualifies=$citation(s) nuancée(s)
viz.argument.socio.opposes=$citation(s) contre

viz.argument.socio.info.title=$Citations regroupées par argument et catégorie d’arguments

# actor
viz.actor.title.-1=Actor (onbekend)
viz.actor.title.0=Individuele persoon
viz.actor.title.1=Organisatie
viz.actor.title.2=Project, merk, evenement
viz.actor.pill.cartography2=Individuen
viz.actor.pill.cartography=Voorkeuren
viz.actor.desc.cartography.1=Mensen die deel uitmaken van deze organisatie.
viz.actor.desc.cartography.1.bis= Tot welke organisaties behoort deze actor?? 
viz.actor.desc.cartography.0=De CV van {0}
viz.actor.pill.radiography=Lezingen
viz.actor.desc.radiography=Meningen en quotes van {0} (meningen vatten citaten samen en zijn geschreven door WebDeb-bijdragers)
viz.actor.pill.sociography=Bondgenoten / Tegenstanders
viz.actor.desc.sociography=Lijst van actoren die een standpunt hebben ingenomen ten opzichte van de woorden van deze actor.
viz.actor.pill.cited=Over
viz.actor.desc.cited=Meningen en quotes over {0} (meningen vatten citaten samen en zijn geschreven door WebDeb-bijdragers)
viz.actor.pill.texts=Teksten
viz.actor.desc.texts=Teksten waarvan de actor de auteur is en waarin hij vernoemd wordt.
viz.actor.pill.details=Kaart
viz.actor.contributions=Bijdragen waar deze acteur wordt weergegeven
viz.actor.contributions.error=Er konden geen bijdragen worden opgehaald voor deze acteur ...
viz.actor.avatar.btn=Foto toevoegen
viz.actor.avatar.tooltip=Voeg een nieuwe foto toe voor deze acteur
viz.actor.former=Vroeger

viz.actor.url.wiki=Wikipedia artikel
viz.actor.url.personal=Persoonlijke website 

viz.actor.carto.belongsto.m=Neemt deel aan…
viz.actor.carto.belongsto.f=Neemt deel aan… 
viz.actor.carto.belongsto.n=Neemt deel aan…
viz.actor.carto.belongings.m=Bestaat uit…
viz.actor.carto.belongings.f=Bestaat uit… 
viz.actor.carto.belongings.n=Bestaat uit… 
viz.actor.carto.btn.history.show=Toon geschiedenis
viz.actor.carto.btn.history.hide=Verberg geschiedenis
viz.actor.carto.btn.person.addaff=affiliatie toevoegen
viz.actor.carto.btn.org.addaff=affiliatie toevoegen
viz.actor.carto.btn.person.addmember=affiliatie toevoegen
viz.actor.carto.btn.org.addmember=Voeg lid (leden) toe
viz.actor.graph.affiliation-history=Verbonden met
viz.actor.graph.affiliated-history=Leden van

viz.actor.affiliation.add.title=Voeg affiliatie (s) toe aan
viz.actor.affiliation.submit.title=Voeg affiliatie (s) toe
viz.actor.affiliation.submit=Voeg toe
viz.actor.affiliation.success=Je hebt succesvol een nieuwe affiliatie toegevoegd aan actor {0}
viz.actor.affiliated.add.person.title=Voeg voorkeuren aan…
viz.actor.affiliated.add.org.title=Voeg deelnemers aan…
viz.actor.affiliated.details=Vul de details in betreffende het lidmaatschap van
viz.actor.affiliated.submit.title=Voeg een nieuwe geaffilieerde actor toe aan deze organisatie
viz.actor.affiliated.person.submit=Toevoegen
viz.actor.affiliated.org.submit=Voeg deelnemers aan 
viz.actor.affiliated.success=Je hebt succesvol een / sommigen aangesloten bij actor {0}

viz.actor.carto.pov=Toon
viz.actor.carto.pov.0=Geschiedenis
viz.actor.carto.pov.1=Lijst

viz.actor.socio.pov=Groepeer op :
viz.actor.socio.pov.author=Auteur
viz.actor.socio.pov.age=Leeftijd
viz.actor.socio.pov.country=Land
viz.actor.socio.pov.function=Functie
viz.actor.socio.pov.organization=Organisatie
viz.actor.socio.excerpt=fragment (en)
viz.actor.socio.nodata=Either this actor has not expressed any argument or no other actor with the selected group key made \
  a stand regarding his/her arguments.


viz.actor.socio.age.-1=onbekende leeftijd
viz.actor.socio.country.unknown=onbekend land
viz.actor.socio.function.unknown=onbekende functie
viz.actor.socio.org.unknown=onbekende organisatie
viz.actor.socio.gender.unknown=unknown gender
viz.actor.socio.publisher.unknown=unknown publisher
viz.actor.socio.age.0=onder 15
viz.actor.socio.age.1=15 - 24
viz.actor.socio.age.2=25 - 34
viz.actor.socio.age.3=35 - 44
viz.actor.socio.age.4=45 - 54
viz.actor.socio.age.5=55 - 64
viz.actor.socio.age.6=65 +
viz.actor.socio.graph=Actorsociografie

viz.actor.socio.similar=actor(en) voor
viz.actor.socio.qualifies=genuanceerde actoren(en)
viz.actor.socio.opposes=actor(en) tegen

viz.actor.radio.key=Sorteren op
viz.actor.radio.key.date=Datum
viz.actor.radio.key.date.tooltip=Sorteren op publicatiedatum
viz.actor.radio.key.excerpts=Aantal fragmenten
viz.actor.radio.key.excerpts.tooltip=Sorteer op aantal vergelijkbare fragmenten voor deze acteur
viz.actor.radio.key.p_similar=Mate van overeenstemming
viz.actor.radio.key.p_similar.tooltip=Sorteer op het deel van soortgelijke fragmenten uitgedrukt door andere actoren
viz.actor.radio.key.t_similar=Debatintensiteit
viz.actor.radio.key.t_similar.tooltip=Sorteer op het totale aantal fragmenten met gelijksoortigheidslinken (inclusief oppositie)
viz.actor.radio.showarg=Show arguments ({0})
viz.actor.radio.citation.nolink=This citation has no opinions linked yet. Click to resume it.
viz.actor.radio.showexc=Fragmenten weergeven ({0})
viz.actor.radio.charttitle=mate van instemming van andere actoren
viz.actor.radio.nodata.aff=Geen enkele mening is gekoppeld aan deze actor
viz.actor.radio.nodata.exc=Geen enkele mening is gekoppeld aan deze actor

viz.actor.radio.citation.actor.0=Quotes van de auteur
viz.actor.radio.citation.actor.1=Quotes van andere auteur(s)
viz.actor.radio.citation.text.0=Quote(s) uit de tekst
viz.actor.radio.citation.text.1=Quote(s) uit andere tekst(en)

viz.actor.cited.showarg=Toon vergelijkbare argumenten ({0})
viz.actor.cited.nodata.aff=Geen enkele mening in WebDeb is gekoppeld aan deze actor.
viz.actor.cited.nodata.exc=Geen enkele mening in WebDeb spreekt over deze actor.
viz.actor.texts.author=Auteur van <span class="text-primary"> {0} </ span> tekst (en) waarvan hij de auteur is.
viz.actor.texts.excerpts=totale hoeveelheid fragmenten
viz.actor.texts.own.author=ondertekende <span class="text-primary"> {0} </ span> argument(en)
viz.actor.texts.excerpt_author=<span class="text-primary"> {0} </ span> tekst (en) waar argumenten door de acteur zijn ondertekend.
viz.actor.texts.own.excerpt_author=ondertekende <span class="text-primary"> {0} </ span> argument (en)
viz.actor.texts.excerpt_cited=Geciteerd in argumenten uit <span class="text-primary"> {0} </ span> tekst (en)
viz.actor.texts.own.excerpt_cited=geciteerd in <span class="text-primary"> {0} </ span> argument (en)

viz.actor.texts.stats.author=auteur van <span class="text-primary"> {0} </ span> tekst (en)
viz.actor.texts.stats.argument_author=argumenten uit <span class="text-primary"> {0} </ span> tekst (en)
viz.actor.texts.stats.argument_cited=geciteerd in <span class="text-primary"> {0} </ span> tekst (en)

viz.actor.error=Onbekend actor

viz.actor.position.line.title=$Analysé sur {0} débat(s), avec {1} position(s) de {2} et {3} position(s) de {4}
viz.actor.position.debate.line.title=${0} position(s) de {1} et {2} position(s) de {3}
viz.actor.position.debate.title=$Débats où {0} et {1} ont pris positions 

viz.actor.citations.list.title=$Citations
viz.actor.citations.actors.title=$Auteurs
viz.actor.citations.debates.title=$Debates
viz.actor.citations.tags.title=$Dossier
viz.actor.citations.texts.title=$Text

viz.actor.citations.list.empty.title.0=$Cet acteur n''est auteur d''aucune citation.
viz.actor.citations.empty.debates.title.0=$Cet acteur n''est auteur d''aucune citation dans aucun débat.

viz.actor.citations.list.empty.title.3=$Cet acteur n''est cité dans aucune citation.
viz.actor.citations.empty.debates.title.3=$Cet acteur n''est cité dans aucune citation dans aucun débat.

# Actor type
viz.actor.type.-1=Onbekend:
viz.actor.type.0=Personen:
viz.actor.type.1=Organisaties:
viz.actor.type.2=Projecten, merken, evenementen:

#debate
debate.creation.error=Het is onmogelijk dit debate op te slaan...
debate.sumup.firstArgument=A voor overkoepelende mening

debate.candidate.desc=U bent zojuist een debat ingevuld, waarvan we denken dat we die al in WebDeb hebben. \
  Controleer of het niet al een van de volgende debatten is om het te bewerken. \
  Als geen enkel voorstel correct lijkt, bevestigt u dat u een nieuw debat wilt toevoegen.
debate.candidate.title=We hebben debatten met een vergelijkbare titel gevonden
debate.candidate.existing.ok=Bewerken het debat
debate.candidate.existing.nok=Voeg een nieuw debat toe

debate.categorty.unclassified=$Non classé

viz.debate.tags.label=Tags*
viz.debate.desc.radiography=Quotes gekoppeld aan de mening.
viz.debate.desc.cited=Actor(en) voor of tegen de mening.
viz.debate.desc.sociography=Hoe staan de allianties van actoren tegenover de mening

viz.debate.position.0.0=$Vrai
viz.debate.position.0.1=$Vrai dans la plupart des cas
viz.debate.position.0.2=$Indécis (peut-être ou ne sait pas)
viz.debate.position.0.3=$Faux dans la plupart des cas
viz.debate.position.0.4=$Faux

viz.debate.position.1.0=$Il faut
viz.debate.position.1.1=$Il faut, moyennant amendements mineurs
viz.debate.position.1.2=$Indécis (peut-être ou ne sait pas)
viz.debate.position.1.3=$Il ne faut pas sauf amendements majeurs
viz.debate.position.1.4=$Il ne faut pas

viz.debate.position.2.0=$Evaluation positive
viz.debate.position.2.1=$Evaluation plutôt positive
viz.debate.position.2.2=$Evaluation mitigée
viz.debate.position.2.3=$Evaluation plutôt négative
viz.debate.position.2.4=$Evaluation négative

viz.debate.position.3.0=$Tout à fait d''accord
viz.debate.position.3.1=$Plutôt d''accord
viz.debate.position.3.2=$Indécis (peut-être ou ne sait pas)
viz.debate.position.3.3=$Pas d''accord
viz.debate.position.3.4=$Pas dutout d''accord

viz.debate.position.instructions=$Déplacez le curseur pour changer le degré de positionnement

viz.debate.arguments.argument=$argument
viz.debate.arguments.author=$auteur

viz.debate.similar.title=$débat(s) similaire(s) :

viz.context.arguments.0.0=$Vrai
viz.context.arguments.0.1=$Faux

viz.context.arguments.1.0=$Pour
viz.context.arguments.1.1=$Contre

viz.context.arguments.2.0=$Points forts
viz.context.arguments.2.1=$Points faibles

# text
viz.text.title=Tekst
viz.text.default.title=$Texte au titre inconnu
viz.text.pill.cartography=Structuur
viz.text.desc.cartography=Argumentatieve structuur van deze tekst.
viz.text.pill.radiography=Gekoppelde teksten
viz.text.desc.radiography=Teksten met gelijkaardige of tegenovergestelde meningen met die uit deze tekst.
viz.text.pill.cited=Meningen / Quotes 
viz.text.pill.details=Kaart
viz.text.contributions=Meningen (en) ontleend aan deze tekst
viz.text.args.btn=Analyseer de tekst
viz.text.args.tooltip=Bewerk of verwijder meningen uit deze tekst

viz.text.nolinks=Ofwel werd geen mening is ontleend aan deze tekst, ofwel zijn er er geen aan elkaar gekoppeld.
viz.text.links.btn=Wijzigingen bewerken
viz.text.links2.btn=Zie de tekst en zijn quotes
viz.text.links.tooltip=Rechtvaardiginglinken tussen meningen uit deze tekst bewerken

viz.text.cited.nodata.aff=Geen enkele mening is gekoppeld aan deze tekst
viz.text.cited.nodata.exc=Geen enkele quote is gekoppeld aan deze tekst

viz.text.error=Deze tekst is onbekend
viz.link.add.0=Voeg een rechtavaarding toe
viz.link.add.1=Ajouter rejection
viz.link.add.2=Voeg een mening toe

# debate
viz.debate.pill.cartography=Argumenten
viz.debate.pill.radiography=Voor/tegen
viz.debate.pill.sociography=Coalities
viz.debate.pill.cited=Voor/tegen
viz.debate.pill.details=Fiche
viz.debate.title=Debat

viz.debate.error=Het debat is onbekend.

# argument options
argument.options.viz.debate.see=Zie debat
argument.options.viz.citation.see=Zie de quotes

argument.options.edit.debate.create=Creëer het debat
argument.options.edit.argument.add=Voeg een argument toe
argument.options.edit.citation.add=Voeg een quote toe
argument.options.edit.citation.addok=Voeg een ondersteunende quote toe
argument.options.edit.citation.addshaded=Voeg een nuancerende quote toe
argument.options.edit.citation.addnotok=Voeg een verwerpende quote toe
argument.options.edit.change.shade=$Changer pour / contre

# debate and text options
debate.options.edit.argument.add=Voeg een overkoepelende mening toe (argumentatieniveau n°1) 
debate.options.edit.argument.add.yes=Voeg een argument toe ten gunste van de ja
debate.options.edit.argument.add.no=Voeg een argument toe ten gunste van de nee
# citation options
excerpt.options.viz.text.see=Zie de tekst
excerpt.options.viz.excerpts.see=Zie gelijkaardige fragmenten
excerpt.options.viz.excerpts.arguments=Toon de meningen
excerpt.options.edit.argument.add=Vat de mening samen 
excerpt.add.instruction=Om een quote te selecteren, klik op de icoon van de gekozen quote en vervolgens op de knop "creëer de link" onderaan de pagina.

# tag
viz.tag.pill.cartography=Debatten
viz.tag.pill.cited=Meningen / Quotes
viz.tag.pill.texts=Teksten
viz.tag.pill.radiography=Hiërarchie
viz.tag.pill.details=Kaart
viz.tag.title=Dossier
viz.tag.arguments=Meningen en quotes gekoppeld aan het dossier
viz.tag.debates=Debatten gekoppeld aan het dossier
viz.tag.texts=Teksten gekoppeld aan het dossier
viz.tag.hierarchy.parents=Ouderdossiers : 
viz.tag.hierarchy.children=Kinderdossiers : 

viz.tag.texts.own=bevat <span class="text-primary">{0}</span> mening(en) en <span class="text-primary">{1}</span> quotes
viz.tag.texts.stats.texts=bevat <span class="text-primary">{0}</span> tekst(en)
viz.tag.texts.stats.arguments=<span class="text-primary">{0}</span> mening(en) gekoppeld aan tekst(en)

viz.tag.error=Dit dossier is onbekend

viz.tag.argsexcs.pov.1=Quotes
viz.tag.argsexcs.pov.0=Argumenten

viz.tag.see=$Voir le tag

viz.tag.arguments.citations=$Dans ce dossier
viz.tag.arguments.parents.citations=$Dans des dossiers plus larges
viz.tag.arguments.children.citations=$Dans des sous-dossiers

viz.tag.arguments.citations.empty=$Ce dossier ne contient pas de citation
viz.tag.arguments.citations.parents.empty=$Il n'y a pas de dossier plus large
viz.tag.arguments.citations.children.empty=$Il n'y a pas de sous-dossier

viz.tag.arguments.drag.citations=$Ce dossier

viz.tag.debates.debates.empty=$Ce dossier ne contient pas de débat

viz.citations.empty=$Il n''y a pas de citations dans lié à cette contribution
viz.citations.arguments.empty=$Il n''y a pas de citations dans les arguments du débat
viz.citations.positions.empty=$Il n''y a pas de citations dans les positions du débat
viz.citations.positions.subdebate.empty=$Il n''y a pas de citations dans les positions du sous-débat

viz.positions.actor.empty=$Cet acteur ne s''est positionné dans aucun débat.
viz.positions.actor.title=$Degré d’accord ou de désaccord d’acteurs avec les positions défendues par {0}
viz.positions.actor.debate.title=$Proximité/éloignement des positions défendues par {0} par rapport à celles de {1} dans divers débats
viz.positions.actor.debate.close=$Proche
viz.positions.actor.debate.far=$Eloigné

viz.citations.empty.test=essais

#
# validate
#
validate.link.title=linken tussen argumenten valideren
validate.link.none=Er zijn momenteel geen link te valideren, u kunt later terugkomen ...
validate.link.question=Zijn deze twee argumenten vergelijkbaar?
validate.link.unconfirmed=De argumenten hebben geen betrekking op elkaar.
validate.link.confirmed=Deze argumenten zijn {0}
validate.link.select.label=Deze argumenten zijn?
validate.link.shade.0=lijken op elkaar
validate.link.shade.1=door elkaar gekwalificeerd
validate.link.shade.2=in tegenmening tot elkaar
validate.submit=Valideren
validate.success=van het verzoek
validate.error=Er is een fout opgetreden bij het werken aan uw verzoek. Probeer het opnieuw of neem contact op met de beheerder.
validate.argument.success=Uw argument is succesvol opgeslagen.
validate.link.success=Uw link is succesvol opgeslagen.

validate.argument.title=Valideer mening
validate.argument.instructions=Webdeb transformeert automatisch naar argumenten tweets of uittreksels uit geïmporteerde teksten. \
   U kunt deze argumenten op deze pagina valideren. <br> \
   Aan de linkerkant vindt u het originele fragment, het argument wordt aan de rechterkant geplaatst. <br> <br> \
   Kies "nee" als u vindt dat dit argument geen betekenis heeft. <br> \
   Kies "ja" als u van mening bent dat deze argumenten van belang zijn; u kunt het afgeleide argument indien nodig bijwerken \
   bijvoorbeeld over de auteurs en of onderwerpen.
validate.argument.none=Er zijn momenteel geen argument om te bevestigen, u kunt later terugkomen ...
validate.argument.question=Wil je dit fragment en argument toevoegen aan de database?
validate.argument.confirmed=Dit argument is toegevoegd aan de kennisbank.
validate.argument.unconfirmed=Dit argument wordt niet aan de kennisbank worden toegevoegd.
validate.argument.edit.title=Bekijk de eigenschappen van dit argument - Stap
validate.argument.label=Afgeleid argument

#
# user settings
#
settings.title.true=Mijn account
settings.title.false=Account van {0}
settings.profile.pill.true=Mijn profiel
settings.profile.pill.false=Profiel
settings.contributions.pill.true=Mijn bijdragen
settings.contributions.pill.false=Bijdragen
settings.mygroup.pill=Mijn abonnementen
settings.manage.group.pill=Mijn groepen
settings.feeder=Externe bronnen
settings.admin=Sitebeheer
settings.profession=Beheer functies
settings.claim=$Signalements

# group management
group.change.tooltip=Bijdragen in deze groep zijn {0}, ledenprofielen zijn {1}.
group.change.modal.title=Huidig bereik wijzigen?
group.change.modal.subtitle=$Vous êtes actuellement dans le groupe
group.change.modal.desc=U kunt een andere van uw groepen als uw huidige bereik selecteren. Als u overschakelt naar een privé groep, gaat u \
   alleen die privé-bijdragen zien. Je huidige bereik wordt standaard gebruikt wanneer je een bijdrage toevoegt, maar \
   je kunt nog steeds een andere groep opgeven voordat je een nieuwe bijdrage verzendt. <br> Je kunt je <a href="{0}"> abonnementen hier controleren </a>.
group.change.notmember=Je bent geen lid van deze groep
group.change.error=Er is een fout opgetreden tijdens het verwerken van uw aanvraag.
group.member.visibility.0=publiek
group.member.visibility.1=privé voor groep
group.member.visibility.2=privé
group.contribution.visibility.0=publiek
group.contribution.visibility.1=privé voor groep
group.contribution.visibility.2=privé
group.visibility.0=publiek
group.visibility.1=privé oor groep
group.visibility.2=privé

group.index.title=$Liste des groupes sur WebDeb
group.gen.current=$Vous êtes actuellement dans le groupe
group.gen.members.search=$Rechercher parmis les membres du groupe

group.isopen.true=$Groupe ouvert
group.isopen.false=$Groupe uniquement accessible sur invitation ou sur demande
group.isdefault=Dit is uw standaardgroep.
group.isfollowed=$Vous suivez ce groupe.
group.isadmin=$Vous êtes administrateur de ce groupe.
group.ismember=$Vous êtes membre de ce groupe.

group.search.placeholder=$Search group by name
groupe.search.open=$groupe ouvert
groupe.search.followed=$groupe suivis
groupe.search.isadmin=$admin
groupe.search.ismember=$membre
group.search.public=$Les contributions de ce groupe sont publiques
group.search.private=$Les contributions de ce groupe sont privées

group.seeall.label=Bekijk alle bijdragen
group.seeall.title=Bekijk alle bijdragen van deze groep
group.seeall.page.title=Bekijk alle bijdragen van groep {0} <i class="small-font"> ({1}) </ i>

groupe.join.label=$Rejoindre le groupe
groupe.join.ask.label=$Demander à rejoindre le groupe

group.leave.label=Groep verlaten
group.leave.title=Verlaat deze groep (je zal niet in staat zijn om bij te dragen in deze groep)
group.leave.modal.text=Weet je zeker dat je de groep {0} wilt verlaten? U zult privé bijdragen van deze groep niet kunnen zien.
group.leave.modal.title=Verlaat deze groep?
group.leave.error=Er is een fout opgetreden bij het verwerken van uw aanvraag ...
group.leave.isdefault=Je mag je standaardgroep {0} niet verlaten, stel eerst een andere standaard in, dan \
   kunt u deze groep te verlaten.
group.leave.ok=Je hebt de groep {0} succesvol verlaten.
group.leave.nok=Je mag de groep {0} niet verlaten.

group.switch.title=Gebruik deze groep als standaard
group.switch.label=Gebruik als standaard
group.switch.modal.title=Verander standaardgroep?
group.switch.modal.text=Je kunt tijdens je huidige weergave overschakelen naar een andere groep. Je zult dan in staat zijn om particuliere bijdragen van die groep te zien. \
   Overschakelen naar groep {0} als uw standaard?
group.switch.ok=U stelde groep {0} met succes in als uw standaard.

group.join.new=Word lid van een nieuwe groep
group.join.modal.title=Word lid van een nieuwe groep?
group.join.modal.search.addon=Zoeken naar een groep
group.join.modal.search.place=Begin met typen om naar groepen te zoeken
group.join.modal.instructions=U kunt uw zoekopdracht starten met <span class="fa fa-search"> </ span>.
group.join.modal.search.title=Begin met zoeken naar groepen
group.join.modal.confirm.title=Deelnemen aan geselecteerde groep
group.join.modal.confirm.btn=Deelnemen aan groep
group.join.ok=Je bent lid geworden van groep {0}.

group.manage.none=Je bent nog geen eigenaar van een groep
group.manage.new=Maak een nieuwe groep
group.members.showmore=Toon alle leden
group.revoke.modal.true.text=Weet je zeker dat je lid {0} van deze groep wilt verwijderen?
group.revoke.modal.true.title=Leden van groep verwijderen?
group.revoke.modal.false.text=Weet je zeker dat je lid {0} wilt autoriseren om opnieuw lid te worden van deze groep?
group.revoke.modal.false.title=Autoriseer lid in groep?
group.manage.member.goto.contrib=Bekijk zijn / haar bijdragen
group.member.revoke.true=lid intrekken
group.member.revoke.false=machtig lid
group.member.you.1=u draagt bij aan deze groep
group.member.you.2=u beheert deze groep
group.member.you.3=u beheert deze groep
group.revoke.ok.true=Lid {0} is succesvol verwijderd uit groep {1}.
group.revoke.ok.false=Lid {0} is succesvol geautoriseerd in groep {1}.
group.member.changerole=Change member's role

group.manage.actions=Acties op groep
group.manage.actions.desc=Selecteer hieronder een optie
group.manage.edit.label=Bewerk groepsdetails
group.manage.edit.tooltip=Bewerk groepsdetails (naam, beschrijvingen, rechten, etc.)
group.manage.mark.label=Bijdragen markeren
group.manage.mark.tooltip=Markeer en / of valideer bijdragen in deze groep
group.manage.merge.label=Samenvoegen naar openbare site
group.manage.merge.tooltip=Voeg gevalideerde bijdragen samen in de 'webdeb public' site
group.manage.empty.label=Lege groep
group.manage.empty.tooltip=Verwijder alle leden en bijdragen van deze groep
group.manage.close.label=Groep sluiten
group.manage.close.tooltip=Sluit groep en verwijder alle bijdragen van deze groep
group.manage.changerole.label=Change member roles
group.manage.changerole.tooltip=Change the role of any member of the group

group.manage.create.title=Maak een nieuwe groep
group.manage.edit.title=Bewerk de details van de groep
group.manage.label.name=Groepsnaam (identificerend) *
group.manage.error.name=De naam is verplicht
group.manage.error.name.unique=Deze naam bestaat al, geef een andere op
group.manage.toplabel.isopen=Deze groep vereist geen uitnodigingen?
group.manage.toplabel.ispedagogic=$Ce groupe est-il à finalité pédagogique ?
group.manage.label.isopen=Deze groep is publiek toegankelijk
group.manage.label.description=Beschrijving (optioneel)
group.manage.place.description=Een meer uitgebreide beschrijving van deze groep
group.manage.label.groupColor=Kleur van de groep *
group.manage.label.contribution.visibility=Bijdragen zichtbaarheid
group.manage.error.contribution.visibility=Selecteer een bijdragezichtbaarheid
group.manage.label.member.visibility=zichtbaar voor leden *
group.manage.error.member.visibility=Selecteer een ledenzichtbaarheid
group.manage.label.color=Groepskleur *
group.manage.error.color.mandatory=Selecteer een groepskleur
group.manage.error.color.format=De code moet een hexadecimaal getal zijn tussen 000000 en FFFFFF
group.manage.label.permissions=Machtigingen voor bijdragen
group.manage.label.permission.2=Elke bijdrager die een bijdrage kan bekijken, mag deze bewerken.
group.manage.label.permission.3=Elke bijdrager die een bijdrage kan bekijken, kan deze verwijderen.
group.manage.label.permission.8=Bijdragen kunnen na validatie door een groepseigenaar in de standaard 'openbare' groep worden geduwd.
group.manage.label.owner.permission=Rechten voor groepseigenaren
group.manage.label.permission.6=Groepseigenaren kunnen ledengegevens bewerken.
group.manage.label.user.help=Gebruikershulp voor codering.
group.manage.label.permission.13=De automatische tekstannotatie is uitgeschakeld.
group.manage.label.permission.13.bis=Automatische annotatie van tekst uitschakelen.
group.manage.label.permission.14=Suggesties voor de meningclassificatie zijn uitgeschakeld.
group.manage.label.permission.14.bis=Suggesties voor meningclassificatie uitschakelen.
group.manage.label.permission.15=$Les non membres du groupe peuvent éditer la contribution.
group.manage.btn.title=Bewaar deze groep
group.manage.btn.create=Groep maken
group.manage.btn.edit=Update groep
group.manage.contributions.tooltip=Zie bijdragen in deze groep
group.manage.contributions.label=Alle bijdragen
group.save.ok=De groep {0} is succesvol opgeslagen.

group.mail.members=E-mail verzenden naar leden
group.mail.members.tooltip=E-mail verzenden naar leden
group.mail.noTitle=Geen titel
group.mail.noContent=Geen inhoud
group.mail.title=Groepsmail
group.mail.mailTitle=Titel
group.mail.mailContent=Content
group.mail.btn.title=Verstuur e-mail

group.invite.member=Leden toevoegen
group.invite.member.tooltip=Nieuwe leden toevoegen of uitnodigen voor deze groep
group.invite.title=Nodig of voeg nieuwe leden toe aan de groep
group.invite.instructions=Vul de nieuwe leden in om uit te nodigen. Onbekende adressen worden uitgenodigd via e-mail.
group.invite.members.label=Zoeken op naam
group.invite.members.email.place=invite@mail.com
group.invite.members.role.select=Selecteer zijn rol
group.invite.members.role.error=u moet een rol in de lijst selecteren
group.invite.btn.title=Alle bovenstaande leden in groep toevoegen, onbekende leden worden uitgenodigd om deel te nemen aan het platform.
group.invite.btn=Leden toevoegen of uitnodigen
group.invite.ok=Bekende bijdragers zijn toegevoegd in groep {0}, anderen zijn uitgenodigd om via e-mail lid te worden van Webdeb.
group.invitation.error=Het lijkt erop dat u leden probeert uit te nodigen voor een groep die u niet bezit ...
group.invitation.notinvited={0} niet kon worden uitgenodigd voor de groep.
group.invitation.nomail=De uitnodigingsmail naar {0} kon niet worden verzonden.

group.mark.title=Valideer bijdragen in groep
group.mark.desc=U kunt elke bijdrager uit de lijst selecteren en hun bijdragen bekijken om ze te valideren. \
   Alleen gevalideerde bijdragen zijn beschikbaar voor samenvoeging in de "webdeb public" -database. <br> \
   De grijze teksten zijn degene met wie deze bijdrager heeft gewerkt, maar die hij / zij zichzelf niet heeft geïmporteerd.
group.mark.validated=Valideren
group.mark.showall=Toon alle bijdragen
group.mark.submit=Sla valideren op
group.mark.validateall=Tick "ja" overal
group.mark.back=Terug naar mijn groepen
group.mark.success=Uw validatie zijn succesvol opgeslagen.

group.merge.checkall=Check alles
group.changerole.title=Wijzig de ledenrol in
group.changerole.instructions=Wijzig de ledenrol door eenvoudige bijdrager rechten of moderator rechten te verlenen
group.changerole.btn.title=Breng wijzigingen aan
group.changerole.btn=Breng wijziging van rollen aan
group.merge.title=Voeg bijdragen van groep {0} samen in WebDeb Public
group.merge.desc=U kunt gevalideerde bijdragen verzenden naar de WebDeb openbare site door ze te selecteren en de wijzigingen in te dienen. \
   Verzonden bijdragen worden niet weergegeven in hun oorspronkelijke groep.
group.merge.sendto=naar WebDeb public
group.merge.submit=Samenvoegen naar WebDeb public
group.merge.success=U hebt de geselecteerde bijdragen succesvol naar de openbare database gestuurd.
group.merge.error=Er is een fout opgetreden bij het verzenden van de geselecteerde bijdragen naar de openbare database, neem contact op met uw beheerder.

group.empty.modal.title=Groep leegmaken
group.empty.modal.text=Weet u zeker dat u alle leden wilt afmelden en alle bijdragen uit de groep <span \
class="strong"> {0} </ span>? Elke bijdrage die niet is samengevoegd in de "webdeb public" -groep zal permanent worden verwijderd. \
  <span class="strong"> Deze bewerking kan niet ongedaan worden gemaakt. </ span>

group.close.modal.title=Groep sluiten
group.close.modal.text=Weet u zeker dat u groep <span class="strong"> {0} </ span> wilt sluiten? Alle leden worden afgemeld, alle bijdragen \
   die niet zijn samengevoegd in de "webdeb public" -groep zullen permanent worden verwijderd en deze groep zal ook \
   verwijderd worden. <span class="strong"> Deze bewerking kan niet ongedaan worden gemaakt. </ span>

group.close.ok.false=De groep {0} is ontdaan van zijn leden en geschreven.
group.close.ok.true=De groep {0} is verwijderd en al zijn bijdragen zijn verwijderd.
group.close.error=De groep {0} is niet leeggemaakt of verwijderd, neem contact op met de beheerder.
group.close.nomail=Sommige leden zijn mogelijk niet gewaarschuwd dat de groep is gesloten.

group.preferences.btn.label=Voorkeuren

group.preferences.save.label=Sla voorkeuren op
group.changefollow.success=Gevolgde groepenlijst succesvol opgeslagen!
group.preferences.filterfollowed.show.btn=Toon alle groepen
group.preferences.filterfollowed.hide.btn=Verberg niet gevolgde groepen
group.preferences.changefollow.btn=Wijzig gevolgde groepen
group.preferences.changefollow.follow.btn=Volg
group.preferences.changefollow.unfollow.btn=Volg niet meer

# feeders and imports
admin.csv.title=Importeer van csv-bestand
admin.csv.new=Import
admin.csv.report.desc=Laatste import en hun rapporten
admin.csv.report.date=Datum importeren
admin.csv.report.files=Lijst met bestanden
admin.csv.report.none=Er kon geen eerdere import worden gevonden
admin.csv.modal.title.0=Selecteer csv-bestanden om acteurs en / of affiliaties te importeren
admin.csv.submit.title.0=Acteurs importeren uit deze csv-bestanden
admin.csv.instructions.0=Selecteer csv-bestand (en) om acteurs en / of affiliaties in {0} te importeren. U kunt een van beide importeren of \
   beide bestanden tegelijk en een csv verslag zal daarna worden gegenereerd.
admin.csv.modal.title.3=$Choisissez les fichiers csv pour importer des citations et/ou des liens citations / auteurs
admin.csv.submit.title.3=$Importer les citations ou auteurs contenus dans les fichiers csv
admin.csv.instructions.3=$Choisissez le(s) fichier(s) csv pour importer des citations et/ou auteurs dans {0}. \
   Vous pouvez importer l''un ou l''autre fichier, ou les deux en une fois et un rapport au format csv sera généré.
admin.csv.btn=Bladeren ...
admin.csv.tooltip=Selecteer een .csv-bestand om te importeren
admin.csv.actor=Actor CSV-bestand
admin.csv.affiliation=Affiliatie csv-bestand
admin.csv.citation=$Fichiers csv de citations
admin.csv.author=$Fichiers csv d''auteurs
admin.csv.charset=tekenset coderen
admin.csv.charset.UTF-8=UTF-8 (UNIX of open / libreoffice)
admin.csv.charset.ISO-8859-1=ISO-8859-1 (Latin-1 - Windows excel)
admin.csv.separator=CSV-kolomscheidingsteken
admin.csv.separator.comma=Komma (",")
admin.csv.separator.semicolon=punt-komma (";")
admin.csv.group=Gekozen groep voor de import bestaat niet
admin.csv.nocharset=Geen tekenset geselecteerd
admin.csv.empty=Er moet minimaal één csv-bestand worden gegeven
admin.csv.result=Het importresultaat kan hier <a href="{0}" target=_blank> worden gedownload </a>.
admin.csv.noresult=Het importresultaat kon niet worden opgeslagen in een csv-bestand, neem contact op met de beheerder om \
   meer te weten over het resultaat (in logbestand) en om het probleem op te lossen.
admin.rss.title=RSS-feeders
admin.rss.new=Voeg een nieuwe RSS-bron toe
admin.rss.edit.btn=Eigenschappen bewerken
admin.rss.ignore=Genegeerde bron
admin.rss.showmore=Toon alle bronnen
admin.rss.actions=meer acties
admin.rss.activate.true.btn=Activeer RSS-bron
admin.rss.activate.true.title=Activeer deze RSS-bron (er worden teksten van die bron opgehaald)
admin.rss.activate.false.btn=Bron deactiveren
admin.rss.activate.false.title=Deactiveer deze RSS-bron (teksten zullen niet meer worden opgehaald)
admin.rss.remove.btn=Verwijder de bron
admin.rss.remove.title=Verwijder deze RSS-bron permanent
admin.rss.edit.title=Bewerk RSS-bron
admin.rss.create.title=Nieuwe RSS-bron toevoegen
admin.rss.label.name=Bronnaam *
admin.rss.place.name=Een RSS-bron
admin.rss.error.name=De naam is verplicht
admin.rss.label.category=Categorie van de inhoud *
admin.rss.place.category=bijv. internationale politiek,...
admin.rss.error.category=De categorie is verplicht
admin.rss.label.subcategory=Subcategorie (optioneel)
admin.rss.place.subcategory=bijv. mening, kolom, ...
admin.rss.label.url=Bronadres *
admin.rss.place.url=http: //some.source.rss
admin.rss.error.url=De URL is verplicht
admin.rss.duplicate.url=Deze url bestaat al
admin.rss.label.country=Oorspronkelijk bronland *
admin.rss.place.country=Selecteer een land
admin.rss.error.country=Het land is verplicht
admin.rss.edit.btn.title=Sla eigenschappen van deze RSS-bron op
admin.rss.btn.create=RSS toevoegen
admin.rss.btn.edit=Update RSS

admin.rss.activate.modal.false.title=Deactiveren RSS-feedbron
admin.rss.activate.modal.false.text=Weet je zeker dat je {0} als RSS-bron wilt deactiveren om teksten op te halen?
admin.rss.activate.modal.true.title=Activeer de RSS-feedbron
admin.rss.activate.modal.true.text=Weet je zeker dat je {0} wilt activeren als RSS-bron om teksten op te halen?
admin.rss.remove.modal.title=RSS-feedbron verwijderen?
admin.rss.remove.modal.text=Weet u zeker dat u volledig {0} uit de RSS-bronnenlijst wilt verwijderen?

admin.rss.nolist=Kan lijst met RSS-bron niet ophalen van externe WDTAL-service
admin.rss.notfound=Kan de vereiste RSS-bron niet vinden van externe service
admin.rss.save.success=De RSS-bron is succesvol opgeslagen
admin.rss.save.error=De RSS-bron is niet opgeslagen vanwege een fout van een externe WDTAL-service
admin.rss.activate.true.success=De RSS-bron is succesvol geactiveerd
admin.rss.activate.true.error=Kan de RSS-bron niet activeren vanwege een fout van de WDTAL-service
admin.rss.activate.false.success=De RSS-bron is succesvol gedeactiveerd
admin.rss.activate.false.error=Kan de RSS-bron niet deactiveren vanwege een fout van de WDTAL-service
admin.rss.remove.success=De RSS-bron is met succes verwijderd
admin.rss.remove.error=Kan de RSS bron te verwijderen vanwege een fout van WDTAL dienst

admin.twitter.title=Twitter-accounts
admin.twitter.new=Twitter-account toevoegen
admin.twitter.actions=meer acties
admin.twitter.actions.desc=Acties voor dit Twitter-account
admin.twitter.edit.btn=Eigenschappen bewerken
admin.twitter.remove.btn=Account verwijderen
admin.twitter.remove.title=Verwijder Twitter-account definitief
admin.twitter.showmore=toon alle accounts
admin.twitter.edit.title=Twitter-account bewerken
admin.twitter.create.title=Voeg een nieuw Twitter-account toe
admin.twitter.label.fullname=Volledige naam *
admin.twitter.place.fullname=Zoeken naar een bestaande actor
admin.twitter.error.fullname=De naam is verplicht en moet een bestaande actor zijn
admin.twitter.error.idfullname=Selecteer expliciet een naam uit de lijst met suggesties
admin.twitter.label.account=Twitter-accountnaam *
admin.twitter.place.account=@Account (hoofdlettergevoelig)
admin.twitter.error.account=Het account is verplicht
admin.twitter.duplicate.account=Dit account bestaat al
admin.twitter.error.gender=Het geslacht is verplicht
admin.twitter.label.languages=Tweets taal (talen)
admin.twitter.place.languages=Selecteer een taal
admin.twitter.edit.btn.title=Sla eigenschappen van dit Twitter-account op
admin.twitter.btn.create=Account toevoegen
admin.twitter.btn.edit=Account bijwerken

admin.twitter.remove.modal.title=Twitter-account verwijderen
admin.twitter.remove.modal.text=Weet je zeker dat je het Twitter-account van {0} wil verwijderen?

admin.twitter.nolist=Kan de lijst met Twitter-accounts niet ophalen vanwege een fout van de externe WDTAL-service
admin.twitter.notfound=Kan het vereiste Twitter-account niet ophalen vanwege een fout van de externe WDTAL-service
admin.twitter.save.success=Het Twitter-account is succesvol opgeslagen
admin.twitter.save.error=Kan het Twitter-account niet opslaan vanwege een fout van de externe WDTAL-service
admin.twitter.remove.success=Het Twittter-account is succesvol verwijderd
admin.twitter.remove.error=Kan het Twitter-account niet verwijderen vanwege een fout van de externe WDTAL-service

# profession administration
admin.profession.error.spelling="Geen functie vertaling opgegeven"
admin.profession.error.lang="Geen vertaaltaal opgegeven"
admin.profession.error.gender="Geen woord geslacht opgegeven"
admin.profession.error.haslink.substitute=""
admin.profession.error.haslink.link=""
admin.profession.error.recursivelink="De functie kan niet aan zichzelf gelinkt worden"
admin.profession.error.notFound="Functie niet gevonden"

admin.search.profession.label=Zoek een functie
admin.freeSource.edit.title=Bewerk bronnen zonder copyright
admin.freeSource.edit.name=https: //www.wikidata.org, www.wikidata.org, wikidata.org
admin.freeSource.edit.label=URL of domeinnaam
admin.freeSource.invalidDomainName=De URL of domeinnaam is niet geldig
admin.freeSource.success=vrije bron (nen) is opgeslagen in de knowledge base
admin.freeSource.delete.success.title=Vrije bron verwijderd
admin.freeSource.delete.success.msg=Vrije bron succesvol verwijderd
admin.freeSource.confirm.title=Weet u zeker dat u deze vrije bron wilt verwijderen?
admin.freeSource.confirm.msg=Bevestig dat u de bron "{0}" wilt verwijderen.

admin.search.profession.instructions=begin met het typen van een beroepnaam om naar functies te zoeken
admin.search.profession.byname=Sorteer op naam
admin.edit.profession.names=Bewerk functie
admin.edit.profession.haslink=Bewerk sub-links
admin.edit.profession.names.tooltip=Bewerk functie
admin.edit.profession.haslink.tooltip=linken bewerken
admin.edit.profession.haslink.title=Bewerk gekoppelde functies
admin.edit.profession.title=Voeg een nieuwe functie toe
admin.edit.profession.admin.title=Bewerk functie
admin.edit.profession.label.gender=geslacht
admin.edit.profession.label=Functie naam
admin.edit.profession.edit.btn=Bevestigen
admin.edit.profession.edit.name=Directeur, leraar, voetballer, ...
admin.edit.profession.edit.like=De generieke functie
admin.edit.profession.edit.displayhierarchy=Afficher les sous fonctions
admin.edit.profession.edit.sublink=Gekoppelde functies
admin.edit.profession.edit.name.title=Bewerk vertalingen
admin.merge.professions.ok=Functies samengevoegd met succes
admin.profession.modify.btn=Update-functie
admin.merge.professions.title=Voeg functies samen
admin.merge.professions.label=Functie om mee samen te voegen
admin.merge.professions=Voeg functie samen
admin.merge.professions.tooltip=Samenvoegen met een andere
admin.profession=Functies
profession.professionNames.delete.confirm.title=Weet je zeker dat je deze vertaling wilt verwijderen?
profession.professionNames.delete.confirm.text=Bevestig de verwijdering van de vertaling "{0}"
profession.professionHasLink.delete.confirm.title=Weet je zeker dat je deze link wilt verwijderen?
profession.professionHasLink.delete.confirm.text=Bevestig het verwijderen van de link met "{0}"
edit.profession.description=U hebt een functie ingevoegd die niet in Webdeb staat. We vragen je om: \
<br> - De spelling van de naam van de functie te controleren \
<br> - Zijn taal en zijn geslacht te controleren \
<br \> <br \> Voeg eventueel andere versies van de naam van de functie toe (andere taal, ander geslacht) met de "+" knop. <br>

profession.name.error=(Waarschuwing) Het is het beste om de volgende elementen in de naam van een functie te vermijden: \
<br> - de naam van een organisatie \
<br> - de naam van een land of een nationaliteit \
Catawiki - de woorden "ere", "ex", "emeritus" of "federale"

profession.update.success=Beroep {0} succesvol bijgewerkt

# contributor administration
admin.search.contributor.addon=Zoeken naar bijdrager
admin.search.contributor.place=Begin met typen om te zoeken naar bijdragers
admin.search.contributor.title=Begin met zoeken naar bijdragers
admin.search.contributor.title.byname=Bijdragers sorteren op naam
admin.search.contributor.title.bydate=Bijdragers sorteren op inschrijvingsdatum
admin.search.contributor.instructions=U kunt uw zoekopdracht starten met <span class="fa fa-search"> </ span>.
admin.search.goback=Ga terug naar groepsbeheer
admin.update.role=Rol bijwerken
admin.change.role.ok=The contributor role has been successfully changed.
admin.revoke.modal.true.text=Weet u zeker dat u {0} van het hele platform wilt verwijderen?
admin.revoke.modal.true.title=Bijdrager verwijderen?
admin.revoke.modal.false.text=Weet u zeker dat u {0} toestemming wilt geven voor toegang tot het platform?
admin.revoke.modal.false.title=autoriseren inzender?
admin.revoke.ok.true={0} is met succes verbannen van het platform.
admin.revoke.ok.false={0} is succesvol geautoriseerd om verbinding te maken met het platform.
role.label.0=alleen bekijken
role.label.1=inzender
role.label.2=manager
role.label.3=beheerder

#
# help page
#
help.navbar.title=Help
help.title=Help-pagina
help.desc=Je vindt hier handleidings met betrekking tot het gebruik van het WebDeb-platform
help.novideo=Uw browser ondersteunt geen video's ...
help.modal.title=Help
help.btn.title=Display help

help.title.entry.actor=Voer een actor in
help.title.group=Beheer een groep
help.title.help.subscribe=Abboneer op WebDeb

help.manual.entry.actor=entry.actor.pdf
help.manual.group=group.pdf
help.manual.help.subscribe=help.subscribe.pdf

help.video.entry.actor=mWEPrnGmf78

#
# entities (NLP helps)
#
entities.label.organisation=Organisatie
entities.label.personne=Persoon
entities.label.lieu=Place
entities.label.temps=Time
entities.label.profession=Function
entities.label.conn=Connector

#
#mail content
#
mail.from=WebDeb Collaborative Platform <admin@webdeb.be>
mail.from2=No Reply - WebDeb Platform <admin@webdeb.be>
mail.sign=WebDeb-projectteam

mail.subject.subscribe=WebDeb-platform - bevestig uw e-mail
mail.title.subscribe=Bevestig uw e-mailadres om uw abonnement op Webdeb te voltooien
mail.content.subscribe=Welkom bij het WebDeb-platform. <br> Klik op de onderstaande link om uw inschrijving te voltooien en een bijdrage te leveren.
mail.btn.subscribe=Finaliseren

mail.subject.confirm_subscribe=WebDeb Platform - Welkom bij de community van webdebers
mail.title.confirm_subscribe=Welkom bij de community van Webdebers
mail.content.confirm_subscribe=Welkom bij het WebDeb-platform. Uw account is nu gevalideerd. <br> Tot binnenkort op WebDeb.
mail.btn.confirm_subscribe=Ga naar Webdeb

mail.subject.change_mail=WebDeb-platform - verander uw e-mail
mail.title.change_mail=U wilt uw inschrijvingse-mail wijzigen?
mail.content.change_mail=U vroeg om uw inschrijvingse-mail te wijzigen. Bevestig deze nieuwe e-mail als uw nieuwe adres door \
   te klikken op onderstaande link.
mail.btn.change_mail=Bevestigen

mail.subject.change_password=WebDeb Platform - Stel uw wachtwoord opnieuw in
mail.title.change_password=U bent uw wachtwoord vergeten op Webdeb?
mail.content.change_password=U vroeg om een nieuw wachtwoord? Klik op onderstaande link en vul een nieuw wachtwoord in voor uw account.
mail.btn.change_password=wachtwoord opnieuw instellen

mail.subject.confirm_change_password=WebDeb Platform - Wachtwoord gewijzigd
mail.title.confirm_change_password=Je hebt je wachtwoord succesvol gereset.
mail.content.confirm_change_password=Uw wachtwoord is succesvol gereset. U kunt het nu gebruiken om verbinding te maken met het platform.
mail.btn.confirm_change_password=Ga naar WebDeb

mail.subject.unknown_change_password=WebDeb-platform - onbekend adres
mail.title.unknown_change_password=Heeft u geprobeerd uw wachtwoord te herstellen?
mail.content.unknown_change_password=U of iemand anders probeerde zijn wachtwoord voor het WebDeb-platform te herstellen. \
   We kennen uw adres echter nog niet. Als u ook uw e-mailadres voor de inschrijving bent vergeten, neem dan contact met ons op. Anders \
   heeft iemand anders een fout gemaakt, maar voel je vrij om met ons mee te doen.
mail.btn.unknown_change_password=Ga naar WebDeb

mail.subject.invite=WebDeb-platform - Uitnodiging om lid te worden van een groep
mail.title.invite=Je bent uitgenodigd om deel te nemen aan het Webdeb Platform
mail.content.invite=U bent uitgenodigd om deel te nemen aan groep <i> {0} </ i> door <i> {1} </ i> op het WebDeb-platform. Klik op onderstaande link om nu lid te worden.
mail.btn.invite=Schrijf u nu in

mail.subject.invite_existing=WebDeb-platform - toegevoegd aan een groep
mail.title.invite_existing=Je bent toegevoegd aan een groep
mail.content.invite_existing=Je bent toegevoegd aan groep <i> {0} </ i> door <i> {1} </ i> op het WebDeb-platform. Klik op onderstaande link om ons te bezoeken.
mail.btn.invite_existing=Ga naar mijn profiel

mail.btn.group_mail=Ga naar Webdeb

mail.subject.joined_group=WebDeb-platform - Een nieuw lid voor uw groep
mail.title.joined_group=Een bijdrager heeft zich bij uw groep gevoegd
mail.content.joined_group=<i> {1} </ i> is lid geworden van uw groep <i> {0} </ i>. Klik op de onderstaande link om uw groepsleden te bekijken.
mail.btn.joined_group=Ga naar mijn groepen

mail.subject.ban_from_group=WebDeb-platform - U bent verbannen uit een groep
mail.title.ban_from_group=Een moderator heeft je verbannen uit een groep
mail.content.ban_from_group=<i> {1} </ i> heeft je gebanned uit de groep <i> {0} </ i>. U kunt contact opnemen met de beheerder voor meer informatie.
mail.btn.ban_from_group=Ga naar WebDeb

mail.subject.unban_from_group=WebDeb-platform - u bent opnieuw toegelaten tot een groep
mail.title.unban_from_group=Een moderator heeft je opnieuw tot een groep toegelaten
mail.content.unban_from_group=<i> {1} </ i> heeft u opnieuw toegelaten tot groep <i> {0} </ i>. U hebt nu toegang tot de inhoud van deze groep.
mail.btn.unban_from_group=Ga naar WebDeb

mail.subject.group_closed=WebDeb-platform - Een van uw groepen is gesloten
mail.title.group_closed=Een groep is gesloten
mail.content.group_closed=<i> {1} </ i> heeft groep <i> {0} </ i> gesloten. Alle bijdragen zijn van het webdeb-platform verwijderd.
mail.btn.group_closed=Ga naar WebDeb

mail.subject.report_rss=WebDeb-platform - RSS-injectierapport
mail.title.report_rss=Rapport van de laatste RSS-feed

mail.subject.report_tweet=WebDeb-platform - Tweet injectierapport
mail.title.report_tweet=Rapport van de laatste feed met tweet

mail.userToUser.contact=$Contacter le contributeur
mail.userToUser.modal.title=$Contacter un contributeur
mail.userToUser.modal.desc=$Vous allez envoyez un email à {0}. Votre email lui sera communiqué afin qu''il puisse \
  vous répondre sur l''adresse mail que vous avez fournie à votre inscription ({1}).
mail.userToUser.answer=$Merci de répondre à ce mail à l''adresse <a class="primary" href="mailto:{0}">{0}</a>

# About
about.webdeb.title = Het WebDeb-project
about.webdeb.descr = Beschrijving van het projet
about.webdeb.jumbo=$WebDeb est une plateforme collaborative gratuite qui permet de\
<a href={0}>consulter</a> les arguments et contre-arguments sur une question de débat et les prises de positions des individus et organisations.<br><br>\
Dans le même esprit que Wikipédia, tout internaute peut y <a href={1}>contribuer</a> dès qu’il s’enregistre, s’il respecte les règles de la plateforme.<br><br> \
WebDeb est également utilisé comme outil <a href={2}>outil pédagogique</a>.
about.webdeb.content=$L’idée de la plateforme WebDeb a germé à l’issue de la recherche européenne KNOWandPOL (2006-2011), \
  coordonnée par l’UCLouvain et étudiant les rapports entre connaissances et politique. A l’issue de cette recherche, \
  une première plateforme expérimentale a été mise en place sous le nom de Wikideb. <br><br>\
  Les enseignements tirés de cette première expérience ont permis de dessiner un nouveau projet. Convaincue, \
  la Région wallonne a décidé de le financer dans le cadre de l’appel « Germaine Tillion ». Ce projet d’une \
  durée de 3 ans (2014-2017) a été mené par trois centres de recherche belges : \
  le Namur Digital Institute (<a href="https://nadi.unamur.be" target="_blank"}>NADI</a>) de l''Université de Namur \
  ainsi que le Groupe interdisciplinaire de Recherche sur la Socialisation, l''Education et la Formation (<a href="https://uclouvain.be/fr/chercher/girsef" target="_blank">GIRSEF</a>) \
  et le Centre de traitement automatique du langage (<a href="https://uclouvain.be/fr/instituts-recherche/ilc/cental" target="_blank">CENTAL</a>), \
  tous deux de l''UCLouvain. <br><br>\
Ce projet a débouché sur une première version d’une plateforme gratuite, collaborative, open source, multilingue et \
  multisectorielle, accessible à l’adresse <a href="https://webdeb.be"}>webdeb.be</a>. <br><br>\
Le développement de la plateforme a pu être poursuivi dans le cadre d’un projet de recherche financé par Innoviris (2017-2021) : \
  Jeunes et enjeux politiques. Ce projet, mené par le <a href="https://uclouvain.be/fr/chercher/girsef" target="_blank">GIRSEF</a> (UCLouvain), \
  le <a href="https://cevipol.centresphisoc.ulb.be/fr/accueil-cevipol" target="_blank">Cevipol</a> (ULB) \
  et <a href="https://www.vub.be/en/department/poli#research" target="_blank">POLI</a> (VUB) porte sur la conception \
  et l’évaluation d’un dispositif d’éducation au politique incluant notamment l’utilisation de WebDeb. Une expérimentation \
  de ce dispositif auprès de 1.200 élèves bruxellois a débouché sur une nouvelle version de la plateforme, qui est toujours en cours de perfectionnement.
about.webdeb.consult.title = Zoeken
about.webdeb.consult.content = WebDeb Het platform kan worden doorzocht door iedereen zonder \
een login. Het is mogelijk om in een oogopslag de posities te bekijken die door meerdere actoren worden ingenomen, de \
rechtvaardigingen of weerleggingen die zij in een debat binnenbrengen, evenals de verbanden tussen hun meningen.
about.webdeb.contribute.title = Bijdragen
about.webdeb.contribute.content = WebDeb biedt iedereen die wil deelnemen de mogelijkheid om <a href={0}> een bijdrage te leveren </a> aan de database, \
na gratis <a href={1}> aan te melden </a>, door hun eigen acteurs, meningenen en teksten in te voeren.
about.webdeb.usage.title = Gebruik cases
about.webdeb.usage.content = Het platform is momenteel ontworpen voor het grote publiek, scholen en \
universiteiten. Leraren kunnen zich abonneren op het platform en hun klasactiviteiten beheren: een mening bouwen \
, een toespraak in delen knippen, tekstanalyse uitvoeren... Wilt u meer weten? \
Aarzel niet om <a href="mailto:info@webdeb.be"> contact met ons op te nemen! <br> Het platform zal daarna worden geconfigureerd voor een \
journalistiek, politiek en industrieel gebruik.
about.webdeb.status.title = Projectvoortgang
about.webdeb.status.content = We testen momenteel een prototype met ongeveer vijftig studenten van een middelbare school. \
Met dit prototype kunnen gebruikers elk type bijdrage coderen en bekijken op verschillende grafische weergaven. \
   Het prototype maakt het ook mogelijk om groepen te maken en eraan deel te nemen, waardoor privéomgevingen worden gecreëerd (bijvoorbeeld voor klaslokalen). \
   De bronnen zijn beschikbaar op <a target="_blank" href="https://bitbucket.org/webdeb/webdeb-sources/"> bitbucket </a>.
about.webdeb.api.title = REST-queryinterface en natuurlijke taalverwerking
about.webdeb.api.content = We hebben een API ontwikkeld om te zoeken in de database die toegankelijk is \
als een REST-volledige webservice. Raadpleeg de <a target="_blank" href={0}> documentatie </a> voor meer informatie. \
Verder hebben we ook een Natural Language Processing API ontwikkeld om te helpen \
in de classificatie en codering van bijdragen. Raadpleeg voor meer informatie de <a target = "_ blank" \
href = {1}> documentatie </a>.

aboutus.webdeb = Projectactoren
aboutus.webdeb.desc = Het WebDeb-project wordt geleid door een consortium van universiteiten en onderzoekscentra, evenals \
   sponsors en industriële partners.
aboutus.webdeb.consortium = Consortium
aboutus.webdeb.consortium.ucl = Université Catholique de Louvain
aboutus.webdeb.consortium.unamur = Universiteit van Namen
aboutus.webdeb.partners = Sponsors en partners
aboutus.webdeb.promoters = Promoters
aboutus.webdeb.actors.team=$L''équipe actuelle
aboutus.webdeb.actors.formerteam=$Les anciens collaborateur
aboutus.webdeb.amelie.title=Amélie Cougnon
aboutus.webdeb.andre.title=Andre Bittar
aboutus.webdeb.bernard.title=Bernard Delvaux
aboutus.webdeb.fabian.title=Fabian Gilson
aboutus.webdeb.deniz.title=Deniz Uygur
aboutus.webdeb.martin.title=Martin Rouffiange
aboutus.webdeb.pyschobb.title=Pierre-Yves Schobbens
aboutus.webdeb.pierre.title=Pierre Beaulieu
aboutus.webdeb.robin.title=Robin Dumont
aboutus.webdeb.marie.title=Marie Defreyne
aboutus.webdeb.cedrick.title=Cédrick Fairon
aboutus.webdeb.andre = André was hoofd van Natural Language Processing (NLP) voor WebDeb en Research Fellow aan de \
  <a href="https://www.uclouvain.be/cental.html" target="_blank"> Cental </a>. Zijn doctoraat, verdedigd in 2010 aan de \
  <a href="http://www.univ-paris-diderot.fr/english/" target="_blank"> Université Paris Diderot </a>, richtte zich op de analyse \
  van temporele informatie in Franse teksten. Na zijn promotie bekleedde hij een postdoctorale onderzoeksfunctie in de Parsing \
  and Semantics groep op het <a href="http://www.xrce.xerox.com/" target="_blank"> Xerox Research Center Europe </a>, in \
  Grenoble. Meer recentelijk werkte hij als Senior Computational Linguist op <a href="http://www.ho2s.com/" target="_blank"> \
  Holmes Semantic Solutions </a>, een bedrijf dat is gespecialiseerd in NLP-services. Zijn interesses variëren van semantische roletikettering, \
  over sentiment- en emotie-analyse, opiniemonitoring, tot NLP voor het medische domein en temporele informatie-extractie. \
  <br> <a class="btn btn-link secondary pull-right" href="http://www.uclouvain.be/andre.bittar" target="_blank"> Meer info ... </a>

aboutus.webdeb.bernard = Bernard Delvaux is de initiator van het WebDeb-project. Hij bedacht het idee terwijl hij aan het werken was \
  op het Europese project <a href="http://www.knowandpol.eu/" target="_blank"> KNOWandPOL </a> (Knowledge and Policy), dat hij \
  mee leidde en dat het gebruik van kennis in het overheidsbeleid bestudeerde. Als socioloog werkt hij als onderzoeker aan de <a \
  href = "http://www.uclouvain.be/" target = "_ blank"> Université Catholique de Louvain </a>, waar hij lid is van <a \
  href = "https://www.uclouvain.be/girsef.html" target = "_ blank"> Girsef </a> (Interdisciplinaire onderzoeksgroep over socialisatie, \
  onderwijs en training). Buiten het WebDeb-project richt zijn onderzoek zich op vragen rond onderwijs (ongelijkheid, \
  segregatie, competitie tussen scholen). Meer recentelijk heeft hij belangmening ontwikkeld voor de evolutie van de relatie \
  tussen onderwijs en de maatschappij in een tijd van diepgaande maatschappelijke veranderingen. Zijn nieuwste boeken zijn getiteld <a href = "https://www.uclouvain.be/530706.html" \
  target = "_ blank"> "Une tout autre école" </a> en <a href = "http://www.deboecksuperieur.com/ouvrage/9782807301726-reflechir-lecole-de-demain" \
  target = "_ blank"> "Réfléchir l''école de demain" </a>. Hij is ook een toegewijde activist in de \
  grass roots movement <a href="http://www.toutautrechose.be/" target="_blank"> Tout Autre Chose </a>, waarin hij de \
  groep <a href="http://www.toutautrechose.be/toutautreecole" target="_blank"> Tout Autre École </a> leidt. \
  <br> <a class="btn btn-link secondary pull-right" href="http://www.uclouvain.be/bernard.delvaux" target="_blank"> Meer info ... </a>

aboutus.webdeb.cedrick=Titulaire d’un doctorat en linguistique informatique (Université Paris VII Diderot), agrégé en lettres, \
  Cédrick Fairon est professeur à l’Université catholique de Louvain où il enseigne la linguistique et l’ingénierie linguistique. \
  Actuellement doyen de la Faculté de philosophie, arts et lettres de l’UCLouvain, il a été longtemps directeur du \
  CENTAL, un laboratoire de recherche en traitement automatique du langage, qui analyse notamment les textes littéraires. \
  Il a été co-promoteur de la recherche WebDeb financée par la Région Wallonne. \
  <br><a class="btn btn-link secondary pull-right" href="https://uclouvain.be/fr/repertoires/cedrick.fairon" target="_blank">Plus d’’info...</a>

aboutus.webdeb.fabian = Fabian was tot september 2017 verantwoordelijk voor de informatietechnologische aspecten van het project. Fabian werkt in \
  de <a href="http://cosc.canterbury.ac.nz/" target="_blank> "> Afdeling Computerwetenschappen en Software Engineering </a> op de <a \
  href = "http://www.canterbury.ac.nz" target = "_ blank>"> University of Canterbury </a>. Hij bracht twee jaar door in de IT-consulting wereld \
  in een gelieerd bedrijf van <a href="http://www.accenture.com" target="_blank"> Accenture </a>, en maakte vervolgens zijn \
  <a href="https://sites.google.com/site/memodiaresearchproject/" target="_blank> "> Ph.D </a> in software engineering \
  met een focus op Agile ontwikkelmethoden en modeltransformaties. Fabian voegde zich bij het WebDeb-team als \
  software architect, analist en ontwikkelaar van het WebDeb-platform en website. <br> \
  Fabian houdt toezicht op de software engineering-cursussen en het lab op de <a href="https://cosc.canterbury.ac.nz" target="_blank"> \
  CSSE-afdeling </a> van de University of Canterbury en is geïnteresseerd in modelgestuurde engineering, \
  modeltraceerbaarheid en modelvisualisatie. <br> <a class = "btn btn-link secondary pull-right" \
  href="http://www.canterbury.ac.nz/engineering/contact-us/people/fabian-gilson.html" target="_blank">More info...</a>

aboutus.webdeb.deniz = Deniz is communicatiemedewerker voor het WebDeb-project. Ze is een taalkundige en een literaire theoretica, ze is \
  onderzoeksassistent bij de <a http://www.uclouvain.be/" target="_blank"> Université Catholique de Louvain </a> en een lid \
  van <a href="https://www.uclouvain.be/girsef.html" target="_blank"> Girsef </a> (Interdisciplinaire onderzoeksgroep over \
  socialisatie, onderwijs en opleiding). Haar onderzoek richt zich op het (inter) subjectief gebruik van taal en identiteitsvorming \
  in de literatuur. Ze werkte als een linguïstisch analist en consultant voor TexTrix, een bedrijf dat content management software produceert <\
 , en ze instrueerde en ondersteunde Belgische teams van Allianz, Dentsu en Petercam. Een recent gecertificeerd hoger secundair \
  schoolleraar, Deniz is gepassioneerd door identiteitsconstructie van tieners binnen de school in het Web 2.0-tijdperk en is actief betrokken \
  bij een burgerbeweging die een heel andere school voorstelt (<a href = "https://www.toutautrechose.be/toutautreecole" \
  target = "_ blank"> Tout Autre école </a>). <br> <a class = "btn btn-link secundair pull-right" href = "http://www.uclouvain.be/deniz.uygur" \
  target = "_ blank"> Meer info ... </a>

aboutus.webdeb.martin = Martin vervoegde in september 2017 het project en is nu de ontwikkelaar van het \
  platform. Hij behaalde onlangs een bachelorgraad in informatica management bij Hénallux. Het onderwerp van zijn afstudeerwerk \
richtte zich op de analyse en implementatie van een webplatform voor het beheren van lesroosters binnen een universiteit. \
<br> <a class="btn btn-link secondary pull-right" href="http://www.uclouvain.be/martin.rouffiange" target="_blank"> Meer informatie ... </a>

aboutus.webdeb.pyschobb=Pierre-Yves Schobbens est professeur d'informatique, spécialisé en vérification de logiciel. \
  Il s'intéresse à la vérification de modèles (model-checking), les logiques, les méthodes formelles de développement de logiciels, \
  les lignes de produits logiciels, le génie logiciel orienté agents. Il est aussi en charge des relations internationales de sa Faculté. \
<br><a class="btn btn-link secondary pull-right" href="https://directory.unamur.be/staff/pyschobb" target="_blank">Plus d''info...</a>

aboutus.webdeb.pierre =Pierre voegde zich bij het project in november 2017 en is een onderzoeker voor het project "Jeunesse et Enjeux Politiques (JEP)", of ̙ "Jeugd en politieke kwesties" \
Naast andere taken zijn hij en zijn andere collega's verantwoordelijk voor het onderwijzen en faciliteren van het pedagogisch systeem van \
het project en de implementatie ervan met docenten en studenten. Pierre is promovendus in de politicologie en sociologie van sociale bewegingen, \
facilitator en trainer in levenslange educatie en in de wereld van jeugd, muzikant in verschillende formaties. Buiten het JEP-project, \
is Pierre Beaulieu geïnteresseerd in interculturaliteit, de vermindering van ongelijkheid, de verbetering van politieke participatie of Oost-West dialoog. \
<br> <a class="btn btn-link secondary pull-right" href="https://www.linkedin.com/in/pierre-beaulieu-90341a140/" target="_blank"> Meer informatie ... </a>

aboutus.webdeb.robin=$Robin Dumont a rejoint le projet en Juin 2018 et remplace Pierre Beaulieu en tant que chercheur pour \
  le projet “Jeunes et enjeux politiques” (JEP). Intéressé lors de son parcours universitaire par les questions de \
  délinquance chez les jeunes, il s’est rapidement tourné vers les questions d’éducation. <br> \
Membre du Girsef (Groupe interdisciplinaire de recherche sur la socialisation, l'éducation et la formation), \
  il est aujourd’hui l’un des chercheurs chargés de l’interprétation des résultats de la recherche autour du \
  projet dispositif « Jeunes et (élections) régionales 2019 ». Il participe également à l’optimisation de la \
  plateforme WebDeb, aux formations à l’utilisation de la plateforme, ainsi qu’à sa diffusion. \
<br><a class="btn btn-link secondary pull-right" href="https://uclouvain.be/fr/repertoires/robin.dumont" target="_blank">Plus d''info...</a>

#
# partners
#
partner.title = Media Partners
partner.desc = We bedanken onze mediapartners die hebben ingestemd met het bieden van toegang tot hun inhoud, geannoteerd door WebDeb-bijdragers.


#
# terms and condition
#
terms.title=Algemene voorwaarden
terms.user.title=Gebruikersvoorwaarden
terms.user.subtitle=(Lees deze voorwaarden tot het einde zorgvuldig door)
terms.privacy.title=Privacybeleid 
terms.contributor.title=Bijdragersvoorwaarden
terms.cookie.title=Cookiebeleid 
terms.software.title=Softwarelicentie
terms.contact.title=Contact

#
# help bubbles (tooltips)
#

bubble.title = Enkele tips
bubble.sample = In deze hulpballonnen vindt u een heleboel nuttige informatie ...

# actor

bubble.entry.actor.type = Selecteer <i> Individuele persoon </ i> als u een fysiek persoon invoert (levend of dood). \
Selecteer <i> Organisatie </ i> als u bijvoorbeeld een bedrijf, een vereniging, een overheid of een staat binnengaat.
bubble.entry.actor.lastname = Voer de naam van de acteur in, met inachtneming van de standaard <b> conventies voor hoofdletters </b>: de eerste \
letter (s) van elk woord in hoofdletter (s) (behalve voor deeltjes), gevolgd door kleine letters. <br /> <br /> \
U kunt <b> achternamen met dubbele of afgebroken achternamen </b> invoeren. Achternamen moeten volledig worden ingetypt: afkortingen bestaande uit een \
hoofdletter gevolgd door een punt is niet toegestaan. <br /> <br /> \
Terwijl u een naam invoert, wordt een lijst met eerder opgeslagen namen aan u voorgesteld. Als u een bestaande naam wilt kiezen, selecteert u deze uit de <b> lijst met suggesties </b> door de muisaanwijzer of de pijltoetsen op uw toetsenbord te gebruiken.

bubble.entry.actor.firstname = Voer de voorna(a)m(en) van de actor in, en houd daarbij rekening met standaard <b> conventies voor hoofdletters </b>: de eerste \
letter (s) van elk woord in hoofdletter (s) (behalve voor deeltjes), gevolgd door kleine letters. <br /> <br /> \
U kunt <b> dubbele voornamen of namen met koppelteken </b> invoeren. Namen moeten volledig worden ingetypt: afkortingen \
gevolgd door een punt zijn niet toegestaan. <br /> <br /> <p class = "indent"> Voer bijvoorbeeld <i> George Walker Bush </ i> in en niet <i> George W. Bush </ i>. </p> Middelnamen mogen enkel ingevoerd worden als ze noodzakelijk zijn om twee actoren te onderscheiden, \
Ten slotte is het een goed idee om ervoor te zorgen dat de spelling van de naam van de actor correct is want deze kan niet worden gewijzigd nadat hij is opgeslagen, \
in tegenmening tot andere stukjes informatie over de acteur.

bubble.entry.actor.birthdate = Voor datums vóór Christus (BC): voeg <i> "-" </ i> toe vóór het jaar.

bubble.entry.actor.pseudo = Belangrijk: Let op: geen interpunctie in de letters van het acroniem of de afkorting. Schrijf bijvoorbeeld <i> JFK </ i> en niet <i> J.F.K </ i>.

bubble.entry.actor.affiliations = De affiliatie van de actor is een organisatie, bedrijf, openbare inmening of vereniging waartoe de actor behoort of ooit behoorde (als bestuurslid, ...). <br /> <br /> Voor de <b> positie </b> voegt u \
binnen de organisatie toe: medewerker, directeur, verkoper, vertegenwoordiger, minister, secretaris, lid, minister van cultuur en onderwijs, beheerder, etc. <br /> <br /> Voer voor de <b> organisatie </b> de volledige naam in van het acroniem (<i> Verenigde Naties </ i> \
in plaats van <i> VN </ i>). <br .<br /> \
<b> Voeg ten lidmaatschap toe of verwijder het </b> door op de knoppen + en - te klikken.

bubble.entry.actor.affiliations.start = voor datums vóór Christus (BC): voeg <i> "-" </ i> toe vóór het jaar.
bubble.entry.actor.affiliations.end=$En cas d''incertitude, laissez le champ vide

bubble.entry.actor.orgname = Vul de volledige naam in \
van de <i> organisatie </ i> waar de actor bij is aangesloten, niet het acroniem. Voer bijvoorbeeld <i> Verenigde Naties </ i> in plaats van <i> VN </ i> in. <br /> \
Terwijl u de naam van een organisatie invoert, wordt een lijst met eerder opgeslagen organisaties aan u voorgesteld. Als u een bestaande organisatie wilt kiezen, selecteert u deze in de <b> lijst met suggesties </b> door de muisaanwijzer of de pijltoetsen op uw toetsenbord te gebruiken.

bubble.entry.actor.acro = Belangrijk: let op: geen interpunctie in de letters van het acroniem of de afkorting. Schrijf bijvoorbeeld <i> NATO </ i> en niet <i> N.A.T.O <i />.

bubble.entry.actor.showoldnames.org = <b> u kent de oude naam van de organisatie <b>: voeg deze toe. \
<br /> <br /> <b> De organisatie veranderde van naam <b>: verplaats de oude naam in het frame hieronder en voeg de nieuwe toe in het frame hierboven.

bubble.entry.actor.orgaffiliationsForm=De affiliatie van een organisatie is de overkoepelende organisatie of overheidsinstantie waartoe de organisatie behoort of ooit behoorde: \
Bijvoorbeeld een afdeling die is aangesloten bij een organisatie, een afdeling die eigendom is van een bedrijf, een NGO-lid van een netwerk of een merk binnen een groot bedrijf. <br /> <br /> \
Voer de volledige naam van de <i> organisatie </ i> in, niet het acroniem. Voer bijvoorbeeld <i> Verenigde Naties </ i> in plaats van <i> VN </ i> in. <br /> <br /> \
Voeg een affiliatie toe of verwijder deze </b> door op de knoppen + en - te klikken.

bubble.entry.actor.qualificationsForm=$Indiquez ici les formations complétées par l''acteur. Indiquez le titre complet de la formation. Par exemple : "maîtrise en sciences politiques et sociales" ou "certificat d''études secondaires supérieur".

bubble.entry.actor.legal= Kies uit de volgende lijst met wettelijke bedrijfsstatussen: \
<ul> \
<li> <i> Bedrijf </ i>: een commerciële onderneming. </li> \
<li> <i> Politieke partij </ i>: een lokale, nationale of internationale politieke partij. </li> \
<li> <i> Openbaar bestuur, openbaar lichaam of politieke besluitvormingsorganisatie </ i>: een regering, een van haar diensten, een overheidsdienst of een ander territoriaal lichaam. </li> \
<li> <i> Unie, groep van organisaties, lobby of sociale beweging </ i>: elke groep mensen die werkt voor het beheer of de verdediging van gemeenschappelijke belangen. </li> \
<li> <i> Een ander type non-profitorganisatie </ i>: NGO, coöperatie of een ander type vereniging waarvan het doel geen financieel gewin is. </li> \
</ul>
bubble.entry.actor.parentsForm=$Indiquez le père et/ou la mère de l''acteur uniquement si il/elle est un acteur·trice public·que également

# text

bubble.entry.author.text.label.author = Een auteur kan een fysieke persoon of organisatie zijn, bijvoorbeeld een persbureau <br /> <br /> Als de auteur een is \
fysieke persoon, voer dan de voornaam in, gevolgd door hun achternaam. \
<p class = "indent"> Voer bijvoorbeeld <i> Barack Obama </ i> in en niet <i> Obama Barack </ i>. </p> \
Voer de achternaam van de auteur in, met inachtneming van de standaard <b> conventies voor hoofdletters </b>: de eerste \
letter (s) van elk woord in hoofdletter (s) (behalve voor deeltjes), gevolgd door kleine letters. <p class = "indent"> Voer bijvoorbeeld <i> Pablo in \
Neruda </ i> en niet <i> Pablo NERUDA </ i>. <P> Als de auteur een <b> organisatie </b> is, moet u de volledige naam opgeven en niet een acroniem of afkorting. \
<p class = "indent"> Voer bijvoorbeeld <i> Verenigde Naties </ i> in het veld in en niet <i> UN </ i>. </p> \
De rol en aansluiting zijn optioneel. Wanneer u een lidmaatschap invoert, wordt een lijst met eerder opgeslagen voorkeuren aan u voorgesteld. \
Als u een bestaande combinatie wilt kiezen, selecteert u deze in de <b> lijst met suggesties </b> door de muisaanwijzer of de pijltoetsen op uw toetsenbord te gebruiken.

bubble.entry.text.language = Als u op "De tekst is een vertaling" klikt, geef dan de titel op in de oorspronkelijke taal en geef de originele taal op.

bubble.entry.text.sourceTitle = Dit veld moet worden ingevuld als de tekst deel uitmaakt van een verzameling (een tijdschrift, tijdschrift, krant, radio of televisieprogramma, enz.) \
<ul> \
<li> Voor collectieve werken (congresverslagen, verzamelingen van korte verhalen, bloemlezingen, enz.), geeft u de hoofdtitel van het werk aan. </li> \
<li> Geef voor journaals, tijdschriften of kranten de titel van het werk aan. </li> \
<li> Geef voor radio- of televisieshows de naam van de show </li> aan. \
</ul>

bubble.entry.tags=Geen hashtag, maar een, twee of drie woorden of woordgroepen die betrekking hebben op "de variabelen" in de zin.<br><br>\
Opdat de gegevensdatabase WebDeb zo coherent mogelijk zou zijn, ziehier een aantal te respecteren regels : <br>\
<ul>\
<li>minimum 1 tag, maximum 3</li>\
<li>niet meer dan twee woorden per tag (zonder het meerekenen van een of twee koppelwoorden zoals in « vermindering van de mortaliteit »  bijvoorbeeld)</li>\
<li>geen referentie aan geografische entiteiten, individuen of organisaties omdat de laatste twee vakken van het formulier het mogelijk maken deze te vermelden.\
</ul><br><br>\
Rekening houden met de suggesties die tevoorschijn komen onder de tag die u aan het schrijven bent. Indien een van de suggesties geschikt is, deze selecteren.\

bubble.entry.text.publication = Voor een niet-vertaalde tekst is de publicatiedatum de eerste datum waarop de tekst werd gepubliceerd of geproduceerd. \
Als u bijvoorbeeld een passage invoert uit de beroemde <i> tekst van Martin Luther King 'I have a dream </ i> die u hebt gevonden op uw favoriete blog, \
voer <i> 28/08/1963 </ i> in en niet de publicatiedatum van de tekst op de blog. <br /> <br /> \
Voor een vertaalde tekst is de publicatiedatum de datum waarop de tekst voor het eerst werd gepubliceerd in de bron van de tekst (niet noodzakelijkerwijs de oorspronkelijke bron), die u moet specificeren, \
door bijvoorbeeld zijn internetadres te geven. <br /> <br /> \
<br /> De publicatiedatum is verplicht. Als u de publicatiedatum niet weet, klikt u op de knop <b> Datum onbekend </b>. <br /> <br />

bubble.entry.text.textType = Selecteer een van de volgende typen: \
<ul> \
<li> <b> Artistiek </b>: roman, gedicht, kort verhaal, film of theater-script ... </li> \
<li> <b> interview </b>: discussie, dialoog, gesprek, scripted, semi-scripted of spontaan interview. </li> \
<li> <b> Informatief </b>: statistisch rapport, activiteits- of voortgangsrapport, encyclopedie, schoolhandleiding of leerboek. </li> \
<li> <b> Journalistiek </b>: nieuwsartikel, rapport, gepubliceerd door grote of kleine mediabronnen of onafhankelijke journalisten </li> \
<li> <b> Normatief </b>: wet of wet, uitspraak, verdrag, standaard. </li> \
<li> <b> Opinie </b>: debat, beroep, toespraak, kritisch essay of artistieke beoordeling (film / boek / artwork, enz.). </li> \
<li> <b> Toekomst </b>: project, plan of programma voor de toekomst </li> \
<li> <b> Praktisch </b>: technische of praktische handleiding, toeristische gids, kookrecept ... </li> \
<li> <b> Advertenties </b>: slogan, argument met een commercieel doel. </li> \
<li> <b> Wetenschappelijk </b>: artikel, monografie, boekrecensie, proefschrift of proefschrift, marktonderzoek ... gepubliceerd in een wetenschappelijke context. </li> \
<Li> <b> andere </b> </li> \
</ul>

bubble.entry.text.area=$Vous ne pouvez copier l''intégralité d''un texte que s''il est libre de droit \
  (texte tombé dans le domaine public, ou avec autorisation de reproduction, ou émanant d''instances publiques). \
  Si le texte est un PDF, veuillez si possible insérer son URL (elle doit finir par .pdf), ou alors l''importer depuis vos fichier en appuyant sur le bouton "parcourir". \
  Dans les autres cas, WebDeb insèrera automatiquement la page du site web si le site qui héberge le texte l''autorise. 

bubble.entry.text.area.2=$Vous ne pouvez copier l''intégralité d''un texte que s''il est libre de droit \
  (texte tombé dans le domaine public, ou avec autorisation de reproduction, ou émanant d'instances publiques). \
  S''il s''agit d''un PDF, vous pouvez l''importer depuis vos fichier en appuyant sur le bouton "parcourir".

bubble.entry.text.visibility = Tekstinhoud kan <b> openbaar zichtbaar zijn </b>, <i> d.w.z. </ i> elke gebruiker heeft toegang tot zijn volledige inhoud. <br> <br> In geval van \
   <b> pedagogische </b> teksten, hebben alleen leraren, onderzoekers of studenten (als zodanig geregistreerd) er toegang toe. <br> <br> In het geval van <b> privé </b> \
   teksten, kunnen alleen de bijdragers die bepaalde inhoud hebben ingevuld, er toegang toe krijgen, maar elke bijdrager heeft mogelijk zijn eigen inhoud.

#workwith

bubble.work.instructions = Om naar een bestaande tekst te zoeken, voert u simpelweg de titel van de tekst in de zoekbalk in. \
Een paar letters zijn genoeg voor de zoekmachine om titels voor te stellen die de woorden bevatten die u invoert. U kunt vervolgens de tekst selecteren die u zoekt \
door uw muisaanwijzer te gebruiken en op de titel te klikken, of door de pijltoetsen op uw toetsenbord te gebruiken en vervolgens op <i> Enter </ i> te drukken. \
<br /> <br /> Als u geen van de bestaande teksten kent, kunt u klikken op <i> Een willekeurig geselecteerde tekst openen </ i> \
Een tekst zal dan aan u worden voorgesteld. Als u deze tekst niet wenst te selecteren, klikt u eenvoudig opnieuw op de knop om een andere te selecteren. \

bubble.work.nlp = Het systeem markeert bepaalde elementen in elke zin om te helpen bij het identificeren en coderen van meningen <br /> <br /> \
Deze elementen kunnen worden gegroepeerd in drie hoofdcategorieën: \
<ul> \
<li> de acteurs van de zin (in kleur): personen of organisaties die in de tekst worden genoemd </li> \
<li> contextuele informatie (in kleur): plaatsen en tijden vermeld in de tekst </li> \
<Li> connectors (onderstreept): voegwoorden, discoursmarkeringen / connectoren en andere elementen die de structuur van een zin en de tekst in het algemeen tonen </li>. \
</ul>

bubble.work.arg.instructions = Een mening is de passage in de tekst die het hoofdidee bevat dat u wilt opnemen. Dit uittreksel \
mag niet te lang zijn en moet niet noodzakelijkerwijs alle elementen bevatten die worden genoemd in het argument dat u wilt opnemen (zoals de acteurs \
vertegenwoordigd door voornaamwoorden). In de volgende tekst kunt u bijvoorbeeld de sectie alleen vetgedrukt kopiëren, ook als \
<i> Roodkapje </ i> niet expliciet wordt genoemd (je introduceert deze naam bij het maken van de mening): <p class = "quote"> Roodkapje gaat snel van start \
om haar grootmoeder te bezoeken die in een ander dorp woonde. <b> Terwijl ze door het bos liep, ontmoette ze een wolf die haar probeerde op te eten. </b> </p> \
Als u een nieuw mening wilt maken, markeert u eenvoudig de betreffende passage in de tekst en klikt u op de knop <i> Toevoegen </ i> die na de markering wordt weergegeven. \

#argument links

bubble.entry.argument.links = In dit gedeelte kunt u de linken tussen twee meningen definiëren. Kies eerst het <u> type relatie </ u> \
Verschillende soorten relaties tussen meningenen zijn mogelijk: \
<ul> \
<li> <b> Gelijksoortigheidsrelatie </b>: wanneer twee meningen identieke of tegenstrijdige ideeën bevatten. <br /> Voorbeeld: \
<p class = "quote"> We moeten het onderwijssysteem hervormen om het egalitair te maken. <p> \
<b> is vergelijkbaar met: </b> \
<p class = "quote"> Het schoolsysteem moet worden hervormd, zodat het minder ongelijk is. </p> </li> \
<li> <b> Rechtvaardigingsrelatie </b>: wanneer een mening een ander rechtvaardigt, kwalificeert of tegenspreekt. <br /> Voorbeeld: \
<p class = "quote"> De structuur van het huidige onderwijssysteem verzet zich tegen het gelijkheidsbeginsel onder studenten. </p> \
<B> rechtvaardigt: </b> \
<p class = "quote"> We moeten de huidige structuur van het onderwijssysteem opschudden. </p> </li> \
</ul> \
Specificeer de manier waarop het eerste mening de tweede kwalificeert. Afhankelijk van het type relatie, kunt u kiezen tussen: \
<ul> \
<li> is vergelijkbaar of is tegen (similariteitsrelatie) </li> \
<li> ondersteunt / rechtvaardigt, kwalificeert of weerspreekt (verantwoordingsrelatie) </li> \
</ul> \
In het derde keuzemenu kunt u een mening kiezen dat dit type link heeft met de eerste mening. \
<br /> <br /> Door op het teken "+" te klikken, kunt u een ander argument toevoegen dat aan het eerste argument is gekoppeld.

#argument

bubble.entry.argument.excerpt = Dit is het uittreksel dat je in de tekst hebt geselecteerd. Dit uittreksel kan niet worden gewijzigd. \
<ul> \
<li> Als u het fragment wilt wijzigen nadat het al is opgeslagen, moet u naar de sectie voor het wijzigen van argumenten gaan \
op de vorige pagina en deze verwijderen door op <span class = "fa fa-trash"> te klikken. </li> \
<li> Als u het fragment wilt wijzigen voordat het is opgeslagen, kunt u eenvoudig teruggaan naar de wijziging van argumenten \
in de tekstbewerkingsinterface door op <i> Annuleren </ i> onder aan de pagina te klikken. </li> \
</ul>

bubble.entry.author.argument.label.reporter=Dit veld is optioneel. Dit veld enkel invullen wanneer de auteur van de tekst de woorden van anderen aanhalen, \
  door deze tussen haakjes te citeren of door deze samen te vatten. Bijvoorbeeld wanneer een journalist in zijn artikel een fragment van een interview of een citaat plaatst. <br /><br /> \
Ajoutez ou retirez un nom à l''aide des boutons <span class="fa fa-plus"></span> en <span class="fa fa-minus"></span>

bubble.entry.author.argument.label.cited=Dit veld is optioneel. Dit veld enkel invullen wanneer een individu of organisatie vernoemt wordt in de zin.\
Wanneer het echter gaat om een individu of organisatie van wie of waarvan men de woorden rapporteert, vermeld deze actor in het veld hierboven. <br><br>\
Voeg of een naam toe of verwijder een naam met de knoppen <span class="fa fa-plus"></span> en <span class="fa fa-minus"></span>

bubble.entry.argument.type = Met deze classificatie kun je het type betrokkenheid van een acteur definiëren met betrekking tot wat ze zeggen. \
Onze tools zullen automatisch een categorie voorstellen. U kunt deze categorie behouden of deze naar wens wijzigen door op een ander selectievakje te klikken. \
Er zijn 4 hoofdtypen engagement in het discours: \
<ol> \
<li> <b> Constatief </b>: alle andere soorten uittreksels. <br /> Voorbeeld: <p class = "quote"> Barack Obama gaat elke ochtend hardlopen in Central Park. </p> \
<li> <b> Prescriptief </b>: elk discours dat te maken heeft met orders, bevelen, de gebiedende wijs. Voorschrijvende meningen worden meestal geformuleerd met werkwoorden zoals <i> moeten, verplichten, het zou goed zijn om </ i> ... \
<br /> Voorbeeld: <p class = "quote"> Je moet je soep opeten om sterk te worden. </p> </li> \
<li> <b> Appreciatief </b>: elk discours gerelateerd aan sentiment of emotie. Waarderende meningen worden meestal geformuleerd met werkwoorden zoals \
<i> liefhebben, houden van, haten, waarderen, vereren, verachten, walgen, genieten, beschimpen, gelukkig / verdrietig / geïrriteerd / weerzinwekkend / beschaamd / verveeld / gepassioneerd zijn </ i> ... <br /> Voorbeelden: <p class = "quote"> Ik hou ervan om mensen te zien dansen net zoals ze horen lachen. </p> \
<p class = "quote"> Het idee dat je weggaat, maakt me verdrietig. </p> </li>  \
<li> <b> Performatief </b>: elk discours dat te maken heeft met een toekomstige verbintenis. Performatieve meningen worden meestal geformuleerd met werkwoorden zoals <i> beloven, \
plannen, willen, de intentie hebben om, wensen </ i> ... <br /> Voorbeelden: <p class = "quote"> Ik beloof dat ik kom als je hulp nodig hebt. </p> < p class = "quote"> \
We willen de organisatie hervormen. </p> </li> \
</ol>

bubble.entry.argument.timing = Geef de tijd aan van het hoofdwerkwoord van de mening en niet dat van het ondergeschikte werkwoord (en) (als die er zijn). \
<br /> Voorbeeld: <p class = "quote"> We zullen hem de hele nacht ondervragen om erachter te komen of hij deze misdaad heeft gepleegd. </p> <p class = "indent"> \
Tijd: toekomst </p>

bubble.entry.argument.shade = Kies uit de verschillende suggesties met betrekking tot de zekerheid / waarschijnlijkheid van de mening. <br /> Voorbeeld: \
<P class = "quote"> De actrice loopt misschien tot 3 kilometer per dag om fit te blijven </p> <p class = "streepje"> Nuance:. Het is mogelijk dat </p>

bubble.entry.argument.standard = Wijzig het fragment (<b>altijd tussen haakjes […]</b>) is relevant wanneer het fragment onnodig lang is \
of wanneer het niet verstaanbaar is voor een persoon die de tekst niet gelezen heeft.</br></br> \
Bijvoorbeeld : \
<ol>\
<li> wanneer voornaamwoorden of andere woorden (deze, dit,...) betrekking hebben op de tekst waaruit het fragment komt moeten deze vervangen worden door wat ze betekenen \
(Dit niet doen wanneer "ik" of "wij" de auteurs zijn van de quote). </li>\
<li>wanneer een contextueel element mankeert voor het begrijpen van de tekst, deze tussen haakjes toevoegen op de juiste plaats in de zin.\
(Het is echter onnodig het gebied toe te voegen want het zal hieronder vernoemd worden in het veld "betrokken gebied(en) »)</li>\
<li>wanneer het contextuele element voorkomt in de zin voor het fragment en dat deze zin lang is, \
kan het handig zijn de quote te starten met een samenvatting van deze zin tussen haakjes […]. </li>\
</ol>

bubble.entry.argument.class.excerpt = Dit is het uittreksel dat je in de tekst hebt geselecteerd. Dit fragment kan niet worden gewijzigd. \
<ul> \
<li> Als u de quote wilt wijzigen nadat het is opgeslagen, moet u naar de sectie voor de wijziging van meningen gaan op de vorige pagina en deze verwijderen door op <span class = "fa fa-trash">. </li> te klikken \
<li> Als u het fragment wilt wijzigen voordat het is opgeslagen, kunt u eenvoudig teruggaan naar de wijziging van meningen \
in de tekstbewerkingsinterface door op <i> Annuleren </ i> onder aan de pagina te klikken. </li> \
</ul>

bubble.entry.argument.class.standard = Dit is de standaardvorm van uw mening. Het kan niet worden gewijzigd. Als u wijzigingen wilt aanbrengen \
ga dan eenvoudigweg terug naar de onderkant van de pagina door op <i>Back</i> te klikken onderaan deze pagina. \

bubble.entry.place.placename =Gebied(en) waarop de zin betrekking heeft (een gemeente, een regio, een land, een continent,...).<br> \
Indien de zin geen betrekking heeft op een specifiek gebied, "wereld" vermelden. <br> \
Indien de zin betrekking heeft op verschillende gebieden, omdat die twee landen vergelijkt bijvoorbeeld, klik dan op + om een ander gebied toe te voegen. <br> \
Begin met het intikken van de eerste letters van de naam en kies in de lijst van suggesties.   


# search

bubble.browse.title = Met de zoekbalk kunt u de WebDeb-database verkennen. Typ een of meer woorden in het zoekveld. \
U kunt uw zoekopdracht verfijnen door slechts een of meer soorten bijdragen te selecteren.

# Others

place.map=Zie op kaart
pdf.cantread=You can''t read the pdf ? 

media.lang.code=nl_NL
media.mail.subject.0=$J`ai vu un acteur sur Webdeb
media.mail.body.0=$J`ai vu une page concernant {0} sur
media.mail.subject.1=$J`ai vu un débat sur Webdeb
media.mail.body.1=$"{0}" sur
media.mail.subject.2=$J`ai vu un texte sur Webdeb
media.mail.body.2=$J`ai vu une page concernant le texte: "{0}" sur
media.mail.subject.3=$J`ai vu une citation sur Webdeb
media.mail.body.3=$"{0}" sur
media.mail.subject.4=$J`ai vu une affirmation sur Webdeb
media.mail.body.4=$"{0}" sur
media.mail.subject.6=$J`ai vu un dossier sur Webdeb
media.mail.body.6=$J`ai vu une page concernant "{0}" sur

#Projects

toolbox.menu.project=Gérer les projets

project.edit=Editer le projet
project.submit=Enregistrer le projet
project.save.success=Enregistré avec succès !
project.save.error=Impossible de sauvegarder...
project.title=Projets
project.name=Nom du projet*
project.technicalName=Nom technique du projet*
project.beginDate=Date du début de projet*
project.endDate=Date de fin de projet*
project.pedagogic=Le projet est-il pédagogique ?*
project.inprogress=Le projet est en cours...

projet.group.edit=Editer le groupe de projet (école)
project.group.name=Nom du groupe*
project.group.technicalName=Nom technique du groupe*
project.group.submit=Enregistrer le groupe

project.subgroup.edit=Editer le sous groupe de projet (classe)
project.subgroup.name=Nom du sous-groupe*
project.subgroup.technicalName=Nom technique du sous-groupe*
project.subgroup.nbContributors=Nombre de participants *
project.subgroup.submit=Enregistrer le sous-groupe
project.subgroup.groups=Groupes liés au sous-group

project.contributors.create=Générer les contributeurs (élèves)

#Dictionary

dictionary.save.error=Element du dictionnaire sauvegarder avec succès!
dictionary.save.success=Erreur dans l''enregistrement de l''élement du dictionnaire...
dictionary.search.instructions=Commencez à taper pour chercher parmi le dictionnaire
dictionary.search.label=Chercher dans le dictionnaire
dictionary.search.title=Lancer la recherche dans le dictionnaire

entry.dictionary.header=Editer l''élement du dictionnaire
entry.dictionary.title.label=Intitulé
entry.dictionary.modify=Modifier cet élément

#Admin mail
admin.mail.title=Stuur een e-mail naar WebDeb-gebruikers

admin.mail.usergroup.label=Gebruikersgroep
admin.mail.usergroup.error=Onjuiste gebruikersgroep

admin.mail.usergroup.1=Bijdrager
admin.mail.usergroup.2=Groepsleiders
admin.mail.usergroup.3=Admins
admin.mail.usergroup.4=Alle gebruikers
admin.mail.usergroup.5=Ikzelf (handig voor testen)

admin.mail.newsletter.label=De e-mail is een nieuwsbericht (verzendt deze e-mail niet naar gebruikers die geen nieuwsbrieven willen)
admin.mail.newsletter.error=De e-mail moet inhoud bevatten

admin.mail.title.label=Titel van de e-mail
admin.mail.title.error=De e-mail moet een titel hebben

admin.mail.style.label=Stijl (CSS) van de mail
admin.mail.style.place=Voer hier de stijl (CSS code) van de mail in.

admin.mail.content.label=Inhoud van de mail
admin.mail.content.place=Voer hier de inhoud van de mail in.
admin.mail.content.error=De e-mail moet inhoud bevatten

admin.mail.general.success=De e-mail is succesvol verzonden !
admin.mail.general.error=De e-mail was niet verzonden...

#election 2019
election.2019.title=Accédez aux CV de tous les candidats aux élections régionales bruxelloises du 26 mai 2019.
election.2019.desc=CV incomplet ? Erreur ? WebDeb vous donne la possibilité d’enrichir et modifier les données. Voici comment :
election.2019.browse=Chercher un candidat :

election.2019.stats.title=Comment les partis ont-ils composé leurs listes pour les élections régionales bruxelloises ?
election.2019.stats.subtitle=Autres chiffres sur les élections régionales bruxelloises :
election.2019.stats.desc=Voici quelques stats à partir des données accumulées dans WebDeb


election.2019.stats.ad.title=pour faire savoir <br> qui est qui
election.2019.stats.ad.subtitle.1=Un chose est certaine
election.2019.stats.ad.subtitle.2=Voir les CV
election.2019.stats.ad.desc.1=Ces informations n’auraient pas été possibles sans les données accumulées dans WebDeb. \
Cette plateforme permet en effet d’établir des liens d’affiliation entre acteurs pour faire savoir <b>qui est qui</b>.
election.2019.stats.ad.desc.2=<b>Faire savoir qui est qui vous intéresse ?</b> WebDeb est alors fait pour vous. \
C’est en effet une plateforme collaborative dans l’esprit de Wikipédia.
election.2019.stats.ad.desc.3=<b>Vous pouvez y contribuer librement</b> à propos de n’importe quel thème ou territoire, \
même si WebDeb est actuellement surtout centré sur la politique bruxelloise.
election.2019.stats.ad.desc.4=Pour savoir <b>comment compléter les informations sur un acteur</b>
election.2019.stats.ad.desc.5=voir la vidéo tutorielle

election.2019.stats.1.title=Quels sont les partis qui font le plus appel à des candidat.es néophytes ?
election.2019.stats.1.desc=70 % des candidat.es aux élections régionales bruxelloises de 2019 ne se sont encore jamais présenté.es aux élections régionales bruxelloises. \
  C’est ce que révèlent nos données, limitées aux élections de 2004, 2009 et 2014. Six partis ne sont d’ailleurs composés que de néophytes. \
  A l’opposé, Open VLD et CD&V n’en comptent qu’un tiers. Les transfuges ne sont pas très nombreux (moins de 4% en moyenne).
election.2019.stats.1.graph=A noter que nous avons considéré que le ou la candidat.e l’avait été dans le même parti quand celui-ci était en cartel avec d’autres.
election.2019.stats.1.list=A quelles élections régionales bruxelloises ont déjà participé les candidat.es ? Et au sein de quel parti  ?
election.2019.stats.1.list.legend=Les candidatures présentées dans d’autres partis, en cartel ou sous un autre nom du même parti figurent en brun
election.2019.stats.1.stat.desc.1=70 % des candidat·es aux élections régionales bruxelloises ne se sont encore jamais présenté·es aux élections régionales bruxelloises.
election.2019.stats.1.stat.desc.2=Six partis ne sont composés que de néophytes. A l’opposé, Open VLD et CD&V n’est comptent qu’un tiers.
election.2019.stats.1.stat.desc.3=Côté francophone, la proportion d’ancien·nes parlementaires ou ministres est au moins 2,5 fois supérieure au PS que dans les autres partis.

election.2019.stats.2.title=Les futurs parlement et gouvernement bruxellois compteront au moins 46 % de nouveaux membres
election.2019.stats.2.desc.1=Le 26 mai, 97 responsables politiques devront être remplacés : 89 députés mais aussi 5 ministres et 3 secrétaires d’état. \
  Parmi eux, un peu plus de la moitié (52) se représente devant les électeurs. Il y aura donc au moins 46 % de nouvelles têtes au gouvernement et au parlement bruxellois.
election.2019.stats.2.desc.2=Côté francophone, il y aura un peu plus de nouvelles têtes. En effet, 51 % des 77 responsables actuels ne se représentent pas. \
  Parmi eux, le ministre Didier Gosuin (DéFI) et les 5 parlementaires qui sont devenus indépendants en cours de législature \
  (la cdH Mahinur Ozdemir, les libéraux Armand De Decker et Alain Destexhe et les socialistes Mahamed Azzouzi et Zahoor Manzoor). \
  Le PS est le parti le plus « conservateur » : près de 3/4 des responsables sortants se représentent. \
  A l’autre bout de la distribution, seuls 1 parlementaire MR sur 3 se représente.
election.2019.stats.2.desc.3=Côté flamand, la continuité est plus forte puisque 70 % des responsables actuels se représentent et que, dans certains partis, \
  cette proportion atteint 100 %.
election.2019.stats.2.desc.4=Parmi les partants, il y a celles et ceux qui quittent définitivement la politique supra-communale, mais d’autres aussi, moins nombreux, \
  qui se présentent au niveau fédéral. Plusieurs partants faisaient partie des meubles du Parlement puisqu’ils étaient déjà là lors de sa création en 1989. \
  Parmi eux, le président sortant Charles Picqué (PS), qui a aussi été ministre et ministre-président, mais aussi Serge de Patoul (DéFI) \
  et Marion Lemesre (MR), qui ont été parlementaires sans discontinuer pendant 30 ans, et d’autres comme Willem Draps (MR), \
  Evelyne Huytebroeck (Ecolo) ou Martine Payfa (DéFI) qui y ont siégé par intermittence. Une page se tourne…
election.2019.stats.2.graph=% de parlementaires et ministres sortants parmi les candidats
election.2019.stats.2.list=Les parlementaires qui quittent la politique régionale bruxelloise (et leur période de présence au Parlement et/ou au gouvernement)
election.2019.stats.2.stat.desc.1= Il y aura au moins 46 % de nouvelles têtes au gouvernement et au parlement bruxellois.
election.2019.stats.2.stat.desc.2=Près de 3/4 des ministres ou député·es socialistes sortant·es se représentent. À l’autre bout de la distribution, seul 1 parlementaire MR sur 3 se représente.
election.2019.stats.2.stat.desc.3=Côté flamand, 70 % des responsables actuels se représentent et, dans certains partis, cette proportion atteint 100 %.

election.2019.stats.3.title=Quelles fonctions ont exercé les 64 candidat.es ayant déjà eu des responsabilités régionales ?
election.2019.stats.3.desc.1=Sur les 771 candidats qui se présentent aux élections régionales bruxelloises, seuls 64 (8,3 %) \
  ont déjà exercé des responsabilités à l’échelle de la région bruxelloise, que ce soit en tant que ministre, secrétaire d’état ou député, \
  et qu’ils l’aient fait lors de la législature qui touche à son terme ou auparavant. Mais si on fait le même calcul pour \
  les seuls partis ayant actuellement des députés, ce taux monte à 12 %. Il est très variable selon les partis, et d’autant plus élevé \
  que ces partis ont précédemment envoyé beaucoup de députés au Parlement.
election.2019.stats.3.desc.2=La proportion d’ancien mandataires parmi les candidats est en moyenne plus élevée dans les partis néerlandophones. \
  Explication partielle : ces partis ne peuvent présenter que 17 candidats (contre 72 pour les partis francophones).
election.2019.stats.3.desc.3=Côté francophone, la proportion d’anciens parlementaires ou ministres est au moins 2,5 fois supérieure au PS que dans les autres partis. \
  En effet, un candidat socialiste sur quatre a un passé ministériel ou parlementaire à la Région contre moins d’un sur dix au MR. \
  Cet écart s’explique en grande partie par le différentiel du nombre d’élus ou ministres obtenu par ces deux partis lors des \
  deux précédentes législatures (respectivement 23 et 24 pour le PS, contre 13 et 15 pour le MR, soit un rapport de 2,6 à 1 sur \
  les deux législatures).
election.2019.stats.3.graph=% de parlementaires et ministres sortants parmi les candidats
election.2019.stats.3.list=Fonctions exercées à la Région par les candidat.es ayant déjà été député, ministre ou secrétaire d'état à la Région Bruxelloise
election.2019.stats.3.stat.desc.1=64 candidat·es (8,3 %) ont déjà été ministre, secrétaire d’état ou député·e bruxellois·e. Pour les partis ayant actuellement des député·es, ce taux monte à 12 %.
election.2019.stats.3.stat.desc.2=La proportion d’ancien·nes mandataires parmi les candidats est en moyenne plus élevée dans les partis néerlandophones. 
election.2019.stats.3.stat.desc.3=Côté francophone, la proportion d’ancien·nes parlementaires ou ministres est au moins 2,5 fois supérieure au PS que dans les autres partis.

election.2019.stats.4.title=30 candidat.es sont actuellement bourgmestre ou échevin.es. Quels seront leurs choix si elles et ils sont élu.es ?
election.2019.stats.4.desc.1=5 bourgmestres et 25 échevin.es se présentent comme candidat.es aux élections régionales bruxelloises. \
  Pour l’instant, rien ne leur interdit de cumuler un poste de député avec leur mandat dans l’exécutif communal{0}. \
  Par contre, s’il leur était proposé de devenir ministre ou secrétaire d’état, ils ou elles devraient faire un choix.
election.2019.stats.4.desc.2=4 de ces candidat.es se présentent sur des listes flamandes. \
  Côté francophone, près de la moitié (12sur 26) proviennent du PS. Et ce parti est, de loin, celui qui intègre le plus \
  d’échevin.es ou de bourgmestres dans sa liste (près de 17 %
election.2019.stats.4.desc.3=Pour composer leurs listes, les 9 partis qui disposent d’échevins ou de bourgmestres n’ont pas adopté les mêmes règles. \
  Ecolo est le seul à avoir renoncé à intégrer des membres de collèges communaux dans ses listes. \
  Les autres l’ont accepté, mais certains comme DéFI, le cdH ou le PS pourraient imposer aux éventuel.les élu.es de choisir\
   entre leur poste de député.e et celui de bourgmestre ou échevin.e.
election.2019.stats.4.desc.4=Si vous envisagez de voter pour ces candidat.es, mieux vaut dès lors savoir si leur souhait \
  est de siéger au parlement ou si leur objectif est de faire profiter les autres membres de leur renommée.
election.2019.stats.4.desc.legend.1=1. Après les « affaires », une proposition d’ordonnance voulait interdire tout cumul \
  de mandat de parlementaire bruxellois avec celui de bourgmestre ou d’échevin au niveau communal. \
  Bien que plus de 2/3 des parlementaires ont voté en faveur de ce projet, celui-ci n’est pas passé du fait de l’opposition \
  d’une majorité de partis flamands. Ont voté contre : MR, Open VLD, N-VA, CD&V et Vlaams Belang. Se sont abstenus : \
  Benoît Cerexhe (cdH), Bernard Clerfayt (DéFI) et Alain Destexhe (MR).
election.2019.stats.4.list=Qui sont les bourgmestres et échevin.es présent.es sur les listes régionales ?
election.2019.stats.4.stat.desc.1=5 bourgmestres et 25 échevin.es se présentent comme candidat·es. Il ne leur est pas interdit de devenir député·e, mais bien d’être ministre ou secrétaire d’état tout en restant bourgmestre ou échevin·e.
election.2019.stats.4.stat.desc.2=4 de ces candidat·es se présentent sur des listes flamandes. Côté francophone, près de la moitié proviennent du PS. 
election.2019.stats.4.stat.desc.3=Ecolo est le seul parti disposant de bourgmestres et échevin.es à avoir renoncé à en intégrer dans ses listes.

election.2019.stats.5.title=Les partis présentent sur leurs listes beaucoup d’élus communaux
election.2019.stats.5.desc.1=197 des candidat.es qui se présentent aux élections régionales bruxelloises occupent actuellement\
   des fonctions de bourgmestre, d’échevin.e ou de conseiller.ère communal.e. C’est loin d’être négligeable : \
  ce nombre représente en effet plus du quart des mandataires communaux (28,4 %) et plus du quart des candidat.es (25,5 %).\
   Et même plus d’un tiers (33,4 %) de ces candidat.es si on se limite aux 12 partis ayant au moins un.e mandataire communal.e. 
election.2019.stats.5.desc.2=Qui plus est, ce type de candidat.es est généralement mieux placé, plus visible, et donc plus \
  susceptible d’être élu.e. Comme le montrent les graphiques, la proportion de mandataires communaux est décroissante \
  à mesure qu’on descend dans la liste des candidat.es, avec un rebond dans les dernières places « de combat ».
election.2019.stats.5.desc.3=Les justifications et raisons avancées par les partis qui adoptent de telles pratiques sont variées : \
  la notoriété plus grande des élu.es communaux sert d’attrape-voix ; l’ancrage sur le terrain communal permet aux députés d’être plus \
  conscients des problèmes quotidiens ; la présence de mandataires communaux au Parlement ou au Gouvernement sert \
  les intérêts de la commune, etc. Mais Les stratégies des partis varient significativement.
election.2019.stats.5.desc.4=Cela dépend évidemment d’abord de la présence ou non de mandataires communaux dans ces partis et \
  du nombre de candidat.es à trouver (72 sur les listes francophones et 17 sur les listes néerlandophones). \
  Mais si l’on compare des partis ayant un vivier communal de taille similaire, on note d’importantes différences.
election.2019.stats.5.desc.5=Côté francophone, PS, MR et Ecolo, les trois partis dominants, qui comptent un nombre assez \
  similaire de mandataires (environ 140), n’en n’envoient pas tous autant aux élections régionales. Alors qu’Ecolo réserve 37,5 % \
  des places à ses conseillers communaux (et aucune à ses bourgmestres et échevins), le PS en envoie 58,3 %, dont un tiers \
  d’échevins et bourgmestres, tandis que le MR adopte une stratégie intermédiaire.
election.2019.stats.5.graph=A noter que les mandataires communales féminines sont presque aussi nombreuses que les hommes \
  à avoir accepter d’être présentes sur les listes régionales (94 contre 103).
election.2019.stats.5.list=Candidat.es qui sont déjà mandataires communaux (comme bourgmestre, échevin ou conseiller communal)
election.2019.stats.5.stat.desc.1=Plus du quart des candidat·es occupent actuellement des fonctions de bourgmestre, d’échevin·e ou de conseiller·ère communal·e.
election.2019.stats.5.stat.desc.2=Bourgmestres, échevin.es ou membres de conseils communaux occupent généralement les places les plus en vue.
election.2019.stats.5.stat.desc.3=Alors qu’Ecolo réserve 37,5 % des places à ses mandataires communaux (et aucune à ses bourgmestres et échevins), le PS en envoie 58,3 %, dont un tiers d’échevin·es et bourgmestres.

contribute.do.title=$Trois façons de contribuer à WebDeb
contribute.do.1.title=$À partir de cette page :
contribute.do.1.text=$vous ajoutez du contenu en cliquant sur les images ci-contre.
contribute.do.2.title=$À partir de chaque page :
contribute.do.2.text=$vous enrichissez ou modifier le contenu en cliquant sur les « + », les  crayons ou les engrenages.
contribute.do.3.title=$À partir d''une page internet
contribute.do.3.text=$à l’aide de l’extension <a href="https://chrome.google.com/webstore/detail/webdeb-extension/jajhkepdfeglimnjjopjgkigofoadfhc">Chrome</a> \
  ou <a href="https://addons.mozilla.org/en-US/firefox/addon/webdeb-extension/">Firefox</a> , vous surlignez des citations et les importez facilement dans WebDeb.

contribute.entry.title=$Enregistrement
contribute.entry.text=$<p>Pour pouvoir faire une contribution, vous devez vous enregistrer \
préalablement sur WebDeb.</p> \
<p>Dans le formulaire d’inscription, vous pouvez choisir un pseudo si vous souhaitez ne pas voir apparaître \
votre nom dans l’historique des contributions</p>

contribute.help.title=$Aides
contribute.help.1.title=$Sur chaque page
contribute.help.1.text=$Boutons « i », «Afficher l''aide»
contribute.help.1.text2=$Afficher l''aide
contribute.help.2.title=$Dans la page d''aide

contribute.contribute.title=$Contribuer
contribute.contribute.1.title=$En solo
contribute.contribute.1.text=$<p>Toute contribution compte. C’est un précieux apport au travail collectif et cumulatif.</p> \
<p>Il n’y a pas de modération a priori, mais une exigence de rigueur. N’encodez que des informations exactes \
sur les acteurs et leurs opinions.</p> \
<p>Ne contribuez pas à WebDeb pour défendre votre opinion mais pour rendre compte de la diversité des opinions.</p>
contribute.contribute.2.title=$En équipe
contribute.contribute.2.text=$<p>L’information est foisonnante. La couvrir seul est quasi impossible.</p> \
<p><a href="{0}">Rejoignez ou créez</a> un collectif thématique centré sur le domaine ou le sujet qui vous intéresse.</p> \
<p>Invitez d’autres personnes à vous rejoindre et partagez entre vous les tâches de collecte et de suivi d’information.</p>
contribute.contribute.3.title=$En privé
contribute.contribute.3.text=$<p>Vous voulez utiliser WebDeb pour un cours, une formation ou un débat interne à votre organisation ?</p> \
<p><a href="{0}">Créez un groupe</a> privé dont le travail n’est visible que par les membres du groupe sans perdre pour autant \
l’accès aux données accumulées dans WebDeb public.</p>

contribute.difuse.title=$Diffuser et réutiliser vos contributions
contribute.difuse.1.title=$Garder vos contributions en mémoire
contribute.difuse.1.text=$<p>Une opinion lue sur le net vous intéresse ? Utiliser l’extension Chrome ou Firefox et en quelques clics ajoutez-la à la page « mes contributions » en même temps qu’à WebDeb public.</p> \
<p>Retrouvez et triez aisément vos contributions dans la page « Mes contributions »</p>
contribute.difuse.2.title=$Faites connaître votre travail
contribute.difuse.2.text=$<p>Vous avez pu mettre à jour l’organigramme d’une administration, lister les filiales d’une multinationale, ou clarifier les positions et les arguments d’un débat ?</p> \
<p>Communiquez aisément l’URL de la page via Facebook, Twitter ou mail.</p>
contribute.difuse.3.title=$Exportez vos données
contribute.difuse.3.text=<$p>Progressivement, nous rendrons possible l’exportation de données de chaque page sous forme de tableurs. Vous pourrez ainsi les analyser et les réutiliser.</p>

contribution.viz.presentation.title=$a propos
contribution.viz.arguments.title=$arguments
contribution.viz.citations.title=$Citations
contribution.viz.argumentation.title=$Argumentation
contribution.viz.arguments.content.title=$Contenu
contribution.viz.arguments.list.title=$Liste
contribution.viz.sociography.title=$Prises de position
contribution.viz.bibliography.title=$bibliographie
contribution.viz.identity.title=$identité
contribution.viz.affiliations.title=$affiliations
contribution.viz.persons.title=$Personnes
contribution.viz.org.title=$Organisations
contribution.viz.about.title=$on en parle
contribution.viz.debate.title=$Débats
contribution.viz.actors.title=$Acteurs
contribution.viz.authors.title=$Auteurs

share.link.sucess=$Lien copié avec succès !

tag.edit.title=$Editer le dossier
tag.category.edit.title=$Editer la catégorie

tag.delete.title=$Supprimer le dossier
tag.category.delete.title=$Supprimer la catégorie

argument.copy.title=$Copier l''argument
citation.copy.title=$Copier la citation